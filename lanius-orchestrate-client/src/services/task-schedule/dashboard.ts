/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/**
 * @desc 调度总览业务封装
 * @fileName dashboard.ts
 * @author zoujunjie@yzbdl.ac.cn
 * @date 2022-4-28
 */
import iconSuccess from "@/assets/img/task-schedule/dashboard/icon-task-instance-success.png";
import iconError from "@/assets/img/task-schedule/dashboard/icon-task-instance-error.png";
import iconFail from "@/assets/img/task-schedule/dashboard/icon-task-instance-fail.png";
import iconInitFail from "@/assets/img/task-schedule/dashboard/icon-task-instance-init-fail.png";
import iconInit from "@/assets/img/task-schedule/dashboard/icon-task-instance-init.png";
import iconPause from "@/assets/img/task-schedule/dashboard/icon-task-instance-pause.png";
import iconRetry from "@/assets/img/task-schedule/dashboard/icon-task-instance-retry.png";
import iconRunning from "@/assets/img/task-schedule/dashboard/icon-task-instance-running.png";
import iconSkip from "@/assets/img/task-schedule/dashboard/icon-task-instance-skip.png";
import iconWaiting from "@/assets/img/task-schedule/dashboard/icon-task-instance-waiting.png";
import iconStop from "@/assets/img/task-schedule/dashboard/icon-task-instance-stop.png";
import iconArrange from "@/assets/img/task-schedule/dashboard/icon-task-arrange.png";
import iconResourceConfig from "@/assets/img/task-schedule/dashboard/icon-resource-config.png";
import iconHost from "@/assets/img/task-schedule/dashboard/icon-host.png";
import iconNode from "@/assets/img/task-schedule/dashboard/icon-node.png";
import iconTaskResource from "@/assets/img/task-schedule/dashboard/icon-task-resource.png";

/**
 * 单个卡片的数据结构
 */
interface CardItemModel {
  key: string;
  text: string;
  color: string;
  iconUrl: string;
  figure: string;
  targetUrl: string;
}

//任务实例和任务编排的接口数据结构
interface TaskItemData {
  status: number;
  count: number;
}

const getTaskInstanceDefaultList = (): CardItemModel[] => {
  return [
    {
      key: "1",
      text: "成功",
      color: "green",
      iconUrl: iconSuccess,
      targetUrl: "/task-schedule/task-instance?status=1",
      figure: "",
    },
    {
      key: "2",
      text: "失败",
      color: "red",
      iconUrl: iconFail,
      targetUrl: "/task-schedule/task-instance?status=2",
      figure: "",
    },
    {
      key: "3",
      text: "初始化失败",
      color: "red",
      iconUrl: iconInitFail,
      targetUrl: "/task-schedule/task-instance?status=3",
      figure: "",
    },
    {
      key: "4",
      text: "初始化中",
      color: "blue",
      iconUrl: iconInit,
      targetUrl: "/task-schedule/task-instance?status=4",
      figure: "",
    },
    {
      key: "5",
      text: "运行中",
      color: "green",
      iconUrl: iconRunning,
      targetUrl: "/task-schedule/task-instance?status=5",
      figure: "",
    },
    {
      key: "6",
      text: "跳过",
      color: "orange",
      iconUrl: iconSkip,
      targetUrl: "/task-schedule/task-instance?status=6",
      figure: "",
    },
    {
      key: "8",
      text: "监控异常",
      color: "red",
      iconUrl: iconError,
      targetUrl: "/task-schedule/task-instance?status=8",
      figure: "",
    },
    {
      key: "11",
      text: "等待中",
      color: "blue",
      iconUrl: iconWaiting,
      targetUrl: "/task-schedule/task-instance?status=11",
      figure: "",
    },
  ];
};

const getTaskArrangementDefaultList = (): CardItemModel[] => {
  return [
    {
      key: "2",
      text: "已启用",
      color: "green",
      iconUrl: iconArrange,
      targetUrl: "/task-schedule/arrangement?status=2",
      figure: "",
    },
    {
      key: "1",
      text: "已禁用",
      color: "",
      iconUrl: iconArrange,
      targetUrl: "/task-schedule/arrangement?status=1",
      figure: "",
    },
  ];
};

const getResourceManagementDefaultList = (): CardItemModel[] => {
  return [
    {
      key: "serverNormalCount",
      text: "主机连接正常",
      color: "green",
      iconUrl: iconHost,
      targetUrl: "/task-schedule/host-config?status=1",
      figure: "",
    },
    {
      key: "serverAbnormalCount",
      text: "主机连接异常",
      color: "orange",
      iconUrl: iconHost,
      targetUrl: "/task-schedule/host-config?status=0",
      figure: "",
    },
    {
      key: "serverProgramRunningCount",
      text: "服务节点运行中",
      color: "green",
      iconUrl: iconNode,
      targetUrl: "/task-schedule/service-node?status=1",
      figure: "",
    },
    {
      key: "serverProgramStoppedCount",
      text: "服务节点已停止",
      color: "red",
      iconUrl: iconNode,
      targetUrl: "/task-schedule/service-node?status=0",
      figure: "",
    },
    {
      key: "serverProgramAbnormalCount",
      text: "服务节点异常",
      color: "orange",
      iconUrl: iconNode,
      targetUrl: "/task-schedule/service-node?status=2",
      figure: "",
    },
    {
      key: "resourceConfigNormalCount",
      text: "资源库连接正常",
      color: "green",
      iconUrl: iconResourceConfig,
      targetUrl: "/task-schedule/allocation-resources?status=1",
      figure: "",
    },
    {
      key: "resourceConfigAbnormalCount",
      text: "资源库连接异常",
      color: "orange",
      iconUrl: iconResourceConfig,
      targetUrl: "/task-schedule/allocation-resources?status=0",
      figure: "",
    },
    {
      key: "jobCount",
      text: "任务资源 - 作业",
      color: "blue",
      iconUrl: iconTaskResource,
      targetUrl: "/task-schedule/resource?resourceType=1",
      figure: "",
    },
    {
      key: "ktrCount",
      text: "任务资源 - 转换",
      color: "blue",
      iconUrl: iconTaskResource,
      targetUrl: "/task-schedule/resource?resourceType=2",
      figure: "",
    },
  ];
};

export {
  CardItemModel,
  TaskItemData,
  getTaskInstanceDefaultList,
  getTaskArrangementDefaultList,
  getResourceManagementDefaultList,
};
