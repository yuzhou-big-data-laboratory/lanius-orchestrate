/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/*
 * @description: 资源库的相关业务逻辑
 * @fileName: repository.ts
 * @author: zoujunjie@yzbdl.ac.cn
 * @date: 2022-09-15
 */

import { IHash, IKeyValue } from "../common/tool";

class ConnectUrl {
  ip: string = "";
  port: number | string = "";
  dbName: string = "";
  params: IHash<string> = {};

  constructor(stringValue: string = "") {
    if (stringValue) {
      try {
        let jsonValue = JSON.parse(stringValue) as ConnectUrl;
        this.ip = jsonValue?.ip;
        this.port = jsonValue?.port;
        this.dbName = jsonValue?.dbName;
        this.params = jsonValue?.params;
      } catch (error) {}
    }
  }

  public toString(): string {
    return JSON.stringify(this);
  }

  public setParams(list: IKeyValue<string, string>[]): void {
    this.params = {};
    list?.forEach((item: IKeyValue<string, string>) => {
      this.params[item.key] = item.value;
    });
  }

  public getParamsList(): IKeyValue<string, string>[] {
    let list: IKeyValue<string, string>[] = [];
    for (const key in this.params) {
      if (Object.prototype.hasOwnProperty.call(this.params, key)) {
        list.push({
          key: key,
          value: this.params[key],
        });
      }
    }
    return list;
  }
}

export { ConnectUrl };
