/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/**
 * @desc 工具类方法
 * @fileName tool.ts
 * @author zoujunjie@yzbdl.ac.cn
 * @date 2022-4-28
 */
import CryptoJS from "crypto-js";

const ENCRYPT_KEY: string = import.meta.env.VITE_AES_ENCRYPT_KEY;

const createGuid = (hasLine: boolean = true): string => {
  function s4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }
  let line = hasLine ? "-" : "";
  return `${s4()}${s4()}${line}${s4()}${line}${s4()}${line}${s4()}${line}${s4()}${s4()}${s4()}`;
};

const checkPassword = (password: string): boolean => {
  if (!password) {
    return false;
  }
  const lowerCaseRule = /[a-z]/;
  const upperCaseRule = /[A-Z]/;
  const numberRule = /[0-9]/;
  const specialCodeRule = /[-`=\\;',.~!@#$%^&*()_+|{}:"?/<>]/;
  const lengthRule = /^.{6,20}$/;
  return (
    lowerCaseRule.test(password) &&
    upperCaseRule.test(password) &&
    numberRule.test(password) &&
    specialCodeRule.test(password) &&
    lengthRule.test(password)
  );
};

const checkIpV4 = (ip: string): boolean => {
  if (ip && ip.trim()) {
    let arr = ip.split(".");
    if (arr.length == 4) {
      for (let i = 0; i < arr.length; i++) {
        const item: string = arr[i];
        const num: number = Number(item);
        if (num >= 0 && num <= 255) {
          if (num.toString().length == item.length) {
            continue;
          }
        }
        return false;
      }
      return true;
    }
  }
  return false;
};

interface IHash<T> {
  [index: string]: T;
}

interface IKeyValue<TKey, TValue> {
  key: TKey;
  value: TValue;
}

const encryptAes = (text: string): string => {
  return CryptoJS.AES.encrypt(text, CryptoJS.enc.Utf8.parse(ENCRYPT_KEY), {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  }).ciphertext.toString();
};

const decryptAes = (encryptedText: string): string => {
  return CryptoJS.AES.decrypt(
    CryptoJS.enc.Base64.stringify(CryptoJS.enc.Hex.parse(encryptedText)),
    CryptoJS.enc.Utf8.parse(ENCRYPT_KEY),
    {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    }
  ).toString(CryptoJS.enc.Utf8);
};

export { createGuid, checkPassword, checkIpV4, encryptAes, decryptAes };

export type { IHash, IKeyValue };
