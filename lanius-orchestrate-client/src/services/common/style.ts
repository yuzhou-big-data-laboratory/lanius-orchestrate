/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/**
 * @desc 样式相关方法
 * @fileName style.ts
 * @author zoujunjie@yzbdl.ac.cn
 * @date 2022-05-30
 */

import styleConfig from "@/assets/css/config.module.scss";

const getColorByKey = (key: string): string => {
  switch (key) {
    case "primary":
      return styleConfig.colorPrimary;
    case "success":
      return styleConfig.colorSuccess;
    case "warning":
      return styleConfig.colorWarning;
    case "error":
    case "danger":
      return styleConfig.colorError;
    case "default":
      return "rgba(0,0,0,0.4)";
  }
  return key;
};

export { getColorByKey };
