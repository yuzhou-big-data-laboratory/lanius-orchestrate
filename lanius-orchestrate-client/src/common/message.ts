/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/*
 * @description: 消息组件的封装
 * @fileName: message.ts
 * @author: zoujunjie@yzbdl.ac.cn
 * @date: 2022-07-04
 */
import { ElMessage } from "element-plus";
import { Close, Loading, Select } from "@element-plus/icons-vue";

const showSuccessMessage = (prop: any) => {
  return ElMessage({
    message: prop.message,
    icon: Select,
    duration: prop.duration,
    customClass: "message-container message-success",
  });
};

const showErrorMessage = (prop: any) => {
  return ElMessage({
    message: prop.message,
    icon: Close,
    duration: prop.duration,
    customClass: "message-container message-error",
  });
};

const showLoadingMessage = (prop: any) => {
  return ElMessage({
    message: prop.message,
    icon: Loading,
    duration: prop.duration,
    customClass: "message-container message-loading",
  });
};

export { showSuccessMessage, showErrorMessage, showLoadingMessage };
