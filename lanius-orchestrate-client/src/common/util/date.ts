
const strToDate = (dateString: any)=>{
    try{
        let _dateString = dateString
        _dateString = _dateString.substring(0,19);    
        _dateString = _dateString.replace(/-/g,'/');
        return new Date(_dateString).getTime();
    }catch(err){
        throw new Error();
    }
}

export {strToDate}