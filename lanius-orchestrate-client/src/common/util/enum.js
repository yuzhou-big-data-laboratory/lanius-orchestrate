export const Enum = (id, name, disabled, data={}) => ({id, name, disabled, data, key: id, value: id, label: name, title: name, text: name, isEnum: true, toString(){return this.id}})
export function init (enumObj) {
	const keys = Object.keys(enumObj).filter(key => enumObj[key].isEnum)
	enumObj._list = keys.map(key => ({...enumObj[key]}))
	for (let i in keys) {
		enumObj[keys[i]] = enumObj[keys[i]].id
	}
	enumObj._of = (id) => {
		return enumObj._objectOf(id).name
	}
	enumObj._objectOf = (id) => {
		let result = enumObj._list.find(item => item.id === id)
		return result || Enum()
	}
    // 冻结，防止修改
    if (Object.freeze) {
        Object.freeze(enumObj)
    }
}