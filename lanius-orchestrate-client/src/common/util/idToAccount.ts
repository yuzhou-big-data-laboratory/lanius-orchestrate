import { HttpClient } from "@/common/http";
const $HTTP = new HttpClient();
// 通过ID返回账号
const idToAccount = (ids:any) => {
  return new Promise(resolve =>{
    $HTTP.post({
      url:`/personal/nicknames`,
      data:ids
    }).then((res:any)=>{
      resolve(res);
    })
  })
}

export {idToAccount}