/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/**
 * @desc 资源类型匹配TS
 * @fileName type-enum.ts
 * @author zhouyongjie@yzbdl.ac.cn
 * @date 2022-4-14
 */
interface IEnumDemo {
  name: string;
  val: any;
}

class EnumHelper {
  private demo: IEnumDemo;
  constructor(name: string, val: any) {
    this.demo = { name, val };
  }

  getName() {
    return this.demo.name;
  }

  getValue() {
    return this.demo.val;
  }
}

class EnumBase {
  static getEnumByVal(val: any) {
    const keys = Object.keys(this);
    for (let i in keys) {
      let _demo: EnumHelper = this[
        keys[i] as keyof typeof EnumHelper
      ] as EnumHelper;
      if (_demo.getValue() == val) {
        return _demo.getName();
      }
    }
  }
}

class TypeEnum extends EnumBase {
  static Work = new EnumHelper("作业", 1);
  static Transform = new EnumHelper("转换", 2);
}
export default TypeEnum;
