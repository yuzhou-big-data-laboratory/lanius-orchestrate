/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/**
 * @desc 树数据结构TS
 * @fileName tree-menu.ts
 * @author zhouyongjie@yzbdl.ac.cn
 * @date 2022-4-27
 */

interface IMenuItem {
  text: string;
  key: string;
  // checked: Boolean;
  children?: IMenuItem[];
  id:string
}
let treeMenu: IMenuItem[] = [
  {
    key: "/task-schedule",
    text: "任务调度",
    id:'1-0',
    children: [
      {
        text: "调度总览",
        key: "/tatistic",
        id: "1-1",
        children:[]
      },
      {
        text: "资源管理",
        id:"1-2",
        key: "",
        children: [
          {
            text: "主机配置",
            id:"1-2-1",
            key: "/resource/server",
            children:[]
          },
          {
            text: "服务节点",
            id:"1-2-2",
            key: "/resource/serverProgram",
            children:[]
          },
          {
            text: "资源配置",
            id:"1-2-3",
            key: "/resource/taskResourceConfig",
            children:[]
          },
          {
            text: "任务资源",
            id:"1-2-4",
            key: "/resource/taskResource",
            children:[]
          },
        ],
      },
      {
        text: "任务编排",
        key: "/task",
        id:"1-3",
        children:[]
      },
      {
        text: "任务实例",
        id:"1-4",
        key: "/task/instance",
        children:[]
      },
    ],
  },
  {
    key: "/system-management",
    text: "系统管理",
    id:"2-0",
    children: [
      {
        text: "角色管理",
        id:"2-1",
        key: "/role",
        children:[]
      },
      {
        text: "用户管理",
        id:"2-2",
        key: "/user",
        children:[]
      },
      {
        text: "日志管理",
        id:"2-2",
        key: "/userLog",
        children:[]
      },
    ],
  },
];
export default treeMenu;
