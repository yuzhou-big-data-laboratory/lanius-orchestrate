/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/*
 * @description:vite 配置
 * @fileName: vite.config.ts
 * @author: zoujunjie@yzbdl.ac.cn
 * @date: 2022-07-05
 */
import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import { resolve } from "path";

// https://vitejs.dev/config/
export default ({ mode }) =>
  defineConfig({
    plugins: [
      vue(),
      Components({
        resolvers: [
          ElementPlusResolver({
            importStyle: "sass",
          }),
        ],
      }),
    ],
    resolve: {
      alias: {
        "@": resolve(__dirname, "src"),
        "~/": `${resolve(__dirname, "src")}/`,
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "@/assets/css/element-plus.scss" as *;`,
        },
      },
    },
    server: {
      port: 3000,
      host: "0.0.0.0",
      proxy: {
        "/api": {
          target: loadEnv(mode, process.cwd()).VITE_API_BASE_URL,
          secure: false,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ""),
        },
      },
    },
    optimizeDeps: {
      include: [
        "vue",
        "element-plus",
        "element-plus/es",
        "element-plus/es/components/container/style/index",
        "element-plus/es/components/main/style/index",
        "element-plus/es/components/header/style/index",
        "element-plus/es/components/aside/style/index",
        "element-plus/es/components/divider/style/index",
        "element-plus/es/components/config-provider/style/index",
        "element-plus/es/components/form/style/index",
        "element-plus/es/components/button/style/index",
        "element-plus/es/components/form-item/style/index",
        "element-plus/es/components/input/style/index",
        "element-plus/es/components/image/style/index",
        "element-plus/es/components/avatar/style/index",
        "element-plus/es/components/tooltip/style/index",
        "element-plus/es/components/breadcrumb/style/index",
        "element-plus/es/components/menu/style/index",
        "element-plus/es/components/space/style/index",
        "element-plus/es/components/breadcrumb-item/style/index",
        "element-plus/es/components/sub-menu/style/index",
        "element-plus/es/components/menu-item/style/index",
        "element-plus/es/components/dropdown/style/index",
        "element-plus/es/components/popover/style/index",
        "element-plus/es/components/dialog/style/index",
        "element-plus/es/components/dropdown-menu/style/index",
        "element-plus/es/components/scrollbar/style/index",
        "element-plus/es/components/dropdown-item/style/index",
        "element-plus/es/components/empty/style/index",
        "element-plus/es/components/icon/style/index",
        "element-plus/es/components/link/style/index",
        "element-plus/es/components/badge/style/index",
        "element-plus/es/components/result/style/index",
        "element-plus/es/components/loading/style/index",
        "element-plus/es/components/select/style/index",
        "element-plus/es/components/option/style/index",
        "element-plus/es/components/switch/style/index",
        "element-plus/es/components/descriptions/style/index",
        "element-plus/es/components/descriptions-item/style/index",
        "element-plus/es/components/date-picker/style/index",
        "element-plus/es/components/table/style/index",
        "element-plus/es/components/pagination/style/index",
        "element-plus/es/components/table-column/style/index",
        "element-plus/es/components/checkbox-group/style/index",
        "element-plus/es/components/checkbox/style/index",
        "element-plus/es/components/autocomplete/style/index",
        "element-plus/es/components/tree/style/index",
        "element-plus/es/components/row/style/index",
        "element-plus/es/components/col/style/index",
        "element-plus/es/components/infinite-scroll/style/index",
        "element-plus/es/components/radio-group/style/index",
        "element-plus/es/components/radio-button/style/index",
        "element-plus/es/components/radio/style/index",
        "element-plus/es/components/tree-select/style/index",
        "element-plus/es/components/tag/style/index",
        "element-plus/es/components/tabs/style/index",
        "element-plus/es/components/tab-pane/style/index",
        "element-plus/es/components/select-v2/style/index",
        "element-plus/es/components/card/style/index",
      ],
    },
  });
