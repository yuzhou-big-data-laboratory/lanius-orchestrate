
-- ----------------------------
-- Table structure for LANIUS.lo_manager
-- ---------------------------
DROP TABLE IF EXISTS LANIUS.lo_manager;
CREATE TABLE LANIUS.lo_manager  (
    id bigint NOT NULL,
    user_name varchar(64),
    password varchar(512),
    create_time TIMESTAMP,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Records of LANIUS.lo_manager
-- ----------------------------
INSERT INTO LANIUS.lo_manager VALUES (1519516605054758913, 'admin', '$2a$10$ASCA3cm8IcdQ3Ul83aRff.0HTAATo263PThnzEe25JLi5y8KULfgq', '2022-04-28 11:19:24');

-- ----------------------------
-- Table structure for LANIUS.lo_menu
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_menu;
CREATE TABLE LANIUS.lo_menu  (
    id bigint NOT NULL,
    menu_name varchar(64),
    menu_url varchar(128),
    PRIMARY KEY (id)
);

-- ----------------------------
-- Records of LANIUS.lo_menu
-- ----------------------------
INSERT INTO LANIUS.lo_menu VALUES (1513357791062450177, '用户管理', '/user');
INSERT INTO LANIUS.lo_menu VALUES (1514493518265548802, '角色管理', '/role');
INSERT INTO LANIUS.lo_menu VALUES (1514505549144240129, '主机配置', '/resource/server');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908804, '服务节点', '/resource/serverProgram');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908805, '资源配置', '/resource/taskResourceConfig');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908806, '任务资源', '/resource/taskResource');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908807, '总览查询', '/tatistic');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908874, '用户日志', '/userLog');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908876, '任务编排', '/task');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908877, '任务实例', '/task/instance');

-- ----------------------------
-- Table structure for LANIUS.lo_monitor_indicator
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_monitor_indicator;
CREATE TABLE LANIUS.lo_monitor_indicator  (
    id bigint NOT NULL,
    indicator_name varchar(128),
    indicator_type bigint,
    remark varchar(512),
    code varchar(64),
    org_id bigint,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_monitor_statistics
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_monitor_statistics;
CREATE TABLE LANIUS.lo_monitor_statistics  (
    id bigint NOT NULL,
    indicator_id bigint NOT NULL,
    statistic varchar(128) NOT NULL,
    statistic_unit bigint NOT NULL,
    statistic_time TIMESTAMP NOT NULL,
    target_id bigint NOT NULL,
    target_symbol varchar(64) NOT NULL,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_org
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_org;
CREATE TABLE LANIUS.lo_org  (
    id bigint NOT NULL,
    org_name varchar(64) NOT NULL,
    create_time TIMESTAMP NOT NULL,
    frozen bit,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_permission
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_permission;
CREATE TABLE LANIUS.lo_permission  (
    id bigint NOT NULL,
    permission_name varchar(64) NOT NULL,
    menu_id bigint NOT NULL,
    permission_code varchar(64),
    sort bigint,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Records of LANIUS.lo_permission
-- ----------------------------
INSERT INTO LANIUS.lo_permission VALUES (1513749187481149441, '编辑', 1513357791062450177, 'system::user::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1513749728030408705, '查询', 1513357791062450177, 'system::user::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1514847509251997698, '编辑', 1514493518265548802, 'system::role::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1514847629699825666, '查询', 1514493518265548802, 'system::role::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214402, '查询', 1514505549144240129, 'resource::server::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214403, '编辑', 1514505549144240129, 'resource::server::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214404, '查询', 1516241554120908804, 'resource::serverProgram::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214405, '编辑', 1516241554120908805, 'resource::taskResourceConfig::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214406, '查询', 1516241554120908805, 'resource::taskResourceConfig::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214407, '编辑', 1516241554120908806, 'resource::taskResource::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214408, '查询', 1516241554120908806, 'resource::taskResource::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908804, '编辑', 1516241554120908804, 'resource::serverProgram::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908808, '查询', 1516241554120908807, 'resource::statistic::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908872, '删除', 1513357791062450177, 'system::user::delete', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908873, '删除', 1514493518265548802, 'system::role::delete', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908875, '查询', 1516241554120908874, 'system::userLog::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908878, '查询', 1516241554120908876, 'task::plan::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908879, '编辑', 1516241554120908876, 'task::plan::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908880, '删除', 1516241554120908876, 'task::plan::delete', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908881, '查询', 1516241554120908877, 'task::plan::instance::query', 0);

-- ----------------------------
-- Table structure for LANIUS.lo_role
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_role;
CREATE TABLE LANIUS.lo_role  (
    id bigint NOT NULL,
    role_name varchar(64) NOT NULL,
    create_time TIMESTAMP,
    create_id varchar(64),
    last_modify TIMESTAMP,
    modified varchar(64),
    org_id bigint NOT NULL,
    deleted bit,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_role_permission
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_role_permission;
CREATE TABLE LANIUS.lo_role_permission  (
    id bigint NOT NULL,
    role_id bigint NOT NULL,
    permission_id bigint NOT NULL,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_server
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_server;
CREATE TABLE LANIUS.lo_server  (
    id bigint NOT NULL,
    server_name varchar(64) NOT NULL,
    server_ip varchar(64) NOT NULL,
    server_port bigint NOT NULL,
    account_name varchar(64) NOT NULL,
    password varchar(128) NOT NULL,
    remark varchar(512),
    status bigint NOT NULL,
    org_id bigint NOT NULL,
    create_time TIMESTAMP,
    create_id bigint ,
    last_modify TIMESTAMP,
    modified bigint,
    deleted bit,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_server_program
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_server_program;
CREATE TABLE LANIUS.lo_server_program  (
    id bigint NOT NULL,
    program_name varchar(64) NOT NULL,
    server_id bigint NOT NULL,
    program_port bigint NOT NULL,
    category bigint NOT NULL,
    remark varchar(512),
    program_config bigint,
    status bigint NOT NULL,
    auth_config LONGVARCHAR NOT NULL,
    org_id bigint NOT NULL,
    create_time TIMESTAMP NOT NULL,
    create_id bigint NOT NULL,
    last_modify TIMESTAMP,
    modified bigint,
    deleted bit,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_task_instance
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_task_instance;
CREATE TABLE LANIUS.lo_task_instance  (
    id bigint NOT NULL,
    task_plan_id bigint NOT NULL,
    begin_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP,
    status bigint NOT NULL,
    server_program_id bigint NOT NULL,
    create_time TIMESTAMP NOT NULL,
    task_info LONGVARCHAR,
    org_id bigint,
    log_level bigint NOT NULL,
    task_resource_id bigint NOT NULL,
    PRIMARY KEY (id)
);

create index lo_task_instance_org_id_index on LANIUS.lo_task_instance(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_plan
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_task_plan;
CREATE TABLE LANIUS.lo_task_plan  (
    id bigint NOT NULL,
    task_name varchar(64) NOT NULL,
    task_cron varchar(512) NOT NULL,
    server_program_id bigint NOT NULL,
    task_resource_id bigint NOT NULL,
    remark varchar(512),
    json_config LONGVARCHAR,
    status bigint NOT NULL,
    group_id bigint,
    log_level bigint NOT NULL,
    org_id bigint,
    create_id bigint,
    create_time TIMESTAMP,
    last_modify TIMESTAMP,
    modified bigint,
    PRIMARY KEY (id)
);

create index lo_task_plan_org_id_index on LANIUS.lo_task_plan(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_plan_group
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_task_plan_group;
CREATE TABLE LANIUS.lo_task_plan_group (
    id bigint NOT NULL,
    group_name varchar(64),
    org_id bigint NOT NULL,
    pid bigint,
    PRIMARY KEY (id)
);

create index lo_task_plan_group_org_id_index on LANIUS.lo_task_plan_group(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_resource
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_task_resource;
CREATE TABLE LANIUS.lo_task_resource  (
    id bigint NOT NULL,
    resource_type bigint NOT NULL,
    remark varchar(512),
    resource_content LONGVARCHAR ,
    resource_config_id bigint NOT NULL,
    create_id bigint NOT NULL,
    create_time TIMESTAMP NOT NULL,
    modified bigint ,
    last_modify TIMESTAMP,
    group_id bigint,
    org_id bigint NOT NULL,
    resource_name varchar(64) NOT NULL,
    deleted bit,
    PRIMARY KEY (id)
);

create index lo_task_resource_org_id_index on LANIUS.lo_task_resource(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_resource_config
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_task_resource_config;
CREATE TABLE LANIUS.lo_task_resource_config  (
    id bigint NOT NULL,
    connect_name varchar(64) NOT NULL,
    connect_url varchar(512) NOT NULL,
    connect_account varchar(64) NOT NULL,
    connect_password varchar(128),
    remark varchar(512),
    create_id bigint NOT NULL,
    create_time TIMESTAMP NOT NULL,
    modified bigint,
    last_modify TIMESTAMP,
    org_id bigint NOT NULL,
    connect_category bigint NOT NULL,
    deleted bit,
    status bigint,
    PRIMARY KEY (id)
);

create index lo_task_resource_config_org_id_index on LANIUS.lo_task_resource_config(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_resource_group
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_task_resource_group;
CREATE TABLE LANIUS.lo_task_resource_group  (
    id bigint NOT NULL,
    group_name varchar(64),
    org_id bigint NOT NULL,
    pid bigint,
    create_time TIMESTAMP,
    create_id bigint,
    last_modify TIMESTAMP,
    modified bigint,
    deleted bit,
    PRIMARY KEY (id)
);

create index lo_task_resource_group_org_id_index on LANIUS.lo_task_resource_group(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_step_log
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_task_step_log;
CREATE TABLE LANIUS.lo_task_step_log  (
    id bigint NOT NULL,
    task_instance_id bigint,
    log_date varchar(30),
    trans_name LONGVARCHAR,
    step_name varchar(100) NOT NULL,
    step_copy bigint,
    lines_read bigint,
    lines_written bigint,
    lines_updated bigint,
    lines_input bigint,
    lines_output bigint,
    lines_rejected bigint,
    errors bigint,
    status_description varchar(20),
    input_buffer_rows bigint,
    output_buffer_rows bigint,
    speed bigint,
    running_time double,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_user
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_user;
CREATE TABLE LANIUS.lo_user  (
    id bigint NOT NULL,
    user_name varchar(64) NOT NULL,
    password varchar(512) NOT NULL,
    nick_name varchar(64) NOT NULL,
    phone varchar(11),
    email varchar(64),
    create_time TIMESTAMP,
    create_id varchar(64),
    last_modify TIMESTAMP,
    modified varchar(64),
    deleted bit,
    status bigint
);

-- ----------------------------
-- Table structure for LANIUS.lo_user_log
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_user_log;
CREATE TABLE LANIUS.lo_user_log  (
    id bigint NOT NULL,
    create_time TIMESTAMP,
    create_id bigint,
    url varchar(512),
    method varchar(32),
    event_name varchar(64),
    event_content varchar(512),
    org_id bigint,
    PRIMARY KEY (id)
);

create index lo_user_log_org_id_index on LANIUS.lo_user_log(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_user_org
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_user_org;
CREATE TABLE LANIUS.lo_user_org  (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    org_id bigint NOT NULL,
    org_chief bit,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_user_role
-- ----------------------------
DROP TABLE IF EXISTS LANIUS.lo_user_role;
CREATE TABLE LANIUS.lo_user_role  (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    PRIMARY KEY (id)
);

