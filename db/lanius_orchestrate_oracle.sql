
-- ----------------------------
-- Table structure for LANIUS.lo_manager
-- ---------------------------
CREATE TABLE LANIUS.lo_manager  (
    id number NOT NULL,
    user_name varchar(64),
    password varchar(512),
    create_time TIMESTAMP,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Records of LANIUS.lo_manager
-- ----------------------------
INSERT INTO LANIUS.lo_manager VALUES (1519516605054758913, 'admin', '$2a$10$ASCA3cm8IcdQ3Ul83aRff.0HTAATo263PThnzEe25JLi5y8KULfgq', to_date('2022-04-28 11:19:24', 'YYYY-MM-DD HH24:MI:SS'));

-- ----------------------------
-- Table structure for LANIUS.lo_menu
-- ----------------------------
CREATE TABLE LANIUS.lo_menu  (
    id number NOT NULL,
    menu_name varchar(64),
    menu_url varchar(128),
    PRIMARY KEY (id)
);

-- ----------------------------
-- Records of LANIUS.lo_menu
-- ----------------------------
INSERT INTO LANIUS.lo_menu VALUES (1513357791062450177, '用户管理', '/user');
INSERT INTO LANIUS.lo_menu VALUES (1514493518265548802, '角色管理', '/role');
INSERT INTO LANIUS.lo_menu VALUES (1514505549144240129, '主机配置', '/resource/server');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908804, '服务节点', '/resource/serverProgram');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908805, '资源配置', '/resource/taskResourceConfig');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908806, '任务资源', '/resource/taskResource');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908807, '总览查询', '/tatistic');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908874, '用户日志', '/userLog');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908876, '任务编排', '/task');
INSERT INTO LANIUS.lo_menu VALUES (1516241554120908877, '任务实例', '/task/instance');

-- ----------------------------
-- Table structure for LANIUS.lo_monitor_indicator
-- ----------------------------
CREATE TABLE LANIUS.lo_monitor_indicator  (
    id number NOT NULL,
    indicator_name varchar(128),
    indicator_type number,
    remark varchar(512),
    code varchar(64),
    org_id number,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_monitor_statistics
-- ----------------------------
CREATE TABLE LANIUS.lo_monitor_statistics  (
    id number NOT NULL,
    indicator_id number NOT NULL,
    statistic varchar(128) NOT NULL,
    statistic_unit number NOT NULL,
    statistic_time TIMESTAMP NOT NULL,
    target_id number NOT NULL,
    target_symbol varchar(64) NOT NULL,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_org
-- ----------------------------
CREATE TABLE LANIUS.lo_org  (
    id number NOT NULL,
    org_name varchar(64) NOT NULL,
    create_time TIMESTAMP NOT NULL,
    frozen char(1),
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_permission
-- ----------------------------
CREATE TABLE LANIUS.lo_permission  (
    id number NOT NULL,
    permission_name varchar(64) NOT NULL,
    menu_id number NOT NULL,
    permission_code varchar(64),
    sort number,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Records of LANIUS.lo_permission
-- ----------------------------
INSERT INTO LANIUS.lo_permission VALUES (1513749187481149441, '编辑', 1513357791062450177, 'system::user::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1513749728030408705, '查询', 1513357791062450177, 'system::user::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1514847509251997698, '编辑', 1514493518265548802, 'system::role::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1514847629699825666, '查询', 1514493518265548802, 'system::role::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214402, '查询', 1514505549144240129, 'resource::server::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214403, '编辑', 1514505549144240129, 'resource::server::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214404, '查询', 1516241554120908804, 'resource::serverProgram::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214405, '编辑', 1516241554120908805, 'resource::taskResourceConfig::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214406, '查询', 1516241554120908805, 'resource::taskResourceConfig::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214407, '编辑', 1516241554120908806, 'resource::taskResource::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516238191002214408, '查询', 1516241554120908806, 'resource::taskResource::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908804, '编辑', 1516241554120908804, 'resource::serverProgram::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908808, '查询', 1516241554120908807, 'resource::statistic::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908872, '删除', 1513357791062450177, 'system::user::delete', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908873, '删除', 1514493518265548802, 'system::role::delete', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908875, '查询', 1516241554120908874, 'system::userLog::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908878, '查询', 1516241554120908876, 'task::plan::query', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908879, '编辑', 1516241554120908876, 'task::plan::edit', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908880, '删除', 1516241554120908876, 'task::plan::delete', 0);
INSERT INTO LANIUS.lo_permission VALUES (1516241554120908881, '查询', 1516241554120908877, 'task::plan::instance::query', 0);

-- ----------------------------
-- Table structure for LANIUS.lo_role
-- ----------------------------
CREATE TABLE LANIUS.lo_role  (
    id number NOT NULL,
    role_name varchar(64) NOT NULL,
    create_time TIMESTAMP,
    create_id varchar(64),
    last_modify TIMESTAMP,
    modified varchar(64),
    org_id number NOT NULL,
    deleted char(1),
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_role_permission
-- ----------------------------
CREATE TABLE LANIUS.lo_role_permission  (
    id number NOT NULL,
    role_id number NOT NULL,
    permission_id number NOT NULL,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_server
-- ----------------------------
CREATE TABLE LANIUS.lo_server  (
    id number NOT NULL,
    server_name varchar(64) NOT NULL,
    server_ip varchar(64) NOT NULL,
    server_port number NOT NULL,
    account_name varchar(64) NOT NULL,
    password varchar(128) NOT NULL,
    remark varchar(512),
    status number NOT NULL,
    org_id number NOT NULL,
    create_time TIMESTAMP,
    create_id number ,
    last_modify TIMESTAMP,
    modified number,
    deleted char(1),
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_server_program
-- ----------------------------
CREATE TABLE LANIUS.lo_server_program  (
    id number NOT NULL,
    program_name varchar(64) NOT NULL,
    server_id number NOT NULL,
    program_port number NOT NULL,
    category number NOT NULL,
    remark varchar(512),
    program_config number,
    status number NOT NULL,
    auth_config CLOB NOT NULL,
    org_id number NOT NULL,
    create_time TIMESTAMP NOT NULL,
    create_id number NOT NULL,
    last_modify TIMESTAMP,
    modified number,
    deleted char(1),
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_task_instance
-- ----------------------------
CREATE TABLE LANIUS.lo_task_instance  (
    id number NOT NULL,
    task_plan_id number NOT NULL,
    begin_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP,
    status number NOT NULL,
    server_program_id number NOT NULL,
    create_time TIMESTAMP NOT NULL,
    task_info CLOB,
    org_id number,
    log_level number NOT NULL,
    task_resource_id number NOT NULL,
    PRIMARY KEY (id)
);

create index lo_task_instance_org_id_index on LANIUS.lo_task_instance(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_plan
-- ----------------------------
CREATE TABLE LANIUS.lo_task_plan  (
    id number NOT NULL,
    task_name varchar(64) NOT NULL,
    task_cron varchar(512) NOT NULL,
    server_program_id number NOT NULL,
    task_resource_id number NOT NULL,
    remark varchar(512),
    json_config CLOB,
    status number NOT NULL,
    group_id number,
    log_level number NOT NULL,
    org_id number,
    create_id number,
    create_time TIMESTAMP,
    last_modify TIMESTAMP,
    modified number,
    PRIMARY KEY (id)
);

create index lo_task_plan_org_id_index on LANIUS.lo_task_plan(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_plan_group
-- ----------------------------
CREATE TABLE LANIUS.lo_task_plan_group (
    id number NOT NULL,
    group_name varchar(64),
    org_id number NOT NULL,
    pid number,
    PRIMARY KEY (id)
);

create index lo_task_plan_group_index on LANIUS.lo_task_plan_group(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_resource
-- ----------------------------
CREATE TABLE LANIUS.lo_task_resource  (
    id number NOT NULL,
    resource_type number NOT NULL,
    remark varchar(512),
    resource_content CLOB ,
    resource_config_id number NOT NULL,
    create_id number NOT NULL,
    create_time TIMESTAMP NOT NULL,
    modified number ,
    last_modify TIMESTAMP,
    group_id number,
    org_id number NOT NULL,
    resource_name varchar(64) NOT NULL,
    deleted char(1),
    PRIMARY KEY (id)
);

create index lo_task_resource_org_id_index on LANIUS.lo_task_resource(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_resource_config
-- ----------------------------
CREATE TABLE LANIUS.lo_task_resource_config  (
    id number NOT NULL,
    connect_name varchar(64) NOT NULL,
    connect_url varchar(512) NOT NULL,
    connect_account varchar(64) NOT NULL,
    connect_password varchar(128),
    remark varchar(512),
    create_id number NOT NULL,
    create_time TIMESTAMP NOT NULL,
    modified number,
    last_modify TIMESTAMP,
    org_id number NOT NULL,
    connect_category number NOT NULL,
    deleted char(1),
    status number,
    PRIMARY KEY (id)
);

create index lo_task_res_config_index on LANIUS.lo_task_resource_config(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_resource_group
-- ----------------------------
CREATE TABLE LANIUS.lo_task_resource_group  (
    id number NOT NULL,
    group_name varchar(64),
    org_id number NOT NULL,
    pid number,
    create_time TIMESTAMP,
    create_id number,
    last_modify TIMESTAMP,
    modified number,
    deleted char(1),
    PRIMARY KEY (id)
);

create index lo_task_res_group_index on LANIUS.lo_task_resource_group(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_task_step_log
-- ----------------------------
CREATE TABLE LANIUS.lo_task_step_log  (
    id number NOT NULL,
    task_instance_id number,
    log_date varchar(30),
    trans_name CLOB,
    step_name varchar(100) NOT NULL,
    step_copy number,
    lines_read number,
    lines_written number,
    lines_updated number,
    lines_input number,
    lines_output number,
    lines_rejected number,
    errors number,
    status_description varchar(20),
    input_buffer_rows number,
    output_buffer_rows number,
    speed number,
    running_time float,
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_user
-- ----------------------------
CREATE TABLE LANIUS.lo_user  (
    id number NOT NULL,
    user_name varchar(64) NOT NULL,
    password varchar(512) NOT NULL,
    nick_name varchar(64) NOT NULL,
    phone varchar(11),
    email varchar(64),
    create_time TIMESTAMP,
    create_id varchar(64),
    last_modify TIMESTAMP,
    modified varchar(64),
    deleted char(1),
    status number
);

-- ----------------------------
-- Table structure for LANIUS.lo_user_log
-- ----------------------------
CREATE TABLE LANIUS.lo_user_log  (
    id number NOT NULL,
    create_time TIMESTAMP,
    create_id number,
    url varchar(512),
    method varchar(32),
    event_name varchar(64),
    event_content varchar(512),
    org_id number,
    PRIMARY KEY (id)
);

create index lo_user_log_org_id_index on LANIUS.lo_user_log(org_id);

-- ----------------------------
-- Table structure for LANIUS.lo_user_org
-- ----------------------------
CREATE TABLE LANIUS.lo_user_org  (
    id number NOT NULL,
    user_id number NOT NULL,
    org_id number NOT NULL,
    org_chief char(1),
    PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for LANIUS.lo_user_role
-- ----------------------------
CREATE TABLE LANIUS.lo_user_role  (
    id number NOT NULL,
    user_id number NOT NULL,
    role_id number NOT NULL,
    PRIMARY KEY (id)
);

