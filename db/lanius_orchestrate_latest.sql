/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : lanius-orchestrate

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/07/2022 10:47:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lo_manager
-- ----------------------------
DROP TABLE IF EXISTS `lo_manager`;
CREATE TABLE `lo_manager`  (
  `id` bigint(20) NOT NULL,
  `user_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_manager
-- ----------------------------
INSERT INTO `lo_manager` VALUES (1519516605054758913, 'admin', '$2a$10$ASCA3cm8IcdQ3Ul83aRff.0HTAATo263PThnzEe25JLi5y8KULfgq', '2022-04-28 11:19:24');

-- ----------------------------
-- Table structure for lo_menu
-- ----------------------------
DROP TABLE IF EXISTS `lo_menu`;
CREATE TABLE `lo_menu`  (
  `id` bigint(20) NOT NULL,
  `menu_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `menu_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_menu
-- ----------------------------
INSERT INTO `lo_menu` VALUES (1513357791062450177, '用户管理', '/user');
INSERT INTO `lo_menu` VALUES (1514493518265548802, '角色管理', '/role');
INSERT INTO `lo_menu` VALUES (1514505549144240129, '主机配置', '/resource/server');
INSERT INTO `lo_menu` VALUES (1516241554120908804, '服务节点', '/resource/serverProgram');
INSERT INTO `lo_menu` VALUES (1516241554120908805, '资源配置', '/resource/taskResourceConfig');
INSERT INTO `lo_menu` VALUES (1516241554120908806, '任务资源', '/resource/taskResource');
INSERT INTO `lo_menu` VALUES (1516241554120908807, '总览查询', '/tatistic');
INSERT INTO `lo_menu` VALUES (1516241554120908874, '用户日志', '/userLog');
INSERT INTO `lo_menu` VALUES (1516241554120908876, '任务编排', '/task');
INSERT INTO `lo_menu` VALUES (1516241554120908877, '任务实例', '/task/instance');

-- ----------------------------
-- Table structure for lo_monitor_indicator
-- ----------------------------
DROP TABLE IF EXISTS `lo_monitor_indicator`;
CREATE TABLE `lo_monitor_indicator`  (
  `id` bigint(20) NOT NULL,
  `indicator_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `indicator_type` tinyint(4) NULL DEFAULT NULL,
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `org_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_monitor_indicator
-- ----------------------------

-- ----------------------------
-- Table structure for lo_monitor_statistics
-- ----------------------------
DROP TABLE IF EXISTS `lo_monitor_statistics`;
CREATE TABLE `lo_monitor_statistics`  (
  `id` bigint(20) NOT NULL,
  `indicator_id` bigint(20) NOT NULL,
  `statistic` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `statistic_unit` tinyint(4) NOT NULL,
  `statistic_time` datetime(0) NOT NULL,
  `target_id` bigint(20) NOT NULL,
  `target_symbol` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_monitor_statistics
-- ----------------------------

-- ----------------------------
-- Table structure for lo_org
-- ----------------------------
DROP TABLE IF EXISTS `lo_org`;
CREATE TABLE `lo_org`  (
  `id` bigint(20) NOT NULL,
  `org_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `frozen` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_org
-- ----------------------------

-- ----------------------------
-- Table structure for lo_permission
-- ----------------------------
DROP TABLE IF EXISTS `lo_permission`;
CREATE TABLE `lo_permission`  (
  `id` bigint(20) NOT NULL,
  `permission_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  `permission_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_permission
-- ----------------------------
INSERT INTO `lo_permission` VALUES (1513749187481149441, '编辑', 1513357791062450177, 'system::user::edit', 0);
INSERT INTO `lo_permission` VALUES (1513749728030408705, '查询', 1513357791062450177, 'system::user::query', 0);
INSERT INTO `lo_permission` VALUES (1514847509251997698, '编辑', 1514493518265548802, 'system::role::edit', 0);
INSERT INTO `lo_permission` VALUES (1514847629699825666, '查询', 1514493518265548802, 'system::role::query', 0);
INSERT INTO `lo_permission` VALUES (1516238191002214402, '查询', 1514505549144240129, 'resource::server::query', 0);
INSERT INTO `lo_permission` VALUES (1516238191002214403, '编辑', 1514505549144240129, 'resource::server::edit', 0);
INSERT INTO `lo_permission` VALUES (1516238191002214404, '查询', 1516241554120908804, 'resource::serverProgram::query', 0);
INSERT INTO `lo_permission` VALUES (1516238191002214405, '编辑', 1516241554120908805, 'resource::taskResourceConfig::edit', 0);
INSERT INTO `lo_permission` VALUES (1516238191002214406, '查询', 1516241554120908805, 'resource::taskResourceConfig::query', 0);
INSERT INTO `lo_permission` VALUES (1516238191002214407, '编辑', 1516241554120908806, 'resource::taskResource::edit', 0);
INSERT INTO `lo_permission` VALUES (1516238191002214408, '查询', 1516241554120908806, 'resource::taskResource::query', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908804, '编辑', 1516241554120908804, 'resource::serverProgram::edit', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908808, '查询', 1516241554120908807, 'resource::statistic::query', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908872, '删除', 1513357791062450177, 'system::user::delete', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908873, '删除', 1514493518265548802, 'system::role::delete', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908875, '查询', 1516241554120908874, 'system::userLog::query', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908878, '查询', 1516241554120908876, 'task::plan::query', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908879, '编辑', 1516241554120908876, 'task::plan::edit', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908880, '删除', 1516241554120908876, 'task::plan::delete', 0);
INSERT INTO `lo_permission` VALUES (1516241554120908881, '查询', 1516241554120908877, 'task::plan::instance::query', 0);

-- ----------------------------
-- Table structure for lo_role
-- ----------------------------
DROP TABLE IF EXISTS `lo_role`;
CREATE TABLE `lo_role`  (
  `id` bigint(20) NOT NULL,
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `create_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_modify` datetime(0) NULL DEFAULT NULL,
  `modified` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `org_id` bigint(20) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_role
-- ----------------------------

-- ----------------------------
-- Table structure for lo_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `lo_role_permission`;
CREATE TABLE `lo_role_permission`  (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_role_permission
-- ----------------------------

-- ----------------------------
-- Table structure for lo_server
-- ----------------------------
DROP TABLE IF EXISTS `lo_server`;
CREATE TABLE `lo_server`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `server_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主机名称',
  `server_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主机ip',
  `server_port` int(11) NOT NULL COMMENT '主机端口',
  `account_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号名称',
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(4) NOT NULL COMMENT '状态 1 - 正常，0 - 异常',
  `org_id` bigint(20) NOT NULL COMMENT '组织id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `last_modify` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `modified` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `deleted` bit(1) NULL DEFAULT NULL COMMENT '是否被删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_server
-- ----------------------------

-- ----------------------------
-- Table structure for lo_server_program
-- ----------------------------
DROP TABLE IF EXISTS `lo_server_program`;
CREATE TABLE `lo_server_program`  (
  `id` bigint(64) NOT NULL COMMENT 'id',
  `program_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '节点名称',
  `server_id` bigint(20) NOT NULL COMMENT '主机id',
  `program_port` int(11) NOT NULL COMMENT '主机端口',
  `category` tinyint(4) NOT NULL COMMENT '程序类型 1-kettle 2-其他平台',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `program_config` tinyint(4) NULL DEFAULT NULL COMMENT '节点配置 1-主节点 2-从节点',
  `status` tinyint(4) NOT NULL COMMENT '节点状态 0-停止 1-运行中',
  `auth_config` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '认证配置',
  `org_id` bigint(20) NOT NULL COMMENT '组织id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `create_id` bigint(20) NOT NULL COMMENT '创建人',
  `last_modify` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `modified` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `deleted` bit(1) NULL DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_server_program
-- ----------------------------

-- ----------------------------
-- Table structure for lo_task_instance
-- ----------------------------
DROP TABLE IF EXISTS `lo_task_instance`;
CREATE TABLE `lo_task_instance`  (
  `id` bigint(20) NOT NULL,
  `task_plan_id` bigint(20) NOT NULL,
  `begin_time` datetime(0) NOT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `server_program_id` bigint(20) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `task_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `org_id` bigint(20) NULL DEFAULT NULL,
  `log_level` tinyint(4) NOT NULL,
  `task_resource_id` bigint(20) NOT NULL COMMENT '任务资源',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `org_index`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_task_instance
-- ----------------------------

-- ----------------------------
-- Table structure for lo_task_plan
-- ----------------------------
DROP TABLE IF EXISTS `lo_task_plan`;
CREATE TABLE `lo_task_plan`  (
  `id` bigint(20) NOT NULL,
  `task_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `task_cron` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `server_program_id` bigint(20) NOT NULL,
  `task_resource_id` bigint(20) NOT NULL,
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `json_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` tinyint(4) NOT NULL,
  `group_id` bigint(20) NULL DEFAULT NULL,
  `log_level` tinyint(4) NOT NULL,
  `org_id` bigint(20) NULL DEFAULT NULL,
  `create_id` bigint(20) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `last_modify` datetime(0) NULL DEFAULT NULL,
  `modified` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `org_index`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_task_plan
-- ----------------------------

-- ----------------------------
-- Table structure for lo_task_plan_group
-- ----------------------------
DROP TABLE IF EXISTS `lo_task_plan_group`;
CREATE TABLE `lo_task_plan_group`  (
  `id` bigint(20) NOT NULL,
  `group_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `org_id` bigint(20) NOT NULL,
  `pid` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `org_index`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_task_plan_group
-- ----------------------------

-- ----------------------------
-- Table structure for lo_task_resource
-- ----------------------------
DROP TABLE IF EXISTS `lo_task_resource`;
CREATE TABLE `lo_task_resource`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `resource_type` tinyint(4) NOT NULL COMMENT '资源类型',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `resource_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '资源配置',
  `resource_config_id` bigint(20) NOT NULL COMMENT '资源配置id',
  `create_id` bigint(20) NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `modified` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `last_modify` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `group_id` bigint(20) NULL DEFAULT NULL COMMENT '资源组id',
  `org_id` bigint(20) NOT NULL COMMENT '组织id',
  `resource_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源名称',
  `deleted` bit(1) NULL DEFAULT NULL COMMENT '是否被删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `org_index`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_task_resource
-- ----------------------------

-- ----------------------------
-- Table structure for lo_task_resource_config
-- ----------------------------
DROP TABLE IF EXISTS `lo_task_resource_config`;
CREATE TABLE `lo_task_resource_config`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `connect_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '连接名',
  `connect_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '链接地址',
  `connect_account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '链接账号',
  `connect_password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接密码',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_id` bigint(20) NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `modified` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `last_modify` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `org_id` bigint(20) NOT NULL COMMENT '组织id',
  `connect_category` tinyint(4) NOT NULL COMMENT '数据库类型',
  `deleted` bit(1) NULL DEFAULT NULL COMMENT '是否被删除',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '数据状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `org_index`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_task_resource_config
-- ----------------------------

-- ----------------------------
-- Table structure for lo_task_resource_group
-- ----------------------------
DROP TABLE IF EXISTS `lo_task_resource_group`;
CREATE TABLE `lo_task_resource_group`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `group_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分组名',
  `org_id` bigint(20) NOT NULL COMMENT '组织id',
  `pid` bigint(20) NULL DEFAULT NULL COMMENT '父级id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `last_modify` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `modified` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `deleted` bit(1) NULL DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `org_index`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_task_resource_group
-- ----------------------------

-- ----------------------------
-- Table structure for lo_task_step_log
-- ----------------------------
DROP TABLE IF EXISTS `lo_task_step_log`;
CREATE TABLE `lo_task_step_log`  (
  `id` bigint(20) NOT NULL,
  `task_instance_id` bigint(20) NULL DEFAULT NULL,
  `log_date` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `trans_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `step_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `step_copy` bigint(20) NULL DEFAULT NULL,
  `lines_read` bigint(20) NULL DEFAULT NULL,
  `lines_written` bigint(20) NULL DEFAULT NULL,
  `lines_updated` bigint(20) NULL DEFAULT NULL,
  `lines_input` bigint(20) NULL DEFAULT NULL,
  `lines_output` bigint(20) NULL DEFAULT NULL,
  `lines_rejected` bigint(20) NULL DEFAULT NULL,
  `errors` bigint(20) NULL DEFAULT NULL,
  `status_description` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `input_buffer_rows` bigint(20) NULL DEFAULT NULL,
  `output_buffer_rows` bigint(20) NULL DEFAULT NULL,
  `speed` bigint(20) NULL DEFAULT NULL,
  `running_time` double(20, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_task_step_log
-- ----------------------------

-- ----------------------------
-- Table structure for lo_user
-- ----------------------------
DROP TABLE IF EXISTS `lo_user`;
CREATE TABLE `lo_user`  (
  `id` bigint(20) NOT NULL,
  `user_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nick_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `create_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_modify` datetime(0) NULL DEFAULT NULL,
  `modified` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_user
-- ----------------------------

-- ----------------------------
-- Table structure for lo_user_log
-- ----------------------------
DROP TABLE IF EXISTS `lo_user_log`;
CREATE TABLE `lo_user_log`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `create_id` bigint(20) NULL DEFAULT NULL,
  `url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `event_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `event_content` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `org_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `org_index`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_user_log
-- ----------------------------

-- ----------------------------
-- Table structure for lo_user_org
-- ----------------------------
DROP TABLE IF EXISTS `lo_user_org`;
CREATE TABLE `lo_user_org`  (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `org_chief` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_user_org
-- ----------------------------

-- ----------------------------
-- Table structure for lo_user_role
-- ----------------------------
DROP TABLE IF EXISTS `lo_user_role`;
CREATE TABLE `lo_user_role`  (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lo_user_role
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
