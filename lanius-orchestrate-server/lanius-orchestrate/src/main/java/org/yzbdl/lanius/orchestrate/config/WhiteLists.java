/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.config;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2020/2/11
 */
public class WhiteLists {

    public static final String[] URLS ={
            "/v2/**", "/configuration/**", "/swagger-resources", "/configuration/**", "/swagger-ui.html", "/webjars/**","/swagger-resources/**",
            "/doc-api/**",
            "/manager/login",
            "/manager/refresh",
            "/user/login",
            "/user/refresh",
            "/user/org",
            "/menu/**",
            "/permission/**",
            "/common/**",
            "/verify/**",
            "/task/instance/transImage/**",
            "/task/instance/downloadIncrLog"
    };

    public static final String[] IGNORE_URLS ={
            "/personal/**"
    };

    public static final String[] ONLY_MANAGER ={
            "/manager/**"
    };



}
