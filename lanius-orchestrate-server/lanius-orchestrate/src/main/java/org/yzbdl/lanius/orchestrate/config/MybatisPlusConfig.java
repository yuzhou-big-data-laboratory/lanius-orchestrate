/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.yzbdl.lanius.orchestrate.common.utils.DbTypeUtil;
import org.yzbdl.lanius.orchestrate.utils.OrgHelperUtil;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.yzbdl.lanius.orchestrate.serv.utils.CurrentUserUtil;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022/2/25
 */
@Configuration
@EnableTransactionManagement
public class MybatisPlusConfig {

    @Value("${spring.datasource.driver-class-name}")
    private String dbDriverClassName;

    private static final String TENANT_ID_COLUMN = "org_id";

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(new TenantLineHandler() {

            @Override
            public Expression getTenantId() {
                if(CurrentUserUtil.isNormalUser()){
                    return new LongValue(CurrentUserUtil.getCurrentUserOrgId());
                }
                return new LongValue(1L);
            }

            @Override
            public boolean ignoreTable(String tableName) {
                return !OrgHelperUtil.getOrgTableNames().contains(tableName);
            }

            @Override
            public String getTenantIdColumn(){
                return TENANT_ID_COLUMN;
            }
        }));

        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbTypeUtil.getDbType(dbDriverClassName)));
        return interceptor;
    }
}
