/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import org.yzbdl.lanius.orchestrate.serv.utils.CurrentUserUtil;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * mybatis-plus自动填充功能，自动填充业务表的创建人、创建时间、更新人、更新时间
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-06 16:36
 */
@Component
@Slf4j
public class LaniusMetaObjectHandler implements MetaObjectHandler {

    /**
     * 创建人字段
     */
    private static final String CREATE_ID = "createId";

    /**
     * 创建时间字段
     */
    private static final String CREATE_TIME = "createTime";

    /**
     * 修改人字段
     */
    private static final String MODIFIED = "modified";

    /**
     * 修改时间字段
     */
    private static final String LAST_MODIFY = "lastModify";

    /**
     * 组织id
     */
    private static final String ORG_ID = "orgId";


    @Override
    public void insertFill(MetaObject metaObject) {
        boolean createIdSetter = metaObject.hasSetter(CREATE_ID);
        boolean createTimeSetter = metaObject.hasSetter(CREATE_TIME);
        boolean orgIdSetter = metaObject.hasSetter(ORG_ID);

        if (createIdSetter) {
            Object createId = metaObject.getValue(CREATE_ID);
            if (Objects.isNull(createId)) {
                setFieldValByName(CREATE_ID, CurrentUserUtil.getCurrentUserId(), metaObject);
            }
        }

        if (createTimeSetter) {
            Object createTime = metaObject.getValue(CREATE_TIME);
            if (Objects.isNull(createTime)) {
                setFieldValByName(CREATE_TIME, LocalDateTime.now(), metaObject);
            }
        }

        if(orgIdSetter){
            Object orgId = metaObject.getValue(ORG_ID);
            if (Objects.isNull(orgId)) {
                setFieldValByName(ORG_ID, CurrentUserUtil.getCurrentUserOrgId(), metaObject);
            }
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        boolean modifiedSetter = metaObject.hasSetter(MODIFIED);
        boolean lastModifySetter = metaObject.hasSetter(LAST_MODIFY);

        if (modifiedSetter) {
            Object modified = metaObject.getValue(MODIFIED);
            if (Objects.isNull(modified)) {
                setFieldValByName(MODIFIED, CurrentUserUtil.getCurrentUserIdNoException(), metaObject);
            }
        }

        if (lastModifySetter) {
            Object lastModify = metaObject.getValue(LAST_MODIFY);
            if (Objects.isNull(lastModify)) {
                setFieldValByName(LAST_MODIFY, LocalDateTime.now(), metaObject);
            }
        }
    }
}
