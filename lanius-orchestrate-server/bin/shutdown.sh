#!/bin/sh

target_dir=`pwd`

pid=`ps ax | grep -i 'lanius-orchestrate-server' | grep ${target_dir} | grep java | grep -v grep | awk '{print $1}'`
if [ -z "$pid" ] ; then
        echo "No lanius-orchestrate-server running."
        exit -1;
fi

echo "The lanius-orchestrate-server(${pid}) is running..."

kill ${pid}

echo "Send shutdown request to lanius-orchestrate-server(${pid}) OK"