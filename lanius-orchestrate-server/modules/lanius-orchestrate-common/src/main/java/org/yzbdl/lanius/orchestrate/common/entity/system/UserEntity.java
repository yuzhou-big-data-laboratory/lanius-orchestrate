package org.yzbdl.lanius.orchestrate.common.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseEntity;
import org.yzbdl.lanius.orchestrate.common.enums.UserStatusEnum;

import java.util.Collection;

/**
 * 用户 Entity
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 13:09
 */
@Data
@SuperBuilder
@TableName(value = "lo_user")
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity extends BaseEntity implements UserDetails {
    @OprLabel
    private String userName;
    @JsonIgnore
    private String password;
    private String nickName;

    /**
     * 用户状态，1-正常，2-禁止
     */
    private Integer status;
    private Boolean deleted;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return status.equals(UserStatusEnum.NORMAL.getCode());
    }

    @Override
    public String getPassword() {
        return password;
    }

}
