/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.config.thread;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 手动创建线程池配置类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-13 10:56
 */
@Configuration
public class ThreadConfig {

	/**
	 * 注入线程池Bean
	 *
	 * @param threadPoolConfigProperties
	 *        线程池配置参数
	 * @return
	 *        线程池实体
	 */
	@Bean
	public ThreadPoolExecutor threadPoolExecutor(ThreadPoolConfigProperties threadPoolConfigProperties){

		Integer coreSize = threadPoolConfigProperties.getCoreSize();
		Integer maxSize = threadPoolConfigProperties.getMaxSize();
		Integer keepAliveTime = threadPoolConfigProperties.getKeepAliveTime();

		return new ThreadPoolExecutor(coreSize,maxSize,keepAliveTime, TimeUnit.SECONDS,
				new LinkedBlockingQueue<>(1000),
				Executors.defaultThreadFactory(),
				// 拒绝策略
				new ThreadPoolExecutor.AbortPolicy());
	}
}
