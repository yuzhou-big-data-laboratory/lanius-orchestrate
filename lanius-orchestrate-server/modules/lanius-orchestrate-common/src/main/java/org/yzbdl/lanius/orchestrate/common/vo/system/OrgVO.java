/*
 *
 * Copyright(c) YuZhou Big Data Laboratory CO.,Ltd, 2022. All rights reserved
 */
package org.yzbdl.lanius.orchestrate.common.vo.system;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 组织VO
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-08-10 17:05
 */
@Data
public class OrgVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String orgName;
    private Boolean chief;
}
