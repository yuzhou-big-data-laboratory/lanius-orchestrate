package org.yzbdl.lanius.orchestrate.common.dto.resource;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseEntity;

/**
 * 服务节点参数类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class ServerPageDto extends BaseEntity {
        
    /**
    * 主机名称
    */    
    @ApiModelProperty(value = "主机名称")
    private String serverName;
        
    /**
    * 主机ip
    */    
    @ApiModelProperty(value = "主机ip")
    private String serverIp;

    /**
     * 主机端口
     */
    @ApiModelProperty(value = "主机端口")
    private Integer serverPort;
        
    /**
    * 账号名称
    */    
    @ApiModelProperty(value = "账号名称")
    private String accountName;
        
    /**
    * 密码
    */    
    @ApiModelProperty(value = "密码")
    private String password;
        
    /**
    * 备注
    */    
    @ApiModelProperty(value = "备注")
    private String remark;
        
    /**
    * 状态 1 - 正常，0 - 异常
    */    
    @ApiModelProperty(value = "状态 1 - 正常，0 - 异常")
    private Integer status;
        
    /**
    * 组织id
    */    
    @ApiModelProperty(value = "组织id")
    private Long orgId;
                        
    /**
    * 是否被删除
    */    
    @ApiModelProperty(value = "是否被删除")
    private Boolean deleted;
    
}

