/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.system;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 管理员查询用户的条件对象
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-09-02 12:50
 */
@Data
public class MemberInsertParamDTO {
    @NotNull(message = "用户id不能为空！")
    private Long userId;
    private List<Long> roleIds;
}
