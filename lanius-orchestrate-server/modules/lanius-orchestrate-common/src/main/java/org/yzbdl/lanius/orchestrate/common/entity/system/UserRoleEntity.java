/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdFieldEntity;

/**
 * 用户角色关联实体
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-12 13:46
 */
@Data
@SuperBuilder
@TableName("lo_user_role")
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleEntity extends IdFieldEntity {
	private Long userId;
	private Long roleId;
}
