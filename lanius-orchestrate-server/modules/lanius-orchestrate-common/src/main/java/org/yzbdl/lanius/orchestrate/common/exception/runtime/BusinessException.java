package org.yzbdl.lanius.orchestrate.common.exception.runtime;

/**
 * Created by chenjunhao@yzbdl.ac.cn on 2019/9/23. <bt>
 */
public class BusinessException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    private String errCode;
    private String errMsg;

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public BusinessException(String errMsg) {
        this.errMsg = errMsg;
    }
}
