/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2. 
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v2 for more details. 
 */
package org.yzbdl.lanius.orchestrate.common.dto.resource;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 任务资源状态统计
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-25 13:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskResourceTypeStatisticDto {

	/**
	 * 计数
	 */
	@ApiModelProperty("计数")
	private Long count;

	/**
	 * 资源类型
	 */
	@ApiModelProperty("资源类型")
	private Integer resourceType;

}
