/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.constant;

/**
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-26 13:37
 */
public interface Const {

	/**
	 * 系统换行符
	 */
	String CR = System.getProperty("line.separator");

	/**
	 * 资源组模块常量
	 */
	interface TaskResourceGroup {

		/**
		 * 资源组根节点ID
		 */
		Long TASK_RESOURCE_GROUP_ROOT_NODE_ID = -1L;

		/**
		 * 资源组根节点名称
		 */
		String TASK_RESOURCE_GROUP_ROOT_NODE_NAME = "全部";
	}


	/**
	 * 任务编排模块常量
	 */
	interface TaskPlanGroup {

		/**
		 * 任务编排根节点ID
		 */
		Long TASK_PLAN_GROUP_ROOT_NODE_ID = -1L;

		/**
		 * 任务编排根节点名称
		 */
		String TASK_PLAN_GROUP_ROOT_NODE_NAME = "全部";

		/**
		 * 任务编排验证数据分页常量page
		 */
		Integer TASK_PLAN_GROUP_VALIDATE_PAGE = 0;

		/**
		 * 任务编排验证数据分页常量pageSize
		 */
		Integer TASK_PLAN_GROUP_VALIDATE_PAGE_SIZE = 1;

	}
}
