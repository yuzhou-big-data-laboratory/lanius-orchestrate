/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.vo.system;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserEntity;

/**
 * 用户昵称类
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-14 11:18
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NickNameVo {
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	private String nickName;

	public NickNameVo(UserEntity userEntity){
		this.id = userEntity.getId();
		this.nickName = userEntity.getNickName();
	}
}
