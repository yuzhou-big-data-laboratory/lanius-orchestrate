/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.enums;

import java.util.Objects;

import lombok.Getter;

/**
 * pentaho日志级别枚举
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-26 13:56
 */
@Getter
public enum PentahoLogLevelEnum {

    /**
     * pentaho日志级别枚举
     */
    NOTHING(0, "Nothing"), ERROR(1, "Error"), MINIMAL(2, "Minimal"), BASIC(3, "Basic"), DETAILED(4, "Detailed"),
    DEBUG(5, "Debug"), ROWLEVEL(6, "Rowlevel");

    private final int level;
    private final String code;

    private PentahoLogLevelEnum(int level, String code) {
        this.level = level;
        this.code = code;
    }

    /**
     * 根据日志级别获取日志code
     *
     * @param level
     *        日志级别
     * @return
     *        日志code
     */
    public static String getPentahoLogLevelCode(Integer level) {
        String levelCode = null;

        for (PentahoLogLevelEnum val : PentahoLogLevelEnum.values()) {
            if (Objects.equals(level, val.getLevel())) {
                levelCode = val.getCode();
                break;
            }
        }
        return levelCode;
    }
}
