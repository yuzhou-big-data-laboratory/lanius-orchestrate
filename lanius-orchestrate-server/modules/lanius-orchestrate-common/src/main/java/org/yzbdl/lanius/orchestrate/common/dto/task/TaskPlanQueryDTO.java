package org.yzbdl.lanius.orchestrate.common.dto.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 任务编排列表查询请求实体
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 11:02
 */
@Data
public class TaskPlanQueryDTO {

    @ApiModelProperty(value = "分组id")
    private String groupId;

    @ApiModelProperty(value = "关键字查询")
    private String keyword;

    @ApiModelProperty(value = "资源类型")
    private Integer status;

}
