/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.result;

import java.io.Serializable;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2019/9/2
 */
public class ResultObj<T> implements Serializable {

    private Integer code;
    private String msg;
    private T data;

    private ResultObj() {
    }

    public ResultObj(ResultTypeEnum type) {
        this.code = type.getCode();
        this.msg = type.getMessage();
    }

    public ResultObj(ResultTypeEnum type, T data) {
        this.code = type.getCode();
        this.msg = type.getMessage();
        this.data = data;
    }

    public ResultObj(ResultTypeEnum type, String content, T data) {
        this.code = type.getCode();
        this.msg = content;
        this.data = data;
    }

    public static ResultObj<String> success() {
        return new ResultObj(ResultTypeEnum.SERVICE_SUCCESS, "Success");
    }

    public static <T> ResultObj<T> success(T data) {
        return new ResultObj(ResultTypeEnum.SERVICE_SUCCESS, data);
    }

    public static <T> ResultObj<T> error(T data) {
        return new ResultObj(ResultTypeEnum.SERVICE_ERROR, data);
    }

    public static  <T> ResultObj<T> success(String content, T data) {
        return new ResultObj(ResultTypeEnum.SERVICE_SUCCESS, content, data);
    }

    public static ResultObj error() {
        return new ResultObj(ResultTypeEnum.SERVICE_ERROR);
    }

    public static ResultObj error(ResultTypeEnum typeEnum) {
        return new ResultObj(typeEnum);
    }

    public static ResultObj error(ResultTypeEnum typeEnum,String msg) {
        return new ResultObj(typeEnum,msg);
    }

    public static ResultObj error(ResultTypeEnum typeEnum,String msg,String data) {
        return new ResultObj(typeEnum,msg,data);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
