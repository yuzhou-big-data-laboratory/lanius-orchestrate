/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.utils;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.GlobalConfigUtils;

/**
 * 生成EntityID雪花算法
 * 
 * @author hujian@yzbdl.ac.cn
 * @date 2022年9月19日15:52:11
 */
public class IdGeneratorUtil {

    /**
     * 生成ID
     *
     * @param entity 对象必须是纳入mybatis plus管理的实体类，并且ID自增策略必须是ASSIGN_ID
     *
     * @return ID
     */
    public static Long generateId(Object entity) {
        TableInfo tableInfo = TableInfoHelper.getTableInfo(entity.getClass());
        IdentifierGenerator identifierGenerator =
            GlobalConfigUtils.getGlobalConfig(
                       tableInfo.getConfiguration()).getIdentifierGenerator();
        return identifierGenerator.nextId(entity).longValue();
    }
}
