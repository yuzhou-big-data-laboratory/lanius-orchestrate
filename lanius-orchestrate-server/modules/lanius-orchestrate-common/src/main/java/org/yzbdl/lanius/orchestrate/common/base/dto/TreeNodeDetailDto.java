/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.base.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 树Dto
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-21 13:38
 */
@Data
@SuperBuilder
public class TreeNodeDetailDto {

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	@JsonSerialize(using = ToStringSerializer.class)
	protected Long id;

	/**
	 * 父节点id
	 */
	@ApiModelProperty(value = "父节点id")
	@JsonSerialize(using = ToStringSerializer.class)
	protected Long parentId;

	/**
	 * 节点名称
	 */
	@ApiModelProperty(value = "节点名称")
	protected String nodeName;

	/**
	 * 路径名称
	 */
	@ApiModelProperty(value = "路径名称")
	protected String path;

	/**
	 * 子节点数据
	 */
	@ApiModelProperty(value = "子节点数据")
	protected List<TreeNodeDetailDto> children;

	public void addTreeNode(TreeNodeDetailDto node){
		if(Objects.isNull(this.children)){
			this.children = new ArrayList<>();
		}
		children.add(node);
	}


}
