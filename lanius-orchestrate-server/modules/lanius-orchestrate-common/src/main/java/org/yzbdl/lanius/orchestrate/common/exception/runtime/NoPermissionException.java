package org.yzbdl.lanius.orchestrate.common.exception.runtime;

import org.springframework.security.core.AuthenticationException;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022/2/11
 */
public class NoPermissionException extends AuthenticationException {

    public NoPermissionException(String msg) {
        super(msg);
    }

    public NoPermissionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
