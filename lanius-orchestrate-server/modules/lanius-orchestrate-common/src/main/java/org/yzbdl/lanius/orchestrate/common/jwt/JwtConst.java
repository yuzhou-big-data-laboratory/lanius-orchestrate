/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.jwt;

/**
 * jwt常量
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-18 16:30
 */
public class JwtConst {
	public static final String CLAIM_KEY = "claimMap";
	public static final String NORMAL_SECRET = "cZkyO0Lxl7";
	public static final String MANAGER_SECRET = "AnkFTeuZNT";
	public static final String TOKEN_HEADER = "Bearer";
	public static final String AUTHORIZATION = "Authorization";
}
