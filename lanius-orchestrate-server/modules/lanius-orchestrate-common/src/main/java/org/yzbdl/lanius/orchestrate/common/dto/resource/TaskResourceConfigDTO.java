package org.yzbdl.lanius.orchestrate.common.dto.resource;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.enums.DataBaseTypeEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 资源配置DTO
 *
 @author zhuhongji@yzbdl.ac.cn
 * @date 2022-09-14 9:39
 */
@Data
public class TaskResourceConfigDTO {

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 关联组织id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orgId;

    /**
     * 连接名
     */
    @OprLabel
    @ApiModelProperty(value = "连接名")
    @NotEmpty(message = "连接名不能为空！")
    @Size(max = 64, message = "连接名不能超出64位字符！")
    private String connectName;

    /**
     * 链接配置
     */
    @NotBlank(message = "链接配置不能为空！")
    @ApiModelProperty(value = "链接配置")
    private String connectUrl;

    /**
     * 链接地址
     */
    @ApiModelProperty(value = "链接地址")
    private String parsedConnectUrl;

    /**
     * 链接账号
     */
    @NotEmpty(message = "链接账号不能为空！")
    @ApiModelProperty(value = "链接账号")
    private String connectAccount;

    /**
     * 链接密码
     */
    @NotEmpty(message = "链接密码不能为空！")
    @ApiModelProperty(value = "链接密码")
    private String connectPassword;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 数据库类型 1 mysql
     * @see DataBaseTypeEnum
     */
    @NotNull(message = "数据库类型不能为空！")
    @ApiModelProperty(value = "数据库类型")
    private Integer connectCategory;

    /**
     * 是否被删除
     */
    @ApiModelProperty(value = "是否被删除")
    private Boolean deleted;

    /**
     * 状态 1 - 正常，0 - 异常
     */
    @ApiModelProperty(value = "状态 1 - 正常，0 - 异常")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;

}
