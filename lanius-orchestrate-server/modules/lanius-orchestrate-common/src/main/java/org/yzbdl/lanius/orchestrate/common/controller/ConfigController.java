/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.controller;

import java.util.Map;
import java.util.function.Consumer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import com.google.common.collect.Maps;

/**
 * 修改配置
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-13 11:30
 */
@Controller
@RequestMapping("/reConfig")
public class ConfigController {

    /**
     * 配置刷新方法
     * /reConfig?http_conn_timeout=30000
     * /reConfig?http_read_timeout=30000
     */
    public static Map<String, Consumer<String>> reConfigMap = Maps.newHashMap();

    /**
     * 新增系统参数设置
     * @param configKey 配置key
     * @param configVal 配置值
     * @return 配置结果
     */
    @GetMapping
    public ResultObj<String> config(@RequestParam String configKey, @RequestParam String configVal) {
        try {
            Consumer<String> consumer = reConfigMap.get(configKey);
            consumer.accept(configVal);
            return ResultObj.success("配置更新成功");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.error("配置更新失败");
        }
    }
}
