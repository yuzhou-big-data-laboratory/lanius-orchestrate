/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.utils;

import com.baomidou.mybatisplus.annotation.DbType;
import org.apache.commons.lang3.StringUtils;

/**
 * 数据库类型映射工具类
 *
 * @author hujian@yzbdl.ac.cn
 * @date 2022-08-19 10:48
 */
public class DbTypeUtil {

	/**
	 * 获取相应数据库的DbType
	 * @param driverClassName 驱动类
	 * @return 数据库DbType
	 */
	public static DbType getDbType(String driverClassName) {
		for (DbDriverEnum value : DbDriverEnum.values()) {
			if(value.getDriverClassName()
							  .equals(driverClassName)){
				return DbType.getDbType(value.getType());
			}
		}

		// 未匹配成功直接返回DbType.OTHER
		return DbType.getDbType(StringUtils.EMPTY);
	}

	/**
	 * 数据库驱动映射枚举
	 */
	enum DbDriverEnum {

		/**
		 * mysql
		 */
		MYSQL("mysql", "com.mysql.cj.jdbc.Driver"),
		/**
		 * 达梦
		 */
		DM("dm", "dm.jdbc.driver.DmDriver"),
		/**
		 * oracle
		 */
		ORACLE("oracle", "oracle.jdbc.OracleDriver");

		private final String type;

		private final String driverClassName;

		DbDriverEnum(String type, String driverClassName) {
			this.type = type;
			this.driverClassName = driverClassName;
		}

		public String getType(){
			return this.type;
		}

		public String getDriverClassName() {
			return this.driverClassName;
		}
	}

}
