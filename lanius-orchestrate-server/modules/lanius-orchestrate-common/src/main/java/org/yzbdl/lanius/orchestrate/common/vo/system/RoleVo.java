/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.vo.system;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 返回角色视图
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-19 10:18
 */
@Data
public class RoleVo {
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	private String roleName;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long createId;
	private LocalDateTime createTime;
	private Long modified;
	private LocalDateTime lastModify;

	private List<PermissionVo> permissions;
}
