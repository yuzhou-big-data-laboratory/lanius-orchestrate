/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.entity.task;

import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdOrgFieldEntity;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 任务编排实例表
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-08 17:19
 */
@Data
@TableName(value = "lo_task_instance")
public class TaskInstance extends IdOrgFieldEntity {

    @NotNull(message = "任务编排不能为空")
    @ApiModelProperty(value = "任务编排ID")
    @TableField(value = "task_plan_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taskPlanId;

    @ApiModelProperty(value = "任务开始时间")
    @TableField(value = "begin_time")
    private Date beginTime;

    @ApiModelProperty(value = "任务结束时间")
    @TableField(value = "end_time")
    private Date endTime;

    @ApiModelProperty(value = "1 - 成功，2 - 失败，3 - 初始化失败，4 - 初始化中，5 - 运行中，6 - 跳过，7 - 已重试，8 - 监控异常，9 - 暂停（手动），1 0 - 停止（手动），1 1 - 等待中")
    @TableField(value = "status")
    private Integer status;

    @NotNull(message = "服务节点不能为空")
    @ApiModelProperty(value = "服务节点ID")
    @TableField(value = "server_program_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long serverProgramId;

    @ApiModelProperty(value = "任务资源id")
    @TableField(value = "task_resource_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taskResourceId;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "参数json格式")
    @TableField(value = "task_info")
    private String taskInfo;

    @ApiModelProperty(value = "日志等级")
    @TableField(value = "log_level")
    private Integer logLevel;

}
