/*
 *
 * Copyright(c) YuZhou Big Data Laboratory CO.,Ltd, 2022. All rights reserved
 */
package org.yzbdl.lanius.orchestrate.common.utils;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

import java.nio.charset.StandardCharsets;

/**
 * AES加密工具类
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-08-29 17:28
 */
public class AesUtils {

    public static String decrypt(String content,String key){
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES,key.getBytes(StandardCharsets.UTF_8));
        return aes.decryptStr(content, CharsetUtil.CHARSET_UTF_8);
    }

    public static String encrypt(String text,String key){
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES,key.getBytes(StandardCharsets.UTF_8));
        return aes.encryptHex(text);
    }

}
