/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.vo.system;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserEntity;
import org.yzbdl.lanius.orchestrate.common.jwt.TokenDto;

/**
 * 用户信息（登录返回信息）
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-21 14:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserTokenVo {
	private UserEntity user;
	private boolean chief;
	private TokenDto token;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long orgId;
}
