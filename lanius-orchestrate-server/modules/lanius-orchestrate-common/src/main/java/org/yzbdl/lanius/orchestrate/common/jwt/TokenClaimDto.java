/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.jwt;

import java.util.Map;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 */
public class TokenClaimDto {
    private String userId;

    private final Map<String,Object> claims;

    public TokenClaimDto(Map<String,Object> claims){
        this.claims = claims;
    }

    public boolean buildFromClaims(){
        if(this.claims!=null){
            if(claims.containsKey("userId")){
                this.userId = claims.get("userId").toString();
                return true;
            }
        }
        return false;
    }

    public String getUserId() {
        return userId;
    }
}
