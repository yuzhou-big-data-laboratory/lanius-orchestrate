/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.resource;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 外部资源信息合集
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-14 11:55
 */
@Data
@Builder
public class ExternalFileDto {

	/**
	 * 任务数据
	 */
	@ApiModelProperty(value = "任务资源")
	private List<ExternalJobDto> jobs;

	/**
	 * 转换信息
	 */
	@ApiModelProperty(value = "转换信息")
	private List<ExternalTransformationDto> transformations;

}
