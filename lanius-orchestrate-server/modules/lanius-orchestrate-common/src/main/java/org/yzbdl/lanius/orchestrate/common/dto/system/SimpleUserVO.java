package org.yzbdl.lanius.orchestrate.common.dto.system;

import lombok.Data;

/**
 * 简易的用户VO
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-15 13:32
 */
@Data
public class SimpleUserVO {
    private String id;
    private String userName;
    private String nickName;
}
