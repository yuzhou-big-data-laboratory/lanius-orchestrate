/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.jwt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * 返回前端的token对象
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 14:15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TokenDto {
    private String accessToken;
    private Date accessTokenExpiresAt;
    private String accessRefresh;
    private Date accessRefreshExpiresAt;


    public TokenDto(Map<String,Object> payloadMap,boolean manager){
        if(manager){
            this.accessToken = ManagerJwtFactory.token().generateToken(payloadMap);
            this.accessTokenExpiresAt = ManagerJwtFactory.token().getExpireDate();
            this.accessRefresh = ManagerJwtFactory.freshToken().generateToken(payloadMap);
            this.accessRefreshExpiresAt = ManagerJwtFactory.freshToken().getExpireDate();
        }else{
            this.accessToken = JwtFactory.token().generateToken(payloadMap);
            this.accessTokenExpiresAt = JwtFactory.token().getExpireDate();
            this.accessRefresh = JwtFactory.freshToken().generateToken(payloadMap);
            this.accessRefreshExpiresAt = JwtFactory.freshToken().getExpireDate();
        }
    }
}
