/*
 *
 * Copyright(c) YuZhou Big Data Laboratory CO.,Ltd, 2022. All rights reserved
 */
package org.yzbdl.lanius.orchestrate.common.vo.system;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 管理员查看账号列表
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-08-10 16:38
 */
@Data
public class AccountVO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String userName;
    private String nickName;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;
    private LocalDateTime createTime;
    private Long modified;
    private LocalDateTime lastModify;
    private Integer status;
    private Boolean deleted;
    private List<OrgVO> relativeOrg;
}
