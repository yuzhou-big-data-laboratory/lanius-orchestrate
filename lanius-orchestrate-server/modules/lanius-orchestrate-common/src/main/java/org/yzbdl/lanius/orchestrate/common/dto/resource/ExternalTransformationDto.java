package org.yzbdl.lanius.orchestrate.common.dto.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 外部转换实体类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-13 15:56:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("R_TRANSFORMATION")
public class ExternalTransformationDto {

    private Long idTransformation;

    private Long idDirectory;

    private String name;

    private String description;

    private String extendedDescription;

    private String transVersion;

    private Long transStatus;

    private Long idStepRead;

    private Long idStepWrite;

    private Long idStepInput;

    private Long idStepOutput;

    private Long idStepUpdate;

    private Long idDatabaseLog;

    private String tableNameLog;

    private String useBatchid;

    private String useLogfield;

    private Long idDatabaseMaxdate;

    private String tableNameMaxdate;

    private String fieldNameMaxdate;

    private Double offsetMaxdate;

    private Double diffMaxdate;

    private String createdUser;

    private LocalDateTime createdDate;

    private String modifiedUser;

    private LocalDateTime modifiedDate;

    private Long sizeRowset;
    
}

