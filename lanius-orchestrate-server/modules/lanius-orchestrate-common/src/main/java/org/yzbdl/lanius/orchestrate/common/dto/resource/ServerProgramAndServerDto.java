/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.resource;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.enums.ServerProgramStatusEnum;
import org.yzbdl.lanius.orchestrate.common.base.validated.Insert;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 主机节点和主机信息数据传输类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class ServerProgramAndServerDto {

    /**
     * 主键
     */
    @ApiModelProperty(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
    * 节点名称
    */
    @ApiModelProperty(value = "节点名称")
    @NotEmpty(message = "节点名称不能为空！", groups = {Insert.class})
    private String programName;

    /**
    * 主机id
    */
    @ApiModelProperty(value = "主机id")
    @NotNull(message = "主机id不能为空！", groups = {Insert.class})
    @JsonSerialize(using = ToStringSerializer.class)
    private Long serverId;

    /**
     * 主机Ip
     */
    @ApiModelProperty(value = "主机ip")
    @NotEmpty(message = "主机ip不能为空！", groups = {Insert.class})
    private String serverIp;

    /**
     * 主机端口
     */
    @ApiModelProperty(value = "主机端口")
    @NotNull(message = "主机ip端口不能为空！", groups = {Insert.class})
    private Integer serverPort;

    /**
     * 账号名称
     */
    @ApiModelProperty(value = "账号名称")
    @NotEmpty(message = "账号名称不能为空！", groups = {Insert.class})
    private String serverAccountName;

    /**
     * 密码
     */
    @NotEmpty(message = "账号密码不能为空！", groups = {Insert.class})
    @ApiModelProperty(value = "账号密码")
    private String serverPassword;

    /**
     * 主机名称
     */
    @ApiModelProperty(value = "主机名称")
    @NotEmpty(message = "主机名称不能为空！", groups = {Insert.class})
    private String serverName;

    /**
     * 节点用户名
     */
    @ApiModelProperty(value = "用户名")
    @NotEmpty(message = "节点用户名不能为空！", groups = {Insert.class})
    private String userName;

    /**
     * 节点用户密码
     */
    @ApiModelProperty(value = "用户密码")
    @NotEmpty(message = "节点用户密码不能为空！", groups = {Insert.class})
    private String password;

    /**
    * 主机端口
    */
    @NotNull(message = "主机节点不能为空！", groups = {Insert.class})
    @ApiModelProperty(value = "节点端口")
    private Integer programPort;

    /**
    * 程序类型 1-kettle 2-其他平台
    */
    @ApiModelProperty(value = "程序类型 1-kettle 2-其他平台")
    @NotNull(message = "程序类型不能为空！", groups = {Insert.class})
    private Integer category;

    /**
    * 备注
    */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
    * 节点配置 1-主节点 2-从节点
    */
    @ApiModelProperty(value = "节点配置 1-主节点 2-从节点")
    private Integer programConfig;

    /**
    * 节点状态 0-停止 1-运行中 2-异常
    * @see ServerProgramStatusEnum
    */
    @ApiModelProperty(value = "节点状态 0-停止 1-运行中 2-异常")
    private Integer status;

    /**
     * 服务器状态 状态 1 - 正常，0 - 异常
     */
    @ApiModelProperty(value = "状态 1 - 正常，0 - 异常")
    private Integer serverStatus;

    /**
    * 认证配置
    */
    @ApiModelProperty(value = "认证配置")
    private String authConfig;

    /**
    * 是否删除
    */
    @ApiModelProperty(value = "是否删除")
    private Boolean deleted;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    private Long orgId;

}

