package org.yzbdl.lanius.orchestrate.common.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 角色 entity
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 13:07
 */
@Data
@TableName(value = "lo_role")
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class RoleEntity extends BaseOrgEntity {

    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空")
    @Size(min = 1, max = 64, message = "长度不能超过64")
    @ApiModelProperty(value = "角色名称")
    @OprLabel
    private String roleName;

    /**
     * 是否删除
     */
    private Boolean deleted;

}
