/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.result;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2019/9/2
 */
public enum ResultTypeEnum {

    SERVICE_SUCCESS(200,"成功"),
    AUTHORIZED_FORBID(401,"没有访问权限"),
    SERVER_ERROR(500,"内部错误"),
    //WARNING(40001,"提示信息"),
    INVALID_TOKEN(40000,"无效的token"),
    NO_PERMISSION(40001,"无权限"),
    PARAM_ERROR(40005,"入参异常"),
    NO_CONTENT(204,"无内容"),
    SERVICE_ERROR(2001,"业务异常");

    private Integer code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ResultTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
