package org.yzbdl.lanius.orchestrate.common.dto.system;

import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 新增角色参数
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 16:58
 */
@Data
public class RoleInsertParamDto {
    @OprLabel
    @NotBlank(message = "角色名称不能为空！")
    @Size(max = 64)
    private String roleName;
    @NotNull
    private List<Long> permissionIds;

}
