/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.constant;

/**
 * 用户操作事件常量
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-13 17:16
 */
public class UserOprEventConstant {


	public static final String SYSTEM_USER_LOGIN = "用户登录";
	public static final String SYSTEM_USER_ADD = "用户新增";
	public static final String SYSTEM_USER_EDIT = "用户编辑";
	public static final String SYSTEM_USER_PASSWORD_RESET = "重置密码";
	public static final String SYSTEM_USER_DELETE = "用户删除";

	public static final String SYSTEM_ROLE_ADD = "角色新增";
	public static final String SYSTEM_ROLE_EDIT = "角色编辑";
	public static final String SYSTEM_ROLE_DELETE = "角色删除";

	/**
	 * 服务器操作标识
	 */
	public static final String SERVER_ADD = "主机配置数据新增";
	public static final String SERVER_UPDATE = "主机配置数据更新";
	public static final String SERVER_DELETE = "主机配置数据删除";

	/**
	 * 服务节点操作标识
	 */
	public static final String SERVER_PROGRAM_ADD = "服务节点数据新增";
	public static final String SERVER_PROGRAM_UPDATE = "服务节点数据更新";
	public static final String SERVER_PROGRAM_DELETE = "服务节点数据删除";

	/**
	 * 资源配置操作标识
	 */
	public static final String TASK_RESOURCE_CONFIG_ADD = "资源配置数据新增";
	public static final String TASK_RESOURCE_CONFIG_UPDATE = "资源配置数据更新";
	public static final String TASK_RESOURCE_CONFIG_DELETE = "资源配置数据删除";

	/**
	 * 任务资源分组操作标识
	 */
	public static final String TASK_RESOURCE_GROUP_ADD = "任务资源分组数据新增";
	public static final String TASK_RESOURCE_GROUP_UPDATE = "任务资源分组数据更新";
	public static final String TASK_RESOURCE_GROUP_DELETE = "任务资源分组数据删除";

	/**
	 * "任务资源操作标识
	 */
	public static final String TASK_RESOURCE_ADD = "任务资源数据新增";
	public static final String TASK_RESOURCE_UPDATE = "任务资源数据更新";
	public static final String TASK_RESOURCE_DELETE = "任务资源数据删除";


	/**
	 * "任务资源操作标识
	 */
	public static final String TASK_PLAN_ADD = "任务计划数据新增";
	public static final String TASK_PLAN_UPDATE = "任务计划数据更新";
	public static final String TASK_PLAN_DELETE = "任务计划数据删除";
	public static final String TASK_PLAN_EXECUTE = "手动执行任务计划";

	public static final String TASK_PLAN_GROUP_ADD = "增加任务计划分组";
	public static final String TASK_PLAN_GROUP_UPDATE = "修改任务计划分组";
	public static final String TASK_PLAN_GROUP_DELETE = "删除任务计划分组";







}
