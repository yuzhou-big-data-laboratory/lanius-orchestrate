/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022/2/21
 */
@Getter
@AllArgsConstructor
public enum UserStatusEnum {

    /**
     * 用户状态
     */
    NORMAL(1, "正常"),
    NO_ORGANIZATION(2, "未属于组织");

    Integer code;
    String value;

    public static UserStatusEnum findValueByCode(Integer code){
        return Optional.ofNullable(code).flatMap(param -> Stream.of(UserStatusEnum.values()).filter(w -> w.code.equals(param)).findFirst()).orElse(null);
    }

}
