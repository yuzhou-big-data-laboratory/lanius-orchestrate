/*
 *
 * Copyright(c) YuZhou Big Data Laboratory CO.,Ltd, 2022. All rights reserved
 */
package org.yzbdl.lanius.orchestrate.common.dto.system;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 组织管理员修改用户参数
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-08-10 16:10
 */
@Data
@Builder
public class UserOrgUpdateParamDTO {

    @NotBlank(message = "用户昵称不能为空")
    @Size(max = 64)
    private String nickName;

    List<UserOrgDTO> userOrgRelations;
}
