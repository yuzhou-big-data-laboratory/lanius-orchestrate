/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.jwt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * JWT token 工厂
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-18 16:03
 */
@Component
public class JwtFactory {
	/**
	 * 过期时间（小时为单位）
	 */
	private static int expireHour;

	/**
	 * 刷新码有效时间（小时为单位）
	 */
	private static int expireFreshHour;

	@Value("${jwt.normal.expire-hour}")
	public void setExpireHour(int hour){
		expireHour = hour;
	}

	@Value("${jwt.normal.expire-fresh-hour}")
	public void setExpireFreshHour(int hour){
		expireFreshHour = hour;
	}

	public static JwtToken token(){
		return new JwtToken(new JwtConfig() {

			@Override
			public String secret() {
				return JwtConst.NORMAL_SECRET;
			}

			@Override
			public Date expireDate() {
				return JwtHelper.toDateByHour(expireHour);
			}
		});
	}

	public static JwtToken freshToken(){
		return new JwtToken(new JwtConfig() {
			@Override
			public String secret() {
				return JwtConst.NORMAL_SECRET;
			}

			@Override
			public Date expireDate() {
				return JwtHelper.toDateByHour(expireFreshHour);
			}
		});
	}



}
