/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.enums;

import lombok.Getter;

/**
 * 服务节点程序类型
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-20 9:08
 */
@Getter
public enum ProgramCategoryEnum {

    /**
     * 服务节点程序类型
     */
    KETTLE(1, "Kettle平台"), OTHER(2, "其它平台");

    private final Integer code;
    private final String name;

    ProgramCategoryEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

}
