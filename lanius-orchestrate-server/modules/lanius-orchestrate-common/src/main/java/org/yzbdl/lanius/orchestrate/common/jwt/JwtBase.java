/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.Map;

/**
 * jwt 基类
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-18 15:37
 */
public class JwtBase{


    private final String secret;
    private final Date expireDate;


    public JwtBase(JwtConfig jwtConfig){
        this.secret = jwtConfig.secret();
        this.expireDate = jwtConfig.expireDate();
    }


    /**
     * 从声明对象转换生成令牌
     * @param claimMap 声明对象
     * @return
     */
    public String generateToken(Map<String,Object> claimMap) {
        return JWT.create()
                .withExpiresAt(expireDate)
                .withClaim(JwtConst.CLAIM_KEY,claimMap)
                .sign(Algorithm.HMAC256(secret));
    }

    public Date getExpireDate(){
        return expireDate;
    }
}
