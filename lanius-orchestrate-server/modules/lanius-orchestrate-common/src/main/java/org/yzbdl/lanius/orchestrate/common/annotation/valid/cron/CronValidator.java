/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.annotation.valid.cron;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.support.CronExpression;

import lombok.extern.slf4j.Slf4j;
import org.yzbdl.lanius.orchestrate.common.utils.cron.CronUtils;

/**
 * Cron表达式校验处理类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022/4/8 15:27
 */
@Slf4j
public class CronValidator implements ConstraintValidator<CronValid, Object> {

    /**
     * Cron表达式校验注解
     */
    private CronValid annotation;

    @Override
    public void initialize(CronValid constraintAnnotation) {
        annotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {

        if (Objects.isNull(value) || StringUtils.isBlank(value.toString())) {
            return true;
        }
        Boolean valid = CronUtils.isValid(value.toString());
        if (valid){
            return CronUtils.validLegalDate(value.toString());
        }
        return valid;
    }
}
