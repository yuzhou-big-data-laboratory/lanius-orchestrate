/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.entity.resource.TaskResourceConfigEntity;
import org.yzbdl.lanius.orchestrate.common.entity.resource.TaskResourceEntity;
import org.yzbdl.lanius.orchestrate.common.entity.task.TaskPlan;

/**
 * 任务编排关联的资源、配置信息
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-14 10:16
 */
@Data
public class TaskPlanResourceDTO extends TaskPlan {

	@ApiModelProperty(value = "任务资源信息")
	private TaskResourceEntity taskResourceEntity;

	@ApiModelProperty(value = "任务资源配置信息")
	private TaskResourceConfigEntity taskResourceConfigEntity;

}
