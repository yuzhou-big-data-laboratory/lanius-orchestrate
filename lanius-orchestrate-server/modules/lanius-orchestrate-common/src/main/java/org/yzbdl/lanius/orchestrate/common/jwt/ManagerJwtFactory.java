/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.jwt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 管理员jwt工厂
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-18 16:17
 */
@Component
public class ManagerJwtFactory {

	private static int expireHour;

	private static int expireFreshHour;

	@Value("${jwt.manager.expire-hour}")
	public void setExpireHour(int hour){
		expireHour = hour;
	}

	@Value("${jwt.manager.expire-fresh-hour}")
	public void setExpireFreshHour(int hour){
		expireFreshHour = hour;
	}

	public static JwtToken token(){
		return new JwtToken(new JwtConfig() {

			@Override
			public String secret() {
				return JwtConst.MANAGER_SECRET;
			}


			@Override
			public Date expireDate() {
				return JwtHelper.toDateByHour(expireHour);
			}
		});
	}

	public static JwtToken freshToken(){
		return new JwtToken(new JwtConfig() {

			@Override
			public String secret() {
				return JwtConst.MANAGER_SECRET;
			}

			@Override
			public Date expireDate() {
				return JwtHelper.toDateByHour(expireFreshHour);
			}
		});
	}
}
