package org.yzbdl.lanius.orchestrate.common.dto.resource;

import org.yzbdl.lanius.orchestrate.common.enums.DataBaseTypeEnum;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseEntity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 资源配置参数类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class TaskResourceConfigPageDto extends BaseEntity {
        
    /**
    * 连接名
    */    
    @ApiModelProperty(value = "连接名")
    private String connectName;
        
    /**
    * 链接地址
    */    
    @ApiModelProperty(value = "链接地址")
    private String connectUrl;
        
    /**
    * 链接账号
    */    
    @ApiModelProperty(value = "链接账号")
    private String connectAccount;
        
    /**
    * 链接密码
    */    
    @ApiModelProperty(value = "链接密码")
    private String connectPassword;
        
    /**
    * 备注
    */    
    @ApiModelProperty(value = "备注")
    private String remark;
                        
    /**
    * 组织id
    */    
    @ApiModelProperty(value = "组织id")
    private Long orgId;
        
    /**
    * 数据库类型 1 mysql
    * @see DataBaseTypeEnum
    */    
    @ApiModelProperty(value = "数据库类型")
    private Integer connectCategory;
        
    /**
    * 是否被删除
    */    
    @ApiModelProperty(value = "是否被删除")
    private Boolean deleted;

    /**
     * 状态 1 - 正常，0 - 异常
     */
    @ApiModelProperty(value = "状态 1 - 正常，0 - 异常")
    private Integer status;
    
}

