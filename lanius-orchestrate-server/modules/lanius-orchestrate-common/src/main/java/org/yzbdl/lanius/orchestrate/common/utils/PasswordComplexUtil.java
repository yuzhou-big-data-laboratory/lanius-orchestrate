/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * 密码复杂度
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-24 17:56
 */
public class PasswordComplexUtil {

	public static final String PW_PATTERN = "^(?![A-Za-z0-9]+$)(?![a-z0-9\\w]+$)(?![A-Za-z\\W]+$)(?![A-Z0-9\\W]+$)[a-zA-Z0-9\\W]+$";


//	public static void main(String[] args){
//		System.out.println(check("1233333333"));
//		System.out.println(check("ddddddddd"));
//		System.out.println(check("ddddddd11111111dd"));
//		System.out.println(check("AAAAAAA11111"));
//		System.out.println(check("AAAAAAA11111aaaaa"));
//		System.out.println(check("AAAAAaaa1aa@@@###"));
//		System.out.println(check("123ee@Je"));
//	}

	public static boolean check(String psw){
		return StringUtils.isNotBlank(psw)&&psw.matches(PW_PATTERN);
	}
}
