package org.yzbdl.lanius.orchestrate.common.dto.system;

import lombok.Builder;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.annotation.valid.password.ComplexValid;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 用户新增 参数
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 15:26
 */
@Data
@Builder
public class UserInsertParamDTO {
    @OprLabel
    @NotBlank(message = "用户名不能为空")
    @Size(max = 64)
    private String userName;
    @NotBlank(message = "用户昵称不能为空")
    @Size(max = 64)
    private String nickName;
    @NotBlank(message = "用户密码不能为空")
    private String password;
    private List<UserOrgDTO> userOrg;
}
