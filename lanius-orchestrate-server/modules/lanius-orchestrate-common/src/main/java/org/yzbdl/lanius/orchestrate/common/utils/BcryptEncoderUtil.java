/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 密码编码工具
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-21 14:42
 */
public class BcryptEncoderUtil {

	/**
	 * 编码
	 * @param psw 编码内容
	 * @return 编码结果
	 */
	public static String encode(String psw){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(psw);
	}


	/**
	 * 密码比对
	 * @param rawPassword 原有密码
	 * @param encodedPassword 编码后的密码
	 * @return 是否匹配
	 */
	public static boolean match(CharSequence rawPassword, String encodedPassword){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.matches(rawPassword, encodedPassword);
	}


}
