/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.base.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.yzbdl.lanius.orchestrate.common.result.page.PageParam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 基础BaseController
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @version 1.0
 * @date 2022-04-07 10:30
 */
public class BaseController {

    /**
     * 构建mybatis分页插件对象
     *
     * @param pageParam 分页参数
     * @return 分页对象
     */
    public Page buildPage(PageParam pageParam) {
        return new Page(pageParam.getPageNum(), pageParam.getPageSize());
    }

    /**
     * 构建mybatis分页插件对象
     *
     * @param page
     *        页码
     * @param size
     *        每页条数
     * @return 分页对象
     */
    public Page buildIPage(Integer page, Integer size) {
        return new Page(page, size);
    }

    /**
     * 解决字符串转date失败
     *
     * @param request 请求
     * @param binder  ServletRequestDataBinder对象
     */
    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

}
