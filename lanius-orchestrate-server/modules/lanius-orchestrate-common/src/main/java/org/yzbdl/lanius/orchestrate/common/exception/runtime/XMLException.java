package org.yzbdl.lanius.orchestrate.common.exception.runtime;

public class XMLException extends Exception {
    public static final long serialVersionUID = -8246477781266195431L;

    public XMLException() {
    }

    public XMLException(String message) {
        super(message);
    }

    public XMLException(Throwable cause) {
        super(cause);
    }

    public XMLException(String message, Throwable cause) {
        super(message, cause);
    }
}