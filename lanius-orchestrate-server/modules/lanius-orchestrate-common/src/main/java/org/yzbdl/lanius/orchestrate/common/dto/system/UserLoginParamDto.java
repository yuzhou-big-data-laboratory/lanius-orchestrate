/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.system;

import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 用户登录参数
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 14:12
 */
@Data
public class UserLoginParamDto {
    /**
     * 登录用户名
     */
    @OprLabel
    @NotBlank(message="用户名不能为空！")
    @Size(max = 64)
    private String userName;
    /**
     * 访问密码
     */
    @NotBlank(message="密码不能为空！")
    @Size(max = 128)
    private String password;

    @NotBlank(message="验证码不能为空！")
    @Size(max = 32)
    private String code;
}
