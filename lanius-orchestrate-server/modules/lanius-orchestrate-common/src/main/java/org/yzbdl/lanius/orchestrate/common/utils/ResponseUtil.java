/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.utils;

import cn.hutool.json.JSONUtil;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.result.ResultTypeEnum;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 请求响应
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-21 17:10
 */
public class ResponseUtil {
	/**
	 * 无接口权限
	 * @param response 回复
	 */
	public static void noPermissionResponse(HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setStatus(401);
		response.setContentType("application/json; charset=utf-8");
		String s = JSONUtil.toJsonStr(ResultObj.error(ResultTypeEnum.NO_PERMISSION,"您没有该功能权限！请联系组织管理员调整！"));
		try (PrintWriter out = response.getWriter()) {
			out.append(s);
		} catch (Exception ignored) {}
	}


	/**
	 *
	 * @param response
	 */
	public static void invalidToken(HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setStatus(401);
		response.setContentType("application/json; charset=utf-8");
		String s = JSONUtil.toJsonStr(ResultObj.error(ResultTypeEnum.INVALID_TOKEN,"token失效！"));
		try (PrintWriter out = response.getWriter()) {
			out.append(s);
		} catch (Exception ignored) {}
	}

}
