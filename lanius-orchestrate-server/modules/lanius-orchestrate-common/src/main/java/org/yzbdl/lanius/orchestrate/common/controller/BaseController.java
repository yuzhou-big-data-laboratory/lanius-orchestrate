/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

/**
 * 基础接口类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022/3/25 9:54
 */
public class BaseController<S extends IService<E>, E> {

    @Autowired
    protected S baseService;

    @ApiOperation(value = "[通用方法]通过主键获取一条对应实体类的数据库记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "实体主键", dataTypeClass = Integer.class, required = true)
    })
    @GetMapping("{id}")
    public ResultObj<E> getById(@PathVariable("id") String id) {
        E entity = baseService.getById(id);
        return ResultObj.success(entity);
    }

    @ApiOperation(value = "[通用方法]分页获取数据库实体记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "path", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页条目数", paramType = "path", dataTypeClass = Integer.class, required = true)
    })
    @GetMapping("page/{pageNo}/{pageSize}")
    public ResultObj<Page<E>> page(@PathVariable("pageNo") Integer pageNo, @PathVariable("pageSize") Integer pageSize) {
        Page<E> page = new Page<>(pageNo, pageSize);
        return ResultObj.success(baseService.page(page, Wrappers.emptyWrapper()));
    }

    @ApiOperation(value = "[通用方法]插入一条对应实体类的数据库记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "entity", value = "实体对象", dataTypeClass = Object.class, required = true)
    })
    @PostMapping
    public ResultObj<String> save(@RequestBody E entity) {
        baseService.save(entity);
        return ResultObj.success();
    }

    @ApiOperation(value = "[通用方法]更新一条对应实体类的数据库记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "entity", value = "实体对象", dataTypeClass = Object.class, required = true)
    })
    @PutMapping
    public ResultObj<String> updateById(@RequestBody E entity) {
        baseService.updateById(entity);
        return ResultObj.success();
    }


    @ApiOperation(value = "[通用方法]通过主键删除一条对应实体类的数据库记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "实体主键", dataTypeClass = Integer.class, required = true)
    })
    @DeleteMapping("{id}")
    public ResultObj<String> deleteById(@PathVariable("id") Integer id) {
        baseService.removeById(id);
        return ResultObj.success();
    }


    
}
