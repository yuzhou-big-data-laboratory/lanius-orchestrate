/*
 * Create on 2022/4/8 13:34
 *
 * Copyright(c) YuZhou Big Data Laboratory CO.,Ltd, 2022. All rights reserved
 */
package org.yzbdl.lanius.orchestrate.common.vo.system;

import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.entity.system.OrgEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 组织新增修改实体类dto
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 13:34
 */
@Data
public class OrgParamDTO {
    @NotBlank(message = "组织管理不能为空")
    @Size(max = 64)
    private String orgName;

    public OrgEntity toOrgEntity(){
        return OrgEntity.builder().orgName(orgName).frozen(false).build();
    }
}
