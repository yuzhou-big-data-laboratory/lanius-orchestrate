/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.resource;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.yzbdl.lanius.orchestrate.common.enums.ResourceTypeEnum;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-13 11:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskResourceDto {

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;

	/**
	 * 资源类型  1作业 2转换
	 * @see ResourceTypeEnum
	 */
	@ApiModelProperty(value = "资源类型 1作业 2转换")
	@NotNull(message = "资源类型不能为空！")
	private Integer resourceType;

	/**
	 * 备注
	 */
	@Size(max = 64, message = "备注不能超出512位字符！")
	@ApiModelProperty(value = "备注")
	private String remark;

	/**
	 * 资源配置
	 */
	@ApiModelProperty(value = "资源配置")
	private String resourceContent;

	/**
	 * 任务资源账户名
	 */
	@ApiModelProperty(value = "任务资源账户名")
	@NotNull(message = "任务资源账户名不能为空！")
	private String resourceAccountName;

	/**
	 * 任务资源密码
	 */
	@ApiModelProperty(value = "任务资源密码")
	@NotNull(message = "任务资源密码不能为空！")
	private String resourcePassword;

	/**
	 * 资源配置id
	 */
	@ApiModelProperty(value = "资源配置id")
	@NotNull(message = "资源配置id不能为空！")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long resourceConfigId;

	/**
	 * 资源组id
	 */
	@ApiModelProperty(value = "资源组id")
	@NotNull(message = "资源组id不能为空！")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long groupId;

	/**
	 * 组织id
	 */
	@ApiModelProperty(value = "组织id")
	private Long orgId;

	/**
	 * 资源名称
	 */
	@OprLabel
	@ApiModelProperty(value = "资源名称")
	@NotEmpty(message = "资源名称不能为空！")
	@Size(max = 64, message = "节点名称不能超出64位字符！")
	private String resourceName;

	/**
	 * 资源库名称
	 */
	@ApiModelProperty(value = "资源库名称")
	private String configConnectName;

	/**
	 * 是否被删除
	 */
	@ApiModelProperty(value = "是否被删除")
	private Boolean deleted;


	/**
	 * 创建人
	 */
	@ApiModelProperty(value = "创建人")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long createId;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	/**
	 * 修改人
	 */
	@ApiModelProperty(value = "修改人")
	private Long modified;

	/**
	 * 上次修改
	 */
	@ApiModelProperty(value = " 上次修改")
	private LocalDateTime lastModify;

	/**
	 * 资源路径
	 */
	@ApiModelProperty(value = " 资源路径")
	@NotEmpty(message = "资源路径不能为空！")
	private String resourcePath;

}
