/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package org.yzbdl.lanius.orchestrate.common.result.page;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Min;

/**
 * 分页查询参数
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @version 1.0
 * @date 2022-04-07 10:20
 */
@Data
public class PageParam<T> {

    @Valid
    @ApiModelProperty(value = "查询参数")
    private T param;

    @ApiModelProperty(value = "页码")
    @Min(value = 1, message = "页码不能小于1")
    private Integer pageNum = 1;

    @ApiModelProperty(value = "每页数据条数")
    @Min(value = 1, message = "每页展示数据条数不能小于1")
    private Integer pageSize = 15;


}
