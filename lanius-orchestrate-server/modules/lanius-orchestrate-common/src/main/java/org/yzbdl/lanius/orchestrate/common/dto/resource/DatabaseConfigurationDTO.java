/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.resource;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * 数据库链接配置
 *
 @author zhuhongji@yzbdl.ac.cn
 * @date 2022-09-14 9:48
 */
@Data
public class DatabaseConfigurationDTO {

    /**
     * 数据库IP
     */
    @ApiModelProperty(value = "数据库ip")
    @NotBlank(message = "数据库ip不能为空！")
    private String ip;

    /**
     * 数据库端口
     */
    @ApiModelProperty(value = "数据库端口")
    @Min(value = 1, message = "数据库端口请输入1-65535的正整数！")
    @Max(value = 65535, message = "数据库端口请输入1-65535的正整数！")
    private Integer port;

    /**
     * 数据库名称
     */
    @ApiModelProperty(value = "数据库名称")
    @NotBlank(message = "数据库名称不能为空！")
    private String dbName;

    /**
     * 数据库参数
     */
    @ApiModelProperty(value = "数据库参数")
    private Map<String,String> params;

}
