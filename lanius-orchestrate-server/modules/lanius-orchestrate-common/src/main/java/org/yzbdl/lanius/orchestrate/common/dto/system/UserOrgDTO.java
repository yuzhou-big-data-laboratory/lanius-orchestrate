/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.dto.system;

import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserOrgEntity;

/**
 * 用户组织关联用到的参数
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 15:26
 */
@Data
public class UserOrgDTO {
    private Long orgId;
    private Boolean chief;

    public UserOrgEntity toUserOrgEntity(Long userId){
        return UserOrgEntity.builder()
                .userId(userId)
                .orgId(orgId)
                .orgChief(chief)
                .build();
    }
}
