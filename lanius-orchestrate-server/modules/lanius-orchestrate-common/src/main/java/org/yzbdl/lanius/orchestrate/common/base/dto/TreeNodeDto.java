/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.base.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 树Dto
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-11 11:12
 */
@Data
@SuperBuilder
public class TreeNodeDto {

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	@JsonSerialize(using = ToStringSerializer.class)
	protected Long id;

	/**
	 * 父节点id
	 */
	@ApiModelProperty(value = "父节点id")
	@JsonSerialize(using = ToStringSerializer.class)
	protected Long parentId;

	/**
	 * 子节点数据
	 */
	@ApiModelProperty(value = "子节点数据")
	protected List<TreeNodeDto> children;

	public void addTreeNode(TreeNodeDto node){
		if(Objects.isNull(this.children)){
			this.children = new ArrayList<>();
		}
		children.add(node);
	}

}
