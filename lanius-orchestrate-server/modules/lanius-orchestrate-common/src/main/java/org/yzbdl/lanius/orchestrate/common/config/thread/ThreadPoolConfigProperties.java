/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.config.thread;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 线程池参数配置类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-13 10:52
 */
@ConfigurationProperties(prefix = "common.thread")
@Component
@Data
public class ThreadPoolConfigProperties {

	/**
	 * 核心线程
	 */
	private Integer coreSize;

	/**
	 * 最大线程
	 */
	private Integer maxSize;

	/**
	 * 空闲线程等工作的超时时间
	 */
	private Integer keepAliveTime;

}
