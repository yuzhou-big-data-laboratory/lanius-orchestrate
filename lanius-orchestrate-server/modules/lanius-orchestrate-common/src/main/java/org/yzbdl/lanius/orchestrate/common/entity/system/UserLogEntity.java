package org.yzbdl.lanius.orchestrate.common.entity.system;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;

import java.time.LocalDateTime;

/**
 * 用户日志 Entity
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 13:11
 */
@Data
@TableName(value = "lo_user_log")
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UserLogEntity extends BaseOrgEntity {
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;
    private String url;
    private String method;
    private String eventName;
    private String eventContent;
}
