/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.entity.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;
import org.yzbdl.lanius.orchestrate.common.base.validated.Insert;
import org.yzbdl.lanius.orchestrate.common.enums.ServerProgramStatusEnum;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 主机节点
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("lo_server_program")
public class ServerProgramEntity extends BaseOrgEntity {

    /**
    * 节点名称
    */
    @OprLabel
    @ApiModelProperty(value = "节点名称")
    @NotEmpty(message = "节点名称不能为空！", groups = {Insert.class})
    @Size(max = 64, message = "节点名称不能超出64位字符！")
    private String programName;

    /**
    * 主机id
    */
    @ApiModelProperty(value = "主机id")
    @NotNull(message = "主机id不能为空！", groups = {Insert.class})
    @JsonSerialize(using = ToStringSerializer.class)
    private Long serverId;

    /**
    * 主机端口
    */
    @ApiModelProperty(value = "主机端口")
    @NotNull(message = "主机端口不能为空！", groups = {Insert.class})
    private Integer programPort;

    /**
    * 程序类型 1-kettle 2-其他平台
    */
    @ApiModelProperty(value = "程序类型 1-kettle 2-其他平台")
    @NotNull(message = "程序类型不能为空！", groups = {Insert.class})
    private Integer category;

    /**
    * 备注
    */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
    * 节点配置 1-主节点 2-从节点
    */
    @ApiModelProperty(value = "节点配置 1-主节点 2-从节点")
    private Integer programConfig;

    /**
    * 节点状态  0-停止 1-运行中 2-异常
    * @see ServerProgramStatusEnum
    */
    @ApiModelProperty(value = "节点状态 0-停止 1-运行中 2-异常")
    private Integer status;

    /**
    * 认证配置
    */
    @ApiModelProperty(value = "认证配置")
    private String authConfig;

    /**
    * 是否删除
    */
    @ApiModelProperty(value = "是否删除")
    private Boolean deleted;

}

