/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.entity.task;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdOrgFieldEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


/**
 * 任务编排分组表
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 15:58
 */
@Data
@TableName(value = "lo_task_plan_group")
public class TaskPlanGroup extends IdOrgFieldEntity {

    @OprLabel
    @NotBlank(message = "任务分组名称不能为空")
    @Size(min = 0, max = 64, message = "任务分组名称字符长度不能超过64")
    @ApiModelProperty(value = "任务分组名称")
    @TableField(value = "group_name")
    private String groupName;

    @ApiModelProperty(value = "父级任务分组ID")
    @TableField(value = "pid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pid;

}
