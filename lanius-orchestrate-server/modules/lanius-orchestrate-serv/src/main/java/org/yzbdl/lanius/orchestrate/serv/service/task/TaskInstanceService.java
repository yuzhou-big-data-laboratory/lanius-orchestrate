/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.task;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.dto.task.IncrLogQueryDTO;
import org.yzbdl.lanius.orchestrate.common.dto.task.TaskInstanceQueryDTO;
import org.yzbdl.lanius.orchestrate.common.dto.task.TaskInstanceResourceDTO;
import org.yzbdl.lanius.orchestrate.common.entity.task.TaskInstance;
import org.yzbdl.lanius.orchestrate.common.vo.task.IncrLogVO;
import org.yzbdl.lanius.orchestrate.common.vo.task.ScheduleTaskInstanceVO;
import org.yzbdl.lanius.orchestrate.common.vo.task.TaskInstanceVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 任务实例service
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 9:03
 */
public interface TaskInstanceService extends IService<TaskInstance> {

    /**
     * 分页查询任务实例列表
     *
     * @param page             分页信息
     * @param instanceQueryDTO 查询条件
     * @return 结果信息
     */
    IPage<TaskInstanceVO> queryPage(Page<TaskInstanceVO> page, TaskInstanceQueryDTO instanceQueryDTO);

    /**
     * 根据任务ID检查是否存在运行中的任务实例
     *
     * @param taskPlanId 任务编排ID
     * @return true/false
     */
    Boolean checkInstanceForRun(Long taskPlanId);

    /**
     * 获取调度总览之任务实例
     *
     * @return 结果信息
     */
    List<ScheduleTaskInstanceVO> getSchedulerViewStatistics();

    /**
     * 获取任务实例信息，忽略租户
     *
     * @param taskInstanceId 任务实例ID
     * @return 任务实例
     */
    TaskInstance getByIdIgnoreTenantId(Long taskInstanceId);

    /**
     * 获取任务实例资源信息
     *
     * @param taskInstanceId 任务实例ID
     * @return 任务实例资源信息
     */
    TaskInstanceResourceDTO getTaskInstanceResourceIgnoreTenantId(Long taskInstanceId);

    /**
     * 获取图片
     *
     * @param taskInstanceId  任务实例ID
     * @param serverProgramId 服务节点ID
     * @return base64字符串
     */
    String getTransImageById(Long taskInstanceId, Long serverProgramId);

    /**
     * 读取任务增量日志
     *
     * @param response        response
     * @param page            页码
     * @param size            行数
     * @param incrLogQueryDTO 查询参数
     * @return 文本指针
     */
    IncrLogVO readIncrLog(HttpServletResponse response, Long page, Long size, IncrLogQueryDTO incrLogQueryDTO);

    /**
     * 下载日志文件
     * @param response response
     * @param taskPlanId 任务计划id
     * @param taskInstanceId 任务实例id
     * @return 日志文本
     */
    void downloadIncrLog(HttpServletResponse response, Long taskPlanId, Long taskInstanceId);

    /**
     * 根据id更新实例，忽略租户ID
     *
     * @param taskInstance 实例
     * @return 影响条数
     */
    int updateByIdIgnoreTenantId(TaskInstance taskInstance);

    /**
     * 根据任务计划ID删除实例记录
     *
     * @param orgId       组织ID
     * @param taskGroupId 分组ID
     * @param taskPlanId  任务计划ID
     * @throws Exception 任务异常
     */
    void deletedByTaskPlanId(Long orgId, Long taskGroupId, Long taskPlanId) throws Exception;
}
