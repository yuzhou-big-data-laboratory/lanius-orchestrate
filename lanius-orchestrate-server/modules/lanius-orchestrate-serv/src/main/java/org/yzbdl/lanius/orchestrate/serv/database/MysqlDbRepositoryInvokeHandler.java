/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.database;

import cn.hutool.db.ds.simple.SimpleDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.yzbdl.lanius.orchestrate.common.enums.DataBaseTypeEnum;

import java.util.Objects;

/**
 * 数据库资源库处理抽象类
 *
 * @author hujian@yzbdl.ac.cn
 * @date 2022-09-20 16:42
 */
@Slf4j
@Component
public class MysqlDbRepositoryInvokeHandler extends AbstractDbRepositoryInvokeHandler implements InitializingBean {


    /**
     * Mysql数据库jdbcUrl连接超时关键字
     */
    private static final String MYSQL_CONNECT_TIME_OUT_KEY_WORDS = "connectTimeout";

    /**
     * 获取Jdbc模板
     * @param jdbcDriverClassName 驱动类名称
     * @return Jdbc模板
     */
    @Override
    public String getJdbcTemplate(String jdbcDriverClassName) {
        return DataBaseTypeEnum.MYSQL.getJdbcUrlTemplate();
    }

    /**
     * 测试数据库连通性；超时时间为10s
     * @param url url
     * @param username 用户名
     * @param password 密码
     * @return 成功与否
     */
    @Override
    public Boolean testConnection(String url, String username, String password) {
        SimpleDataSource simpleDataSource = null;
        try {
            if(!url.contains(MYSQL_CONNECT_TIME_OUT_KEY_WORDS)) {
                String prefix = url.contains("?") ? "&" : "?" ;
                url = url + prefix + "connectTimeout=" + CONNECT_DB_TIME_OUT;
            }
            simpleDataSource = new SimpleDataSource(url, username, password);
            return simpleDataSource
                        .getConnection().isValid(CONNECT_DB_SOCKET_TIME_OUT);
        } catch (Exception e) {
            log.error("数据库{}连接超时！", url, e);
            return false;
        } finally {
            try{
                if(Objects.nonNull(simpleDataSource)){
                    simpleDataSource.close();
                }
            }
            catch (Exception e){
                log.error("数据库{}关闭超时！", url, e);
            }
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        DbRepositoryFactory.register(DataBaseTypeEnum.MYSQL.getCode(), this);
    }
}
