/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.task;

import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.entity.task.TaskStepLog;
import org.yzbdl.lanius.orchestrate.common.vo.task.TaskStepLogVO;

import java.util.List;

/**
 * 任务步骤日志服务
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-19 14:16
 */
public interface TaskStepLogService extends IService<TaskStepLog> {


	/**
	 * 获取步骤度量
	 *
	 * @param taskInstanceId
	 *        任务实例ID
	 * @return
	 *        结果数据
	 */
	List<TaskStepLogVO> getStepLogByTaskInstanceId(Long taskInstanceId);
}
