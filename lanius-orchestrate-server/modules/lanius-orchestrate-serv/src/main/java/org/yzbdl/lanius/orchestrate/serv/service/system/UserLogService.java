/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.dto.system.UserLogQueryDto;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserLogEntity;
import org.yzbdl.lanius.orchestrate.common.vo.system.UserLogVo;

import java.util.List;

/**
 * 用户日志接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-13 15:32
 */
public interface UserLogService extends IService<UserLogEntity> {

	/**
	 * 获取日志事件
	 * @return 返回事件名称
	 */
	List<String> getAllEventName();

	/**
	 * 获取用户日志分页列表
	 * @param page 分页对象
	 * @param userLogQueryDto 用户日志条件
	 * @return 分页问题
	 */
	Page<UserLogVo> listUserLogsPage(IPage<UserLogEntity> page, UserLogQueryDto userLogQueryDto);
}
