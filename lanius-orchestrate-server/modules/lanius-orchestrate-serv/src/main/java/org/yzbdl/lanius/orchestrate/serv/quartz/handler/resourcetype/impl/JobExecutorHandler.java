/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.quartz.handler.resourcetype.impl;

import org.springframework.stereotype.Component;
import org.yzbdl.lanius.orchestrate.common.enums.ResourceTypeEnum;
import org.yzbdl.lanius.orchestrate.serv.quartz.handler.resourcetype.ResourceTypeFactory;
import org.yzbdl.lanius.orchestrate.serv.quartz.handler.resourcetype.ResourceTypeInvokerHandler;

import java.util.Arrays;
import java.util.List;

/**
 * Kettle平台：作业任务调度执行器
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-14 10:55
 */
@Component
public class JobExecutorHandler implements ResourceTypeInvokerHandler {

    /**
     * 检查网址状态
     */
    private static final String CHECK_STATUS_URL = "jobStatus";

    /**
     * 作业图片请求地址
     */
    private static final String JOB_IMAGE_URL = "repoFileImage";

    @Override
    public String getCheckStatusUrl() {
        return CHECK_STATUS_URL;
    }

    @Override
    public List<String> getRequestParam() {
        //TODO  lanius 版更新 runRepoJob
        return Arrays.asList("runRepoJob", "job", "removeJob");
    }

    @Override
    public String getImageUrL() {
        return JOB_IMAGE_URL;
    }

    @Override
    public void afterPropertiesSet() {
        ResourceTypeFactory.register(ResourceTypeEnum.KETTLE_OBJ.getCode().toString(), this);
    }
}
