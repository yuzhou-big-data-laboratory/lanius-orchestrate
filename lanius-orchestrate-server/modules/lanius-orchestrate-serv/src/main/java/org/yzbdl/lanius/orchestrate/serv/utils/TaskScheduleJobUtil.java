/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.utils;

/**
 * 调度任务工具类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-12 13:35
 */
public class TaskScheduleJobUtil {

    /**
     * JOB分组前缀
     */
    private static final String JOB_GROUP_NAME_PRE = "job_group_org_id_";

    /**
     * 构建任务编排JOB分组名称
     *
     * @param orgId
     *        组织ID
     * @return
     *        JOB分组名称
     */
    public static String buildJobGroupName(Long orgId) {
        StringBuilder builder = new StringBuilder(JOB_GROUP_NAME_PRE);
        builder.append(orgId);
        return builder.toString();
    }

}
