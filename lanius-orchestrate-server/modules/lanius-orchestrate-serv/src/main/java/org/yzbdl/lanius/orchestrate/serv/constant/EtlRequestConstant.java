/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.constant;

/**
 * ETL请求常量定义
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-15 14:15
 */
public interface EtlRequestConstant {

    /**
     * 请求头：资源库名称
     */
    String HEADER_REPO_NAME = "repositoryName";

    /**
     * 请求头：资源库描述
     */
    String HEADER_REPO_DESC = "description";

    /**
     * 数据库资源库类型 值：KettleDatabaseRepository
     */
    String HEADER_REPO_ID = "repositoryId";
    /**
     * 请求头：资源库配置json
     */
    String HEADER_REPO_CONFIG_JSON = "repConfigJson";

    /**
     * 认证token
     */
    String HEADER_AUTH = "Authorization";

    /**
     * 请求头：数据库连接名
     */
    String HEADER_DB_CONN_NAME = "name";

    /**
     * 请求头：数据库连接主机
     */
    String HEADER_DB_CONN_HOST = "host";

    /**
     * 请求头：数据库类型
     */
    String HEADER_DB_TYPE = "type";

    /**
     * 请求头：数据库名
     */
    String HEADER_DB_NAME = "db";

    /**
     * 请求头：数据库端口号
     */
    String HEADER_DB_PORT = "port";

    /**
     * 请求头：数据库账号
     */
    String HEADER_DB_USER = "user";

    /**
     * 请求头：数据库密码
     */
    String HEADER_DB_PASS = "pass";

    /**
     * 请求头：额外的选项
     */
    String HEADER_DB_EO = "dbExtraOption";

    /**
     * 资源库名称
     */
    String BODY_REPO_NAME = "rep";

    /**
     * 资源库账号
     */
    String BODY_REPO_USER = "user";

    /**
     * 资源库密码
     */
    String BODY_REPO_PASS = "pass";

    /**
     * cart唯一标识ID
     */
    String CARTE_OBJ_ID = "carteObjectId";


}
