/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.yzbdl.lanius.orchestrate.common.entity.system.PermissionEntity;
import org.yzbdl.lanius.orchestrate.common.vo.system.PermissionMenuUrlVo;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.PermissionMapper;
import org.yzbdl.lanius.orchestrate.serv.service.system.PermissionService;

import java.util.List;

/**
 * 权限服务实现
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-11 09:22
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, PermissionEntity> implements PermissionService {

	/**
	 * 根据权用户id和组织id查询权限
	 * @param userId 用户id
	 * @return 权限列表
	 */
	@Override
	public List<PermissionEntity> queryPermissionsByUserId(Long userId){
		return this.baseMapper.queryPermissionsByUserId(userId);
	};

	/**
	 * 根据菜单查询权限
	 * @param menuId 菜单id
	 * @return 权限列表
	 */
	@Override
	public List<PermissionEntity> listPermissionByMenuId(Long menuId){
		return this.list(new QueryWrapper<PermissionEntity>()
					.lambda().eq(PermissionEntity::getMenuId,menuId));
	};


	@Override
	public List<PermissionMenuUrlVo> listPermissionsWithMenuUrl(){
		return this.baseMapper.listPermissionsWithMenuUrl();
	}



}
