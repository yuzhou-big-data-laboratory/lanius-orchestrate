/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.resource.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ResourceStatisticDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.StatusStatisticDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.TaskResourceTypeStatisticDto;
import org.yzbdl.lanius.orchestrate.common.enums.ResourceDataStatusEnum;
import org.yzbdl.lanius.orchestrate.common.enums.ResourceTypeEnum;
import org.yzbdl.lanius.orchestrate.common.enums.ServerProgramStatusEnum;
import org.yzbdl.lanius.orchestrate.serv.mapper.resource.ServerMapper;
import org.yzbdl.lanius.orchestrate.serv.mapper.resource.ServerProgramMapper;
import org.yzbdl.lanius.orchestrate.serv.mapper.resource.TaskResourceConfigMapper;
import org.yzbdl.lanius.orchestrate.serv.mapper.resource.TaskResourceMapper;
import org.yzbdl.lanius.orchestrate.serv.service.resource.ResourceStatisticService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-18 11:31
 */
@Service
@AllArgsConstructor
public class ResourceStatisticServiceImpl implements ResourceStatisticService {

	private final ServerMapper serverMapper;

	private final ServerProgramMapper serverProgramMapper;

	private final TaskResourceConfigMapper taskResourceConfigMapper;

	private final TaskResourceMapper taskResourceMapper;

	@Override
	public ResourceStatisticDto getStatusStatistic() {
		Long serverNormalCount;
		Long serverAbnormalCount;
		Long serverProgramStoppedCount;
		Long serverProgramRunningCount;
		Long serverProgramAbnormalCount;
		Long resourceConfigNormalCount;
		Long resourceConfigAbnormalCount;
		Long jobCount;
		Long ktrCount;

		// 主机
		List<StatusStatisticDto> serverStatusGroupCount = serverMapper.getStatusGroupCount();
		// 主机链接正常数
		serverNormalCount = this.getStatusCount(serverStatusGroupCount, ResourceDataStatusEnum.NORMAL.getCode());
		// 主机链接异常数
		serverAbnormalCount = this.getStatusCount(serverStatusGroupCount, ResourceDataStatusEnum.ABNORMAL.getCode());

		// 服务节点
		List<StatusStatisticDto> serverProgramStatusGroupCount = serverProgramMapper.getStatusGroupCount();
		// 服务节点进行中数量
		serverProgramStoppedCount = this.getStatusCount(serverProgramStatusGroupCount, ServerProgramStatusEnum.STOPPED.getCode());
		// 服务节点已停止数量
		serverProgramRunningCount = this.getStatusCount(serverProgramStatusGroupCount, ServerProgramStatusEnum.RUNNING.getCode());
		// 服务节点异常数量
		serverProgramAbnormalCount = this.getStatusCount(serverProgramStatusGroupCount, ServerProgramStatusEnum.ABNORMAL.getCode());

		// 资源库
		List<StatusStatisticDto> taskConfigStatusGroupCount = taskResourceConfigMapper.getStatusGroupCount();
		// 资源库链接正常数量
		resourceConfigNormalCount = this.getStatusCount(taskConfigStatusGroupCount, ResourceDataStatusEnum.NORMAL.getCode());
		// 资源库链接异常数量
		resourceConfigAbnormalCount = this.getStatusCount(taskConfigStatusGroupCount, ResourceDataStatusEnum.ABNORMAL.getCode());

		// 任务资源
		List<TaskResourceTypeStatisticDto> taskResourceCount = taskResourceMapper.getStatusGroupCount();
		Map<Integer, Long> taskResourceCountCountMap = taskResourceCount.stream()
				.collect(Collectors.toMap(TaskResourceTypeStatisticDto::getResourceType, TaskResourceTypeStatisticDto::getCount));
		// 任务资源作业数量
		jobCount = Optional.ofNullable(taskResourceCountCountMap.get(ResourceTypeEnum.KETTLE_OBJ.getCode())).orElse(0L);
		// 任务资源转换数量
		ktrCount = Optional.ofNullable(taskResourceCountCountMap.get(ResourceTypeEnum.KETTLE_KTR.getCode())).orElse(0L);

		return ResourceStatisticDto.builder()
				.serverNormalCount(serverNormalCount)
				.serverAbnormalCount(serverAbnormalCount)
				.serverProgramStoppedCount(serverProgramStoppedCount)
				.serverProgramRunningCount(serverProgramRunningCount)
				.serverProgramAbnormalCount(serverProgramAbnormalCount)
				.resourceConfigNormalCount(resourceConfigNormalCount)
				.resourceConfigAbnormalCount(resourceConfigAbnormalCount)
				.jobCount(jobCount)
				.ktrCount(ktrCount)
				.build();
	}

	/**
	 * 获取统计计数
	 * @param statusGroupCount 计数数组
	 * @return count
	 */
	private Long getStatusCount(List<StatusStatisticDto> statusGroupCount, Integer status) {
		Map<Long, Long> map = statusGroupCount.stream().collect(Collectors.toMap(StatusStatisticDto::getStatus, StatusStatisticDto::getCount));
		return Optional.ofNullable(map.get(Long.valueOf(status))).orElse(0L);
	}

}
