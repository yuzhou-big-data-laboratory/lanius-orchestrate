/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.quartz.handler.resourcetype.impl;

import org.springframework.stereotype.Component;
import org.yzbdl.lanius.orchestrate.common.enums.ResourceTypeEnum;
import org.yzbdl.lanius.orchestrate.serv.quartz.handler.resourcetype.ResourceTypeFactory;
import org.yzbdl.lanius.orchestrate.serv.quartz.handler.resourcetype.ResourceTypeInvokerHandler;

import java.util.Arrays;
import java.util.List;

/**
 * Kettle平台：转换任务调度执行器
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-14 11:00
 */
@Component
public class TransExecutorHandler implements ResourceTypeInvokerHandler {

    /**
     * 检查网址状态
     */
    private static final String CHECK_STATUS_URL = "transStatus";

    /**
     * 作业图片请求地址
     */
    private static final String KTR_IMAGE_URL = "repoFileImage";

    @Override
    public String getCheckStatusUrl() {
        return CHECK_STATUS_URL;
    }

    @Override
    public List<String> getRequestParam() {
        //TODO  lanius 版更新 runRepoTrans
        return Arrays.asList("runRepoTrans", "trans", "removeTrans");
    }

    @Override
    public String getImageUrL() {
        return KTR_IMAGE_URL;
    }


    @Override
    public void afterPropertiesSet() {
        ResourceTypeFactory.register(ResourceTypeEnum.KETTLE_KTR.getCode().toString(), this);
    }
}
