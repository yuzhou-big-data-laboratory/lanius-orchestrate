/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.common.dto.system.SimpleUserVO;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserEntity;
import org.yzbdl.lanius.orchestrate.common.vo.system.AccountVO;
import org.yzbdl.lanius.orchestrate.common.vo.system.UserVo;

import java.util.List;

/**
 * 用户mapper
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 14:16
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

    /**
     * 根据组织获取用户
     * @param orgId 组织id
     * @param skip 跳过页
     * @param pageSize 页大小
     * @param wrapper mybatisplus条件
     * @return 用户VO列表
     */
    List<UserVo> queryUserVoByOrgId(@Param("orgId") Long orgId, @Param("skip") Long skip, @Param("pageSize") Long pageSize, @Param("ew") LambdaQueryWrapper<UserEntity> wrapper);

    /**
     * 根据组织id统计用户
     *
     * @param orgId  组织id
     * @param wrapper 筛选条件
     * @return 统计行数
     */
    int countUserVoByOrgId(@Param("orgId") Long orgId, @Param("ew") LambdaQueryWrapper<UserEntity> wrapper);

    /**
     * 根据组织id查询用户列表
     *
     * @param orgId  组织id
     * @param page   分页
     * @param wrapper mybatisplus条件
     * @return 用户列表
     */
    Page<UserEntity> queryUserByOrgId(@Param("orgId") Long orgId, IPage<UserEntity> page, @Param("ew") LambdaQueryWrapper<UserEntity> wrapper);

    /**
     * 查询第一个用户，用来判断存不存在
     * 没有使用数据库自带的exist，减少数据库语法依赖
     *
     * @param orgId 组织id
     * @return userid
     */
    Long getFirstUserIdByOrgId(@Param("orgId") Long orgId);


    /**
     * 管理员查询用户接口
     *
     * @param skip     跳过页数
     * @param pageSize 页面大小
     * @param wrapper  查询条件
     * @return 账号列表
     */
    List<AccountVO> queryAccountVO(@Param("skip") Long skip, @Param("pageSize") Long pageSize, @Param("ew") LambdaQueryWrapper<UserEntity> wrapper);


    /**
     * 获取不在该组织的用户
     *
     * @param orgId        组织id
     * @param likeUserName 模糊查询用户名称
     * @return 用户列表
     */
    List<SimpleUserVO> listUsersNotInOrg(Long orgId, String likeUserName);

}
