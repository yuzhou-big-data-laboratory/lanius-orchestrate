/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.constant;

/**
 * 资源相关常量
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-13 17:13
 */
public class TaskResourceConstant {

	/**
	 * 外部目录表名
	 */
	public static final String DIRECTORY_TABLE_NAME = "R_DIRECTORY";

	/**
	 * 外部任务表名
	 */
	public static final String JOB_TABLE_NAME = "R_JOB";

	/**
	 * 外部转换表名
	 */
	public static final String TRANSFORMATION_TABLE_NAME = "R_TRANSFORMATION";

	/**
	 * 资源库名称
	 */
	public static final String CONFIG_CONTENT_REPO_NAME = "repo_name";

	/**
	 * 资源库账户
	 */
	public static final String CONFIG_CONTENT_REPO_ACCOUNT = "repo_account";

	/**
	 * 资源库密码
	 */
	public static final String CONFIG_CONTENT_REPO_PASSWORD = "repo_password";

	/**
	 * 资源库路径
	 */
	public static final String CONFIG_CONTENT_REPO_ENTRANCE = "repo_entrance";

	/**
	 * 服务节点用户名
	 */
	public static final String SERVER_PROGRAM_USERNAME = "username";

	/**
	 * 服务节点密码
	 */
	public static final String SERVER_PROGRAM_PASSWORD = "password";

	/**
	 * http前缀
	 */
	public static final String HTTP = "http://";

}
