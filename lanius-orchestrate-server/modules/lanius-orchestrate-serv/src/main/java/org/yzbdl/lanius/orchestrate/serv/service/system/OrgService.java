/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.dto.system.OrgParamDto;
import org.yzbdl.lanius.orchestrate.common.dto.system.OrgQueryDTO;
import org.yzbdl.lanius.orchestrate.common.entity.system.OrgEntity;

import java.util.List;

/**
 * 组织服务接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 13:29
 */
public interface OrgService extends IService<OrgEntity> {

	/**
	 * 根据用户id查询组织
	 * @param userId 用户id
	 * @return 组织列表
	 */
	List<OrgEntity> queryOrgByUserId(Long userId);

	/**
	 * 根据分页查询组织
	 * @param orgQueryDto 查询条件
	 * @param page 分页对象
	 * @return 组织分页列表
	 */
	Page<OrgEntity> listOrgPage(OrgQueryDTO orgQueryDto, Page<OrgEntity> page);

	/**
	 * 根据组织名称查询组织
	 * @param orgName 组织名称
	 * @return 组织列表
	 */
	List<OrgEntity> listOrgByOrgName(String orgName);

	/**
	 * 查看当前用户是否属于某组织
	 * @param orgId 组织id
	 * @return 是否
	 */
	boolean isCurrentUserBelongToOrg(Long orgId);

	/**
	 * 改变组织冻结状态
	 * @param frozen 冻结状态值
	 * @param orgId 组织id
	 * @return 是否
	 */
	boolean changeOrgFrozen(Boolean frozen,Long orgId);

	/**
	 * 将用户添加进新的组织中
	 * @param userId 用户id
	 * @param orgId 组织id
	 * @param chief 是否是领袖
	 * @return 更改行数
	 */
	int addUserIntoOrg(Long userId,Long orgId,boolean chief);


	/**
	 * 添加组织并关联已有用户
	 * @param orgParamDto 组织参数
	 * @return 更改行数
	 */
	int insertOrgRelateUser(OrgParamDto orgParamDto);

	/**
	 * 判断用户是否在该组织中
	 * @param userId 用户id
	 * @param orgId 组织id
	 * @return
	 */
	boolean isUserInOrg(Long userId,Long orgId);

	/**
	 * 是否存在组织
	 * @param orgName 组织名称
	 * @return 布尔值
	 */
	boolean existOrg(String orgName);

	/**
	 * 更新组织
	 * @param orgEntity 组织实体
	 * @return 布尔值
	 */
	boolean updateOrg(OrgEntity orgEntity);

	/**
	 * 移除组织
	 * @param id 组织id
	 * @return 是否正确
	 */
	Boolean removeCascade(Long id);

}
