/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.constant;

/**
 * 任务编排Json配置常量
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-14 13:46
 */
public interface TaskJsonConfigConstant {

	/**
	 * 参数名称
	 */
	String PARAM_NAME = "paramName";

	/**
	 * 参数类型
	 */
	String PARAM_TYPE = "paramType";

	/**
	 * 参数值
	 */
	String PARAM_VALUE = "paramValue";
}
