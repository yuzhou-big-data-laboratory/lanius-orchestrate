package org.yzbdl.lanius.orchestrate.serv.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.serv.utils.CommonUtil;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.utils.cron.CronUtils;

import java.util.List;
import java.util.Map;

/**
 * 公共数据
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 15:44
 */
@RestController
@RequestMapping("/common/")
@Api(value = "枚举类公共数据控制管理相关接口", tags = "枚举类公共数据控制管理相关接口")
public class CommonController {

    /**
     * 资源类型下拉数据
     *
     * @return 结果数据
     */
    @GetMapping(value = "resourceType")
    @ApiOperation(value = "资源类型下拉数据", notes = "资源类型下拉数据")
    @ResponseBody
    public ResultObj<List<Map<String, Object>>> getResourceType() {
        return ResultObj.success(CommonUtil.getResourceType());
    }

    /**
     * 任务编排状态下拉数据
     *
     * @return 结果数据
     */
    @GetMapping(value = "taskStatus")
    @ApiOperation(value = "任务编排状态下拉数据", notes = "任务编排状态下拉数据")
    @ResponseBody
    public ResultObj<List<Map<String, Object>>> getTaskStatus() {
        return ResultObj.success(CommonUtil.getTaskStatus());
    }

    /**
     * 任务实例状态下拉数据
     *
     * @return 结果数据
     */
    @GetMapping(value = "taskInstanceStatus")
    @ApiOperation(value = "任务实例状态下拉数据", notes = "任务实例状态下拉数据")
    @ResponseBody
    public ResultObj<List<Map<String, Object>>> getTaskInstanceStatus() {
        return ResultObj.success(CommonUtil.getTaskInstanceStatus());
    }

    /**
     * 日志等级下拉数据
     *
     * @return 结果数据
     */
    @GetMapping(value = "logLevel")
    @ApiOperation(value = "日志等级下拉数据", notes = "日志等级下拉数据")
    @ResponseBody
    public ResultObj<List<Map<String, Object>>> getLogLevel() {
        return ResultObj.success(CommonUtil.getLogLevel());
    }

    /**
     * 数据类型下拉数据
     *
     * @return 结果数据
     */
    @GetMapping(value = "dataType")
    @ApiOperation(value = "数据类型下拉数据", notes = "数据类型下拉数据")
    @ResponseBody
    public ResultObj<List<Map<String, Object>>> getDataType() {
        return ResultObj.success(CommonUtil.getDataType());
    }

    /**
     * 根据Quartz-Cron表达式获取最近几次执行时间
     *
     * @param cron
     *        cron表达式
     * @return
     *        结果数据
     */
    @GetMapping(value = "getRecentTriggerTime")
    @ApiOperation(value = "根据Quartz-Cron表达式获取最近几次执行时间", notes = "根据Quartz-Cron表达式获取最近几次执行时间")
    @ResponseBody
    public ResultObj<List<String>> getRecentTriggerTime(@RequestParam String cron) {
        return ResultObj.success(CronUtils.getRecentTriggerTime(cron));
    }
}
