/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.constant;

/**
 * 任务编排调度常用常量
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 16:16
 */
public interface TaskPlanSchedulerConstant {

    /**
     * 任务编排分组的ID
     */
    String TASK_PLAN_GROUP_ID = "task_plan_group_id";

    /**
     * 任务编排的ID（在定时任务中当作任务的jobName）
     */
    String TASK_PLAN_ID = "task_plan_id";

    /**
     * 任务编排执行节点的ID
     */
    String TASK_PLAN_NODE_ID = "task_plan_node_id";

    /**
     * 任务编排是否获取增量日志
     */
    String TASK_PLAN_INCR_LOG = "task_plan_incremental_log";

    /**
     * 是否异步
     */
    String TASK_PLAN_IS_ASYNC = "async";

    /**
     * 是否并行执行
     */
    String TASK_PLAN_IS_PARALLEL = "parallel";

    /**
     * 调度基础路径前缀
     */
    String SCHEDULER_BASE_URL_PREFIX = "http://";

}
