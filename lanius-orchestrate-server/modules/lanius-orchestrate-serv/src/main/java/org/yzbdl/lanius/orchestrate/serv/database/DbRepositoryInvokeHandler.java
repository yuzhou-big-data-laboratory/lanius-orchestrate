/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.database;

import com.alibaba.druid.pool.DruidDataSource;
import org.yzbdl.lanius.orchestrate.common.base.dto.DataBaseInfo;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ExternalDirectoryDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ExternalFileDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ExternalJobDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ExternalTransformationDto;

import java.util.List;
import java.util.Map;

/**
 * 数据库资源库处理顶层接口
 *
 * @author hujian@yzbdl.ac.cn
 * @date 2022-09-20 16:42
 */
public interface DbRepositoryInvokeHandler {

    /**
     * 通过Druid获取数据库连接对象
     * @param dbInfo 数据库信息
     * @return 数据库连接对象
     */
    DruidDataSource getDruidDataSource(DataBaseInfo dbInfo);

    /**
     * 获取Jdbc模板
     * @param jdbcDriverClassName 驱动类名称
     * @return Jdbc模板
     */
    String getJdbcTemplate(String jdbcDriverClassName);

    /**
     * 生成JdbcUrl连接
     * @param jdbcTemplate url模板
     * @param ip ip地址
     * @param port 端口
     * @param database 数据库
     * @param paramMap 参数Map
     * @return jdbc url
     */
    String generateJdbcUrl(String jdbcTemplate, String ip,
                           Integer port, String database, Map<String, String> paramMap);

    /**
     * 拼接数据库参数
     * @param url 地址
     * @param map 参数集合
     * @return 数据库链接地址
     */
    String generateFullJdbcUrl(String url, Map<String, String> map);

    /**
     * 根据Map生成Url的参数
     *
     * @param map
     *        参数Map
     * @return
     *        数据库参数
     */
    String generateParams(Map<String, String> map);

    /**
     * 测试数据库连通性；超时时间为10s
     * @param url url
     * @param username 用户名
     * @param password 密码
     * @return 成功与否
     */
    Boolean testConnection(String url, String username, String password);

    /**
     * 获取数据库资源库外部节点数据
     * @param dataBaseInfo 数据库信息
     * @return 外部节点数据
     */
    List<ExternalDirectoryDto> getDbRepoExternalDirectory(DataBaseInfo dataBaseInfo);


    /**
     * 获取外部树目录下内容
     * @param dataBaseInfo 数据库信息
     * @param directoryId 目录id
     * @param resourceType 资源类型
     * @return 外部树目录下内容
     */
    ExternalFileDto getDbRepoExternalFile(DataBaseInfo dataBaseInfo, Long directoryId, Integer resourceType);

    /**
     * 获取数据库资源库的作业集合
     * @param dataBaseInfo 数据库信息
     * @param directoryId 目录id
     * @return 作业集合
     */
    List<ExternalJobDto> getDbRepoExternalJobList(DataBaseInfo dataBaseInfo, Long directoryId);

    /**
     * 获取数据库资源库的转换集合
     * @param dataBaseInfo 数据库信息
     * @param directoryId 目录id
     * @return 转换集合
     */
    List<ExternalTransformationDto> getDbRepoTransformationList(DataBaseInfo dataBaseInfo, Long directoryId);

}
