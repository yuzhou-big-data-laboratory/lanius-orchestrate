/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.resource.impl;

import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.yzbdl.lanius.orchestrate.common.base.dto.DataBaseInfo;
import org.yzbdl.lanius.orchestrate.common.constant.Const;
import org.yzbdl.lanius.orchestrate.common.dto.resource.*;
import org.yzbdl.lanius.orchestrate.common.entity.resource.TaskResourceConfigEntity;
import org.yzbdl.lanius.orchestrate.common.entity.resource.TaskResourceEntity;
import org.yzbdl.lanius.orchestrate.common.entity.task.TaskPlan;
import org.yzbdl.lanius.orchestrate.common.enums.ResourceTypeEnum;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.BusinessException;
import org.yzbdl.lanius.orchestrate.common.utils.AesUtils;
import org.yzbdl.lanius.orchestrate.common.utils.ExceptionUtil;
import org.yzbdl.lanius.orchestrate.common.utils.MessageUtil;
import org.yzbdl.lanius.orchestrate.common.utils.SpecialCharacterUtil;
import org.yzbdl.lanius.orchestrate.serv.database.DbRepositoryFactory;
import org.yzbdl.lanius.orchestrate.serv.database.DbRepositoryInvokeHandler;
import org.yzbdl.lanius.orchestrate.serv.mapper.resource.TaskResourceConfigMapper;
import org.yzbdl.lanius.orchestrate.serv.mapper.resource.TaskResourceMapper;
import org.yzbdl.lanius.orchestrate.serv.mapper.task.TaskPlanMapper;
import org.yzbdl.lanius.orchestrate.serv.service.resource.TaskResourceService;
import org.yzbdl.lanius.orchestrate.serv.utils.TreeUtil;
import org.yzbdl.lanius.orchestrate.serv.vo.TaskResourceVO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.yzbdl.lanius.orchestrate.serv.constant.TaskResourceConstant.*;

/**
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-08 16:49
 */
@Service
@RequiredArgsConstructor
public class TaskResourceServiceImpl extends ServiceImpl<TaskResourceMapper, TaskResourceEntity> implements TaskResourceService {

    private static final Logger logger = LoggerFactory.getLogger(TaskResourceServiceImpl.class);

    private final TaskResourceConfigMapper taskResourceConfigMapper;

    private final TaskPlanMapper taskPlanMapper;

    @Value("${encryption.user-key}")
    String encryptKey;

    @Override
    public Boolean addTaskResource(TaskResourceDto taskResourceDto) {
        checkResourceType(taskResourceDto.getResourceType(), taskResourceDto.getResourcePath(), taskResourceDto.getGroupId());
        // 重命名判断
        LambdaQueryWrapper<TaskResourceEntity> queryWrapper = new QueryWrapper<TaskResourceEntity>().lambda()
                .eq(TaskResourceEntity::getResourceName, taskResourceDto.getResourceName())
                .eq(TaskResourceEntity::getDeleted, false);
        ExceptionUtil.checkParam(!this.baseMapper.exists(queryWrapper), "已存在同样的资源名称，请重新输入！");
        // 检测资源库是否存在
        this.checkResourceConfigExists(taskResourceDto.getResourceConfigId());
        // Dto数据拷贝
        TaskResourceEntity taskResourceEntity = new TaskResourceEntity();
        BeanUtils.copyProperties(taskResourceDto, taskResourceEntity);
        taskResourceEntity.setDeleted(false);
        // 写入资源配置数据
        this.setResourceContent(taskResourceDto, taskResourceEntity);
        return this.save(taskResourceEntity);
    }

    /**
     * 检测任务资源是否存在
     *
     * @param taskResourceConfigId 任务资源id
     */
    private void checkResourceConfigExists(Long taskResourceConfigId) {
        if (Objects.nonNull(taskResourceConfigId)) {
            LambdaQueryWrapper<TaskResourceConfigEntity> resourceConfigWrapper = new QueryWrapper<TaskResourceConfigEntity>().lambda()
                    .eq(TaskResourceConfigEntity::getId, taskResourceConfigId)
                    .eq(TaskResourceConfigEntity::getDeleted, false);
            ExceptionUtil.checkParam(taskResourceConfigMapper.exists(resourceConfigWrapper), "所选资源配置已删除请重新选择！");
        }
    }

    @Override
    public Boolean deleteTaskResource(Long id) {
        LambdaQueryWrapper<TaskPlan> queryWrapper = new QueryWrapper<TaskPlan>().lambda()
                .eq(TaskPlan::getTaskResourceId, id);
        if (taskPlanMapper.exists(queryWrapper)) {
            throw new BusinessException("该任务资源已被调用，无法删除！");
        }
        return this.updateById(TaskResourceEntity.builder().id(id).deleted(true).build());
    }

    @Override
    public TaskResourceEntity getTaskResource(Long id) {
        TaskResourceEntity getOne = this.getById(id);
        if(ObjectUtil.isNull(getOne)){
            getOne.setResourceContent(AesUtils.encrypt(getOne.getResourceContent(),encryptKey));
        }
        return getOne;
    }

    @Override
    public IPage<TaskResourceDto> pageTaskResource(Integer page, Integer size, TaskResourcePageDto taskResource) {
        // 1. 分页获取任务资源数据
        Page<TaskResourceEntity> pageParam = new Page<>(page, size);
        LambdaQueryWrapper<TaskResourceEntity> queryWrapper = new QueryWrapper<TaskResourceEntity>().lambda()
                // 资源类型条件
                .eq(Objects.nonNull(taskResource.getResourceType()), TaskResourceEntity::getResourceType,
                        taskResource.getResourceType())
                // 资源名称匹配
                .like(StringUtils.hasLength(taskResource.getResourceName()), TaskResourceEntity::getResourceName,
                        SpecialCharacterUtil.escapeStr(taskResource.getResourceName()))
                // 时间查询条件
                .between(Objects.nonNull(taskResource.getStartTime()) && Objects.nonNull(taskResource.getEndTime()),
                        TaskResourceEntity::getCreateTime, taskResource.getStartTime(), taskResource.getEndTime())
                // 匹配分组信息
                .eq(Objects.nonNull(taskResource.getGroupId()), TaskResourceEntity::getGroupId, taskResource.getGroupId())
                // 筛选未删除数据
                .eq(TaskResourceEntity::getDeleted, false)
                // 创建时间降序排序
                .orderByDesc(TaskResourceEntity::getCreateTime)
                .orderByDesc(TaskResourceEntity::getId);
        Page<TaskResourceEntity> pageList = this.page(pageParam, queryWrapper);
        // 2. 获取配置信息
        List<Long> ids = pageList.getRecords().stream().map(TaskResourceEntity::getResourceConfigId).distinct()
                .collect(Collectors.toList());
        List<TaskResourceConfigEntity> taskEntities =
                !CollectionUtils.isEmpty(ids) ? taskResourceConfigMapper.selectBatchIds(ids) : new ArrayList<>();
        // 3. 转换为配置Map信息
        Map<Long, TaskResourceConfigEntity> configMap = taskEntities.stream()
                .collect(Collectors.toMap(TaskResourceConfigEntity::getId, Function.identity(), (o1, o2) -> o1));
        // 4. 拼装配置信息
        return this.getConvert(pageList, configMap);
    }

    @Override
    public Boolean isExistsTaskResource(Long id) {
        LambdaQueryWrapper<TaskResourceEntity> queryWrapper = new QueryWrapper<TaskResourceEntity>().lambda()
                .eq(TaskResourceEntity::getGroupId, id)
                .eq(TaskResourceEntity::getDeleted, false);
        // 判断当前目录是否存在内容
        return this.baseMapper.exists(queryWrapper);
    }

    @Override
    public Boolean updateTaskResource(TaskResourceDto taskResourceDto) {
        checkResourceType(taskResourceDto.getResourceType(), taskResourceDto.getResourcePath(), taskResourceDto.getGroupId());
        ExceptionUtil.checkParam(Objects.nonNull(taskResourceDto.getId()), "id不能为空");
        // 重命名判断
        LambdaQueryWrapper<TaskResourceEntity> queryWrapper = new QueryWrapper<TaskResourceEntity>().lambda()
                .ne(TaskResourceEntity::getId, taskResourceDto.getId())
                .eq(TaskResourceEntity::getResourceName, taskResourceDto.getResourceName())
                .eq(TaskResourceEntity::getDeleted, false);
        ExceptionUtil.checkParam(!this.baseMapper.exists(queryWrapper), "已存在同样的资源名称，请重新输入！");
        // 检测资源库是否存在
        this.checkResourceConfigExists(taskResourceDto.getResourceConfigId());
        // Dto数据拷贝
        TaskResourceEntity taskResourceEntity = new TaskResourceEntity();
        BeanUtils.copyProperties(taskResourceDto, taskResourceEntity);
        // 写入资源配置数据
        this.setResourceContent(taskResourceDto, taskResourceEntity);
        return this.updateById(taskResourceEntity);
    }

    /**
     * 匹配资源类型与资源路径
     *
     * @param resourceType 资源类型
     * @param resourcePath 资源路径
     * @param resourceGroupId 资源分组ID
     */
    private void checkResourceType(Integer resourceType, String resourcePath, Long resourceGroupId) {
        boolean ktrCheck = ResourceTypeEnum.KETTLE_KTR.getCode().equals(resourceType) &&
                resourcePath.endsWith(ResourceTypeEnum.KETTLE_KTR.getValue());
        boolean jobCheck = ResourceTypeEnum.KETTLE_OBJ.getCode().equals(resourceType) &&
                resourcePath.endsWith(ResourceTypeEnum.KETTLE_OBJ.getValue());
        if (!(ktrCheck || jobCheck)) {
            throw new BusinessException("操作失败，请注意资源类型与所选资源相匹配！");
        }

        if(Objects.isNull(resourceGroupId)) {
            throw new BusinessException("操作失败，请选择任务资源分组！");
        }

        if(Const.TaskResourceGroup.TASK_RESOURCE_GROUP_ROOT_NODE_ID.equals(resourceGroupId)) {
            throw new BusinessException("操作失败，请勿选择任务资源根节点！");
        }
    }

    @Override
    public void batchUpdateResourceContent(Long resourceConfigId, String repoName) {
        LambdaQueryWrapper<TaskResourceEntity> queryWrapper = new QueryWrapper<TaskResourceEntity>().lambda()
                .eq(TaskResourceEntity::getResourceConfigId, resourceConfigId);
        List<TaskResourceEntity> list = this.list(queryWrapper);
        if (!CollectionUtils.isEmpty(list)) {
            List<TaskResourceEntity> updateList = list.stream().map(entity -> {
                Map<String, String> contentMap = JSONUtil.toBean(JSONUtil.parse(entity.getResourceContent()), new TypeReference<>() {
                }, true);
                contentMap.put(CONFIG_CONTENT_REPO_NAME, repoName);
                return TaskResourceEntity.builder()
                        .id(entity.getId())
                        .resourceContent(JSONUtil.toJsonStr(contentMap))
                        .build();
            }).collect(Collectors.toList());
            this.updateBatchById(updateList);
        }
    }

    /**
     * 设定资源配置字段
     *
     * @param taskResourceDto    taskResourceDto
     * @param taskResourceEntity taskResourceEntity
     */
    private void setResourceContent(TaskResourceDto taskResourceDto, TaskResourceEntity taskResourceEntity) {
        Map<String, String> resourceContent = new HashMap<>(4);
        TaskResourceConfigEntity taskResourceConfigEntity
                                                  = taskResourceConfigMapper.selectById(taskResourceDto.getResourceConfigId());
        ExceptionUtil.checkParam(Objects.nonNull(taskResourceConfigEntity), "未查询到对应数据库信息！");
        DatabaseConfigurationDTO databaseConfigurationDTO = JSONUtil.toBean(taskResourceConfigEntity.getConnectUrl(),
                DatabaseConfigurationDTO.class);
        // 构建资源库Json数据
        resourceContent.put(CONFIG_CONTENT_REPO_NAME, databaseConfigurationDTO.getDbName());
        resourceContent.put(CONFIG_CONTENT_REPO_ACCOUNT, taskResourceDto.getResourceAccountName());
        resourceContent.put(CONFIG_CONTENT_REPO_PASSWORD, AesUtils.decrypt(taskResourceDto.getResourcePassword(), encryptKey));
        resourceContent.put(CONFIG_CONTENT_REPO_ENTRANCE, taskResourceDto.getResourcePath());
        taskResourceEntity.setResourceContent(JSONUtil.toJsonStr(resourceContent));
    }

    @Override
    public List<TaskResourceEntity> getListByGroupIdForTaskPlan(Long resourceGroupId) {
        LambdaQueryWrapper<TaskResourceEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TaskResourceEntity::getGroupId, resourceGroupId);
        queryWrapper.eq(TaskResourceEntity::getDeleted, Boolean.FALSE);
        queryWrapper.select(TaskResourceEntity::getId, TaskResourceEntity::getGroupId, TaskResourceEntity::getResourceName);
        return this.list(queryWrapper);
    }

    @Override
    public List<ExternalDirectoryTreeDto> buildExternalResourceTree(Long resourceConfigId) {

        // 获取数据库资源库相关处理信息
        TaskResourceVO taskResourceVO = buildRepositoryProcessData(resourceConfigId);

        // 获取外部节点数据
        List<ExternalDirectoryDto> directoryDtoLists
                                = taskResourceVO.getHandler().getDbRepoExternalDirectory(taskResourceVO.getDataBaseInfo());
        // 拼装树结构
        List<ExternalDirectoryTreeDto> treeNodes = directoryDtoLists.stream().map(directory -> ExternalDirectoryTreeDto
                .builder()
                .id(directory.getIdDirectory())
                .parentId(directory.getIdDirectoryParent())
                .nodeName(directory.getDirectoryName())
                .build()).collect(Collectors.toList());
        return TreeUtil.buildDetailByRoot(treeNodes, 0L);

    }

    @Override
    public ExternalFileDto getExternalResourceFile(Long directoryId, Long resourceConfigId, Integer resourceType) {
        // 获取数据库资源库相关处理信息
        TaskResourceVO taskResourceVO = buildRepositoryProcessData(resourceConfigId);

        return taskResourceVO.getHandler()
                .getDbRepoExternalFile(taskResourceVO.getDataBaseInfo(), directoryId, resourceType);
    }



    /**
     * 构建资源库处理类等信息
     * @param resourceConfigId 资源配置ID
     * @return 信息VO
     */
    private TaskResourceVO buildRepositoryProcessData(Long resourceConfigId) {
        TaskResourceConfigEntity taskResourceConfigEntity = taskResourceConfigMapper.selectById(resourceConfigId);
        ExceptionUtil.checkParam(Objects.nonNull(taskResourceConfigEntity), "未找到对应数据库资源！");

        // 获取数据库资源库连通性
        DbRepositoryInvokeHandler handler
                = DbRepositoryFactory.getHandler(taskResourceConfigEntity.getConnectCategory());
        if(Objects.isNull(handler)) {
            throw new BusinessException(MessageUtil.get("task.resource.retrieve.handler.fail"));
        }

        // 解析数据库资源库关键信息
        DatabaseConfigurationDTO databaseConfigurationDTO;
        try{
            databaseConfigurationDTO
                    = JSONUtil.toBean(taskResourceConfigEntity.getConnectUrl(), DatabaseConfigurationDTO.class);
        } catch (Exception e){
            logger.error("[解析资源库数据失败！],[资源库Json配置解析异常],[请清除资源库数据重新填写资源库数据！]");
            throw new BusinessException("资源库链接配置不正确，请重新编辑修改！");
        }

        // 获取数据库链接
        String jdbcUrl = handler.generateJdbcUrl(handler.getJdbcTemplate(
                org.apache.commons.lang3.StringUtils.EMPTY), databaseConfigurationDTO.getIp(),
                databaseConfigurationDTO.getPort(),
                databaseConfigurationDTO.getDbName(), databaseConfigurationDTO.getParams());

        DataBaseInfo dataBaseInfo = DataBaseInfo.builder()
                .url(jdbcUrl)
                .userName(taskResourceConfigEntity.getConnectAccount())
                .password(taskResourceConfigEntity.getConnectPassword()).build();
        DruidDataSource dataSource = handler.getDruidDataSource(dataBaseInfo);

        return new TaskResourceVO(handler, taskResourceConfigEntity, dataBaseInfo);
    }

    /**
     * taskResource数据转换
     *
     * @param pageList  entityList数据
     * @param configMap map配置
     * @return TaskResourceDto数据
     */
    private IPage<TaskResourceDto> getConvert(Page<TaskResourceEntity> pageList,
                                                                  Map<Long, TaskResourceConfigEntity> configMap) {
        return pageList.convert(task -> {
            TaskResourceDto taskResourceDto = new TaskResourceDto();
            BeanUtils.copyProperties(task, taskResourceDto);
            taskResourceDto.setConfigConnectName(Optional
                    .ofNullable(configMap.get(taskResourceDto.getResourceConfigId()))
                    .map(TaskResourceConfigEntity::getConnectName)
                    .orElse(null));
            taskResourceDto.setResourceContent(AesUtils.encrypt(taskResourceDto.getResourceContent(), encryptKey));
            return taskResourceDto;
        });
    }
}