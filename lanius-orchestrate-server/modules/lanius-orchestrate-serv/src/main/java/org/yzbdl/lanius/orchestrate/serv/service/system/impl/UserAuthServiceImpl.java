/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yzbdl.lanius.orchestrate.common.dto.system.PayLoadClaimDto;
import org.yzbdl.lanius.orchestrate.common.entity.system.OrgEntity;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserEntity;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserOrgEntity;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.BusinessException;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.NoPermissionException;
import org.yzbdl.lanius.orchestrate.common.jwt.JwtHelper;
import org.yzbdl.lanius.orchestrate.common.jwt.TokenDto;
import org.yzbdl.lanius.orchestrate.common.utils.AesUtils;
import org.yzbdl.lanius.orchestrate.common.utils.BcryptEncoderUtil;
import org.yzbdl.lanius.orchestrate.common.utils.MessageUtil;
import org.yzbdl.lanius.orchestrate.common.vo.system.UserTokenVo;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.OrgMapper;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.UserMapper;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.UserOrgMapper;
import org.yzbdl.lanius.orchestrate.serv.service.system.UserAuthService;
import org.yzbdl.lanius.orchestrate.serv.utils.CurrentUserUtil;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 用户认证服务
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-21 11:10
 */
@Service
@Transactional
public class UserAuthServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserAuthService {

	@Autowired
	OrgMapper orgMapper;

	@Autowired
	UserOrgMapper userOrgMapper;

	@Value("${encryption.user-key}")
	String encryptKey;

	/**
	 * 根据用户名获取用户
	 * @param userName 用户名
	 * @return 用户对象
	 * @throws UsernameNotFoundException 框架自带的异常
	 */
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
    	return Optional.ofNullable(getUserByUserName(userName))
			    .orElseThrow(()->new UsernameNotFoundException(MessageUtil.get("system.user.login.wrong_username")));
    }


	/**
	 * 获取已验证的用户
	 * @param userName 用户名
	 * @param password 密码
	 * @return 用户实体
	 */
	public UserEntity getValidUser(String userName, String password){
		String decryptPassword = AesUtils.decrypt(password, encryptKey);
	    UserEntity user = (UserEntity) loadUserByUsername(userName);
        if(!BcryptEncoderUtil.match(decryptPassword, user.getPassword())){
            throw new NoPermissionException(MessageUtil.get("system.user.login.wrong_password"));
        }
        return user;
    }

	/**
	 * 通过用户名称获取用户
	 * @param userName 用户名
	 * @return 返回用户实体
	 */
	@Override
    public UserEntity getUserByUserName(String userName){
    	return this.getOne(new LambdaQueryWrapper<UserEntity>()
	        .eq(UserEntity::getUsername,userName)
				.eq(UserEntity::getDeleted,false)
	    );
    }

	/**
	 * 用户登录
	 * @param userName 用户名
	 * @param password 用户密码
	 * @return UserTokenVo
	 */
	@Override
	public UserTokenVo login(String userName, String password) {
		UserEntity userEntity = getValidUser(userName,password);
		if(!userEntity.isEnabled()){
			throw new NoPermissionException(MessageUtil.get("system.user.disable"));
		}
		List<OrgEntity> orgEntities= orgMapper.queryOrgByUserId(userEntity.getId());
		if(orgEntities.size()>0){
			Long orgId = orgEntities.get(0).getId();
			return getUserTokenMap(userEntity,orgId);
		}
		throw new NoPermissionException(MessageUtil.get("system.user.none_org"));
	}

	/**
	 * 获取用户token视图
	 * @param userEntity 用户提示
	 * @param orgId 组织id
	 * @return 用户token视图
	 */
	protected UserTokenVo getUserTokenMap(UserEntity userEntity,Long orgId){
		boolean isOrgChief = isOrgChief(userEntity,orgId);
		return UserTokenVo.builder()
				.user(userEntity)
				.token(buildTokenFromPersonEntity(userEntity,orgId,isOrgChief))
				.chief(isOrgChief)
				.orgId(orgId)
				.build();
	}


	/**
	 * 判断用户是否是首领
	 * @param userEntity 用户实体
	 * @param orgId 组织id
	 * @return 布尔值
	 */
	private boolean isOrgChief(UserEntity userEntity,Long orgId){
		UserOrgEntity userOrgEntity= userOrgMapper.selectOne(new LambdaQueryWrapper<UserOrgEntity>()
				.eq(UserOrgEntity::getUserId,userEntity.getId())
				.eq(UserOrgEntity::getOrgId,orgId)
		);
		return Optional.ofNullable(userOrgEntity).orElseThrow(()->new BusinessException(MessageUtil.get("system.user.user_org_not_match"))).isOrgChief();
	}

	/**
	 * 生成token
	 * @param user 用户实体
	 * @param orgId 组织id
	 * @param isOrgChief 是否是组织领袖
	 * @return token对象
	 */
	private TokenDto buildTokenFromPersonEntity(UserEntity user, Long orgId, boolean isOrgChief) {
		PayLoadClaimDto payLoadClaimDto = PayLoadClaimDto.builder()
				.userId(user.getId())
				.userName(user.getUsername())
				.manager(false)
				.orgChief(isOrgChief)
				.orgId(orgId)
				.build();
		CurrentUserUtil.setCurrentUser(payLoadClaimDto,user);
		return new TokenDto(payLoadClaimDto.translateToMap(),false);
	}

	/**
	 * 刷新Token
	 * @param orgId 组织id
	 * @return 用户tokenVO
	 */
	@Override
	public UserTokenVo refreshTokenByOrgId(Long orgId){
		UserEntity userEntity = this.getById(CurrentUserUtil.getCurrentUserId());
		return getUserTokenMap(userEntity,orgId);
	}

	/**
	 * 更新密码
	 * @param userId 用户id
	 * @param newPassword 新密码
	 * @param oldPassword 旧密码
	 * @return 布尔值
	 */
	@Override
	public Boolean updatePassword(Long userId, String newPassword, String oldPassword){
		UserEntity userEntity = this.getById(userId);
		newPassword = AesUtils.decrypt(newPassword, encryptKey);
		oldPassword = AesUtils.decrypt(oldPassword, encryptKey);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		if(!encoder.matches(oldPassword, userEntity.getPassword())){
			throw new NoPermissionException(MessageUtil.get("system.user.wrong_old_password"));
		}
		return this.update(new LambdaUpdateWrapper<UserEntity>()
				.eq(UserEntity::getId,userId).set(UserEntity::getPassword, BcryptEncoderUtil.encode(newPassword))
		);
	}

	@Override
	public UserTokenVo refresh(String rfToken) {
		try{
			Map<String,Object> claims = JwtHelper.getNormalClaimMapFromToken(rfToken);
			PayLoadClaimDto payLoadClaimDto = JSONUtil.toBean(JSONUtil.toJsonStr(claims),PayLoadClaimDto.class);
			return getUserTokenMap(this.getById(payLoadClaimDto.getUserId()),payLoadClaimDto.getOrgId());
		}catch (Exception e){
			throw new BusinessException(MessageUtil.get("system.user.refresh_token_fail"));
		}

	}
}
