/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.task;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.common.dto.task.TaskPlanQueryDTO;
import org.yzbdl.lanius.orchestrate.common.dto.task.TaskPlanResourceDTO;
import org.yzbdl.lanius.orchestrate.common.entity.task.TaskPlan;
import org.yzbdl.lanius.orchestrate.common.vo.task.ScheduleTaskPlanVO;
import org.yzbdl.lanius.orchestrate.common.vo.task.TaskPlanVO;

import java.util.List;

/**
 * 任务编排mapper
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 10:03
 */
@Mapper
public interface TaskPlanMapper extends BaseMapper<TaskPlan> {

    /**
     * 分页查询任务编排列表
     *
     * @param page             分页信息
     * @param taskPlanQueryDto 查询参数
     * @return 结果信息
     */
    IPage<TaskPlanVO> queryPage(@Param("page") Page<TaskPlanVO> page, @Param("taskPlanQueryDto") TaskPlanQueryDTO taskPlanQueryDto);

	/**
	 * 获取任务编排及对应的分组信息
	 *
	 * @param taskPlanId
	 *        任务编排ID
	 * @return
	 *        结果信息
	 */
	TaskPlanVO getOneAndGroup(@Param("taskPlanId") Long taskPlanId);

	/**
	 * 根据任务ID获取任务资源、配置信息
	 * <p>
	 *     任务调度专用接口，忽略租户
	 * </p>
	 *
	 * @param taskPlanId
	 *        任务编排ID
	 * @return
	 *        数据信息
	 */
	@InterceptorIgnore(tenantLine = "on")
	TaskPlanResourceDTO getTaskPlanAndResourceInfoIgnoreTenantId(@Param("taskPlanId") Long taskPlanId);

	/**
	 * 获取任务计划忽略组织id
	 * @param taskPlanId 任务计划id
	 * @return 任务计划
	 */
	@InterceptorIgnore(tenantLine = "on")
	TaskPlan getTaskPlanByIdIgnoreTenantId(@Param("taskPlanId") Long taskPlanId);

	/**
	 * 获取调度总览之任务编排
	 * <p>
	 *     任务调度专用接口，忽略租户
	 * </p>
	 *
	 * @return
	 *        结果信息
	 */
	List<ScheduleTaskPlanVO> getSchedulerViewStatistics();

	/**
	 * 获取所有的任务编排数据
	 * <p>
	 *     整个系统的数据
	 * </p>
	 *
	 * @return
	 *        结果信息
	 */
	@InterceptorIgnore(tenantLine = "on")
	List<TaskPlan> getAllListIgnoreTenantId();
}
