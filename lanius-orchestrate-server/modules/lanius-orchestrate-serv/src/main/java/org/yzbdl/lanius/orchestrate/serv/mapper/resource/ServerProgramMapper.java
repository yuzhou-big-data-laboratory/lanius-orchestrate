/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.resource;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ServerProgramAndServerDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ServerProgramInfoDTO;
import org.yzbdl.lanius.orchestrate.common.dto.resource.StatusStatisticDto;
import org.yzbdl.lanius.orchestrate.common.entity.resource.ServerProgramEntity;

import java.util.List;

/**
 * 数据节点数据操作层
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-08 15:35
 */
@Mapper
public interface ServerProgramMapper extends BaseMapper<ServerProgramEntity> {

    /**
     * 获取服务节点详细信息，（包含关联的节点信息）
     * <p>
     *     任务调度专用接口，忽略租户
     * </p>
     *
     * @param programId
     *        服务节点ID
     * @return
     *       数据
     */
    @InterceptorIgnore(tenantLine = "on")
    ServerProgramInfoDTO getServerProgramInfoByIdIgnoreTenantId(@Param("programId") Long programId);


    /**
     * 获取服务节点和服务器信息数据
     * @return 节点和服务器数据
     */
    @InterceptorIgnore(tenantLine = "on")
    List<ServerProgramAndServerDto> getAllServerProgramAndServerInfo();

    /**
     * 状态统计接口
     * @return 状态统计
     */
    List<StatusStatisticDto> getStatusGroupCount();

    /**
     * 批量更新服务节点状态
     * @param list list服务节点
     */
    @InterceptorIgnore(tenantLine = "on")
    void batchUpdate(List<ServerProgramEntity> list);
}
