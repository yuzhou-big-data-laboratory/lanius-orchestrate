/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.resource;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ServerProgramDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ServerProgramInfoDTO;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ServerProgramPageDto;
import org.yzbdl.lanius.orchestrate.common.entity.resource.ServerProgramEntity;

/**
 * 服务节点信息业务接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-08 15:35
 */
public interface ServerProgramService extends IService<ServerProgramEntity> {

	/**
	 * 增加服务节点
	 * @param serverProgramDto 服务节点信息
	 * @return true|false
	 */
	Boolean addServerProgram(ServerProgramDto serverProgramDto);

	/**
	 * 删除服务节点
	 * @param id 待删除数据id
	 * @return true|false
	 */
	Boolean deleteServerProgram(Long id);

	/**
	 * 根据id获取服务节点详细
	 * @param id id
	 * @return 服务节点详细
	 */
	ServerProgramEntity getServerProgram(Long id);

	/**
	 * 分页查询数据
	 * @param page 页码
	 * @param size 单页数据量
	 * @param serverProgramPageDto 分页参数
	 * @return 服务节点列掉
	 */
	IPage<ServerProgramDto> pageServerProgram(Integer page, Integer size, ServerProgramPageDto serverProgramPageDto);

	/**
	 * 更新服务节点参数
	 * @param serverProgramDto 更新参数
	 * @return true|false
	 */
	Boolean updateServerProgram(ServerProgramDto serverProgramDto);

	/**
	 * 获取服务节点详细信息，（包含关联的节点信息）
	 *
	 * @param programId
	 *        服务节点ID
	 * @return
	 *       数据
	 */
	ServerProgramInfoDTO getServerProgramInfoByIdIgnoreTenantId(Long programId);

	/**
	 * 节点连接测试
	 * @param ip ip
	 * @param port port
	 * @param userName 用户名
	 * @param password 密码
	 * @return true|false
	 */
	Boolean testConnection(String ip, Integer port, String userName, String password);

	/**
	 * 定时任务测试服务节点
	 * @param serverId 服务器id
	 * @param ip ip
	 * @param port port
	 * @param userName 用户名
	 * @param password 密码
	 * @return true|false
	 */
	Integer testConnectionExecuteByScheduled(Long serverId, String ip, Integer port, String userName, String password);

}
