/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.resource;

import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.dto.resource.TaskResourceTreeDto;
import org.yzbdl.lanius.orchestrate.common.entity.resource.TaskResourceGroupEntity;

import java.util.List;

/**
 * 任务资源接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-11 11:29
 */
public interface TaskResourceGroupService extends IService<TaskResourceGroupEntity> {

    /**
     * 构建任务资源树结构
     * @return List<TaskResourceTreeDto> 任务资源树
     */
    List<TaskResourceTreeDto> buildResourceTree();

    /**
     * 增加任务资源分组
     * @param taskResourceGroupEntity 任务资源分组信息
     * @return true|false
     */
    Boolean addTaskResourceGroup(TaskResourceGroupEntity taskResourceGroupEntity);

    /**
     * 更新任务资源分组
     * @param taskResourceGroupEntity 任务资源分组信息
     * @return true|false
     */
    Boolean updateTaskResourceGroup(TaskResourceGroupEntity taskResourceGroupEntity);

    /**
     * 删除任务资源分组
     * @param id 待删除数据id
     * @return true|false
     */
    Boolean deleteTaskResourceGroup(Long id);

    /**
     * 直接清除任务资源分组
     * @param id 待删除数据id
     * @return true|false
     */
    Boolean directlyDeleteTaskResourceGroup(Long id);

    /**
     * 是否存在子目录
     * @param id 目录id
     * @return true|false
     */
    Boolean isExistChildrenTaskResourceGroup(Long id);

    /**
     * 根据id获取任务资源分组详细
     * @param id id
     * @return 任务资源分组详细
     */
    TaskResourceGroupEntity getTaskResourceGroup(Long id);

    /**
     * 通过名称模糊匹配分组
     * @param groupName 分组名称
     * @return 返回详细分组值
     */
    List<TaskResourceGroupEntity> getTaskResourceGroup(String groupName);

    /**
     * 构建任务资源树结构，不计算数量。任务编排新增、编辑使用
     *
     * @return List<TaskResourceTreeDto> 任务资源树
     */
    List<TaskResourceTreeDto> treeListForTaskPlan();

}
