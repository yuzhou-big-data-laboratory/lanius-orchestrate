/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.task;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.dto.task.TaskPlanQueryDTO;
import org.yzbdl.lanius.orchestrate.common.dto.task.TaskPlanResourceDTO;
import org.yzbdl.lanius.orchestrate.common.entity.resource.ServerProgramEntity;
import org.yzbdl.lanius.orchestrate.common.entity.task.TaskPlan;
import org.yzbdl.lanius.orchestrate.common.vo.task.ScheduleTaskPlanVO;
import org.yzbdl.lanius.orchestrate.common.vo.task.TaskPlanVO;
import org.yzbdl.lanius.orchestrate.common.vo.task.schedule.AddTaskPlanJobVO;
import org.yzbdl.lanius.orchestrate.common.vo.task.schedule.UpdateTaskPlanJobStatusVO;

import java.util.List;

/**
 * 任务编排service
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @version 1.0
 * @date 2022-04-07 10:00
 */
public interface TaskPlanService extends IService<TaskPlan> {

    /**
     * 分页查询任务编排列表
     *
     * @param page             分页信息
     * @param taskPlanQueryDto 查询参数
     * @return 结果信息
     */
    IPage<TaskPlanVO> queryPage(Page<TaskPlanVO> page, TaskPlanQueryDTO taskPlanQueryDto);

    /**
     * 新增任务
     *
     * @param taskPlan
     *        任务编排
     * @return
     *        结果信息
     */
    Boolean saveEntity(TaskPlan taskPlan);

    /**
     * 更新任务
     *
     * @param taskPlan
     *        任务编排
     * @return
     *        结果信息
     */
    Boolean updateEntity(TaskPlan taskPlan);

    /**
     * 根据id删除分组信息
     *
     * @param id
     *        任务ID
     * @return
     *        结果信息
     * @throws Exception
     *         任何异常
     */
    Boolean deleteEntity(Long id)throws Exception;

    /**
     * 更新任务状态
     *
     * @param taskId
     *        任务ID
     * @param status
     *        任务状态
     * @return
     *        结果信息
     */
    Boolean updateTaskStatus(Long taskId, Integer status);

    /**
     * 立即执行一次任务
     *
     * @param taskId
     *        任务编排ID
     */
    void executeNow(Long taskId);

    /**
     * 创建任务编排的定时任务
     *
     * @param addTaskPlanJobVO
     *        任务信息
     * @return
     *       执行的机器ID
     */
    Long addTaskJob(AddTaskPlanJobVO addTaskPlanJobVO);

    /**
     * 修改任务编排的定时任务执行频率
     *
     * @param addTaskPlanJobVO
     *        参数
     */
    void updateTaskJobCron(AddTaskPlanJobVO addTaskPlanJobVO);

    /**
     * 修改任务编排的定时任务执行频率、参数
     *
     * @param addTaskPlanJobVO
     *        参数
     * @param taskStatus
     *        系统任务编排的状态
     */
    void updateTaskJobCronAndParam(AddTaskPlanJobVO addTaskPlanJobVO,Integer taskStatus);

    /**
     * 修改任务编码的执行状态
     *
     * @param updateTaskPlanJobStatusVO
     *        参数
     * @param jobGroupName
     *        任务调度JOB分组名称
     */
    void updateTaskJobStatus(UpdateTaskPlanJobStatusVO updateTaskPlanJobStatusVO, String jobGroupName);

    /**
     * 立即执行任务编排定时任务
     *
     * @param taskPlanId
     *        任务编排ID
     * @param jobGroupName
     *        任务调度JOB分组名称
     */
    void executeNowTaskJob(Long taskPlanId, String jobGroupName);

    /**
     * 删除任务编排定时任务
     *
     * @param taskPlanId
     *        任务编排ID
     * @param jobGroupName
     *        任务调度JOB分组名称
     */
    void deleteTaskJob(Long taskPlanId, String jobGroupName);

    /**
     * 获取服务节点列表
     *
     * @return
     *        结果信息
     */
    List<ServerProgramEntity> serverPrograms();

    /**
     * 根据任务ID获取任务资源、配置信息
     *
     * @param taskPlanId
     *        任务编排ID
     * @return
     *        数据信息
     */
    TaskPlanResourceDTO getTaskPlanAndResourceInfoIgnoreTenantId(Long taskPlanId);

    /**
     * 获取调度总览之任务编排
     *
     * @return
     *        结果信息
     */
    List<ScheduleTaskPlanVO> getSchedulerViewStatistics();

    /**
     * 获取所有的任务编排数据
     * <p>
     *     整个系统的数据
     * </p>
     *
     * @return
     *        结果信息
     */
    List<TaskPlan> getAllListIgnoreTenantId();
}
