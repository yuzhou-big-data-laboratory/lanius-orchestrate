/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.constant.schedule.kettle;

/**
 * Kettle平台返回结果XML常量
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-20 10:15
 */
public interface KettleXmlConstant {

    /**
     * 转换名称节点
     */
    String TRANS_NAME = "transname";

    /**
     * 日志日期节点
     */
    String LOG_DATE = "log_date";

    /**
     * 步骤状态集合节点
     */
    String STEP_STATUS_LIST_NODE = "stepstatuslist";

    /**
     * 步骤状态节点
     */
    String STEP_STATUS = "stepstatus";

    /**
     * 步骤名称
     */
    String STEP_NAME = "stepname";

    /**
     * 速度
     */
    String SPEED = "speed";

    /**
     * 时间节点
     */
    String SECONDS = "seconds";

    /**
     * 复制节点
     */
    String COPY = "copy";

    /**
     * 读取节点
     */
    String LINES_READ = "linesRead";

    /**
     * 写节点
     */
    String LINES_WRITTEN = "linesWritten";

    /**
     * 输入节点
     */
    String LINES_INPUT = "linesInput";

    /**
     * 输出节点
     */
    String LINES_OUTPUT = "linesOutput";

    /**
     * 更新节点
     */
    String LINES_UPDATED = "linesUpdated";

    /**
     * 拒绝
     */
    String LINES_REJECTED = "linesRejected";

    /**
     * 错误
     */
    String ERRORS = "errors";

    /**
     * 缓存数据量
     */
    String PRIORITY_VAL = "-";

    /**
     * 状态描述节点
     */
    String STATUS_DESC = "status_desc";

    /**
     * 全量日志节点
     */
    String LOGGING_STR = "logging_string";

    /**
     * 最后日志行数节点
     */
    String LAST_LOG_LINE_NR = "last_log_line_nr";
}
