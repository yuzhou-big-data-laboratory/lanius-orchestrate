/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.base.dto.DataBaseInfo;
import org.yzbdl.lanius.orchestrate.common.entity.resource.TaskResourceConfigEntity;
import org.yzbdl.lanius.orchestrate.serv.database.DbRepositoryInvokeHandler;

/**
 * 任务资源处理VO
 *
 * @author hujian@yzbdl.ac.cn
 * @date 2022-09-21 10:19
 */
@Data
@AllArgsConstructor
public class TaskResourceVO {

    /**
     * 资源库处理类
     */
    private DbRepositoryInvokeHandler handler;

    /**
     * 资源库配置信息
     */
    private TaskResourceConfigEntity entity;

    /**
     * 数据库信息
     */
    private DataBaseInfo dataBaseInfo;

}
