/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.constant.schedule.kettle;

/**
 * Kettle平台请求常量
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-20 10:49
 */
public interface KettleConstant {

	/**
	 * 调度基础路径API前缀
	 */
	String KETTLE_BASE_URL_API_PRE_PREFIX = "/kettle/";

	/**
	 * 调度路径日志参数
	 */
	String KETTLE_EXEC_URL_LOG_PARAM = "/?level=";

	/**
	 * 查询接口URL参数账户
	 */
	String QUERY_URL_PARAM_NAME = "/?name=";

	/**
	 * 查询接口URL参数cartObjectId
	 */
	String QUERY_URL_PARAM_CARTE_OBJECT_ID= "&id=";

	/**
	 * 查询接口URL参数XML
	 */
    String QUERY_URL_PARAM_XML= "&xml=y";

	/**
	 * 查询接口URL参数日志行数
	 */
	String QUERY_URL_PARAM_LOG_NUMBER = "&from=";

	/**
	 * 类型：作业、转换
	 */
	String QUERY_URL_PARAM_KTR_TYPE = "&type=";
}
