/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.handle.system;

import org.yzbdl.lanius.orchestrate.common.dto.system.OprLogFieldLabelDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 日志管理中需要映射的字段标识处理器
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-20 14:37
 */
public class FieldLabelHandle {

	List<OprLogFieldLabelDto> oprLogFieldLabels = new ArrayList<>();


	public void addFieldLabel(OprLogFieldLabelDto oprLogFieldLabelDto){
		this.oprLogFieldLabels.add(oprLogFieldLabelDto);
	}

	public String toMsg(String format){
		String resMsg = format;
		for(OprLogFieldLabelDto oprLogFieldLabelDto :oprLogFieldLabels){
			resMsg = resMsg.replaceAll("\\$\\{"+oprLogFieldLabelDto.getFieldName()+"}",oprLogFieldLabelDto.getFieldValue());
		}
		return resMsg;
	}
}
