/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.constant;

/**
 * 任务资源资源配置信息常量
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-15 14:09
 */
public interface ResourceContentConstant {

	/**
	 * 资源库名称
	 */
	String KETTLE_REPO_NAME = "repo_name";

	/**
	 * 资源库账号
	 */
	String KETTLE_REPO_ACCOUNT = "repo_account";

	/**
	 * 资源库类型
	 */
	String KETTLE_REPO_TYPE = "repo_type";

	/**
	 * 资源库密码
	 */
	String KETTLE_REPO_PASSWORD = "repo_password";

	/**
	 * 资源库描述
	 */
	String KETTLE_REPO_DESC = "repo_description";

	/**
	 * etl全路径
	 */
	String KETTLE_REPO_ENTRANCE = "repo_entrance";
}
