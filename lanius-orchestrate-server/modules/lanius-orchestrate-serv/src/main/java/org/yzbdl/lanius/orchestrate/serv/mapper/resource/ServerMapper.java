/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.resource;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.common.dto.resource.StatusStatisticDto;
import org.yzbdl.lanius.orchestrate.common.entity.resource.ServerEntity;

import java.util.List;

/**
 * 服务器数据操作层
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-03-29 14:46
 */
@Mapper
public interface ServerMapper extends BaseMapper<ServerEntity> {

	/**
	 * 状态统计接口
	 * @return 状态统计
	 */
	List<StatusStatisticDto> getStatusGroupCount();

	/**
	 * 获取所有服务器数据不区分主租户
	 * @return list务器数据
	 */
	@InterceptorIgnore(tenantLine = "on")
	List<ServerEntity> getAllServer();

	/**
	 * 更新服务器信息
	 * @param list list务器数据
	 */
	@InterceptorIgnore(tenantLine = "on")
	void batchUpdate(@Param("list") List<ServerEntity> list);

	/**
	 * 根据id获取服务器信息
	 * @param id 服务器id
	 * @return 服务器信息
	 */
	@InterceptorIgnore(tenantLine = "on")
	ServerEntity getServerInfoById(@Param("id") Long id);

}
