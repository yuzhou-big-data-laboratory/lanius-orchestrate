/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.entity.system.PermissionEntity;
import org.yzbdl.lanius.orchestrate.common.vo.system.PermissionMenuUrlVo;

import java.util.List;

/**
 * 权限服务
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-11 09:21
 */
public interface PermissionService extends IService<PermissionEntity> {
	/**
	 * 根据权用户id和组织id查询权限
	 * @param userId 用户id
	 * @return 权限列表
	 */
	List<PermissionEntity> queryPermissionsByUserId(Long userId);

	/**
	 * 根据菜单查询权限
	 * @param menuId 菜单id
	 * @return 权限列表
	 */
	List<PermissionEntity> listPermissionByMenuId(Long menuId);


	List<PermissionMenuUrlVo> listPermissionsWithMenuUrl();
}
