/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserLogEntity;
import org.yzbdl.lanius.orchestrate.common.vo.system.UserLogVo;

/**
 * 用户日志mapper
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-13 15:31
 */
@Mapper
public interface UserLogMapper extends BaseMapper<UserLogEntity> {
	/**
	 * 查询用户日志信息
	 * @param nickName 用户昵称
	 * @param wapper 筛选条件
	 * @param page 分页信息
	 * @return 返回数据
	 */
	Page<UserLogVo> queryUserLogByNickName(
			@Param("nickName") String nickName,
			@Param("ew") LambdaQueryWrapper<UserLogEntity> wapper,
			IPage<UserLogEntity> page
	);
}
