/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserEntity;
import org.yzbdl.lanius.orchestrate.common.vo.system.UserTokenVo;

/**
 * 普通用户认证服务接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-21 11:00
 */
public interface UserAuthService extends IService<UserEntity>, UserDetailsService {

	/**
	 * 用户登录
	 * @param username 用户名
	 * @param password 用户密码
	 * @return 用户token vo
	 */
	UserTokenVo login(String username, String password);

	/**
	 * 修改用户密码
	 * @param userId 用户id
	 * @param newPassword 新密码
	 * @param oldPassword 旧密码
	 * @return 成功与否
	 */
	Boolean updatePassword(Long userId, String newPassword, String oldPassword);

	/**
	 * 刷新token
	 * @param rfToken 刷新码
	 * @return
	 */
	UserTokenVo refresh(String rfToken);

	/**
	 * 根据组织刷新token
	 * 用在切换组织
	 * @param orgId 组织id
	 * @return 返回token实体
	 */
	UserTokenVo refreshTokenByOrgId(Long orgId);

	/**
	 * 通过userName来获取用户实体
	 * @param userName 用户名
	 * @return 实体对象
	 */
	UserEntity getUserByUserName(String userName);
}
