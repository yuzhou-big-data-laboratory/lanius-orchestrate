/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.utils;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.Set;

/**
 * 获取空值工具类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-24 13:39
 */
public class NullPropertyUtil {

	/**
	 * 获取实体对象为空字段
	 * @param source 资源数据
	 * @return  返回value为null的字段
	 */
    public static String[] getNullPropertyNames(Object source) {
    	final BeanWrapper src = new BeanWrapperImpl(source);
	    PropertyDescriptor[] pds = src.getPropertyDescriptors();
	    Set<String> emptyNames = new HashSet<>();
	    for(PropertyDescriptor pd : pds){
		    Object srcValue = src.getPropertyValue(pd.getName());
		    if(srcValue == null){
		    	emptyNames.add(pd.getName());
		    }
	    }
	    String[] result = new String[emptyNames.size()];
	    return emptyNames.toArray(result);
    }

}
