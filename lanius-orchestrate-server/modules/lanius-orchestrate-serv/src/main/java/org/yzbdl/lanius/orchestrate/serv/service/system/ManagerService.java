/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.common.dto.system.ManagerInsertParamDto;
import org.yzbdl.lanius.orchestrate.common.dto.system.ManagerListDto;
import org.yzbdl.lanius.orchestrate.common.entity.system.ManagerEntity;

/**
 * 管理员服务接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 10:59
 */
public interface ManagerService extends IService<ManagerEntity> {

    /**
     * 新增管理员
     * @param managerInsertParamDto 管理员新增参数类
     * @return 成功与否
     */
    boolean insertManager(ManagerInsertParamDto managerInsertParamDto);

    /**
     * 分页查询管理员列表
     * @param managerListDto 管理员查询条件
     * @param page 分页
     * @return 管理员列表
     */
    Page<ManagerEntity> listManagerPage(ManagerListDto managerListDto, Page<ManagerEntity> page);



}
