/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.quartz.handler.category;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 服务节点程序类型执行任务工厂
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-20 9:12
 */
@Slf4j
public class ProgramCategoryExecutorFactory {

    /**
     * 处理类收集
     */
    private static final Map<String, ProgramCategoryExecutorInvokerHandler> HANDLER_MAP = new ConcurrentHashMap<>();

    /**
     * 获取某个具体处理任务执行的类
     *
     * @param type
     *        类型
     * @return
     *        执行类
     */
    public static ProgramCategoryExecutorInvokerHandler getHandler(String type) {
        return HANDLER_MAP.get(type);
    }

    /**
     * 根据类型注册执行器
     *
     * @param type
     *        程序类型
     * @param invokeHandler
     *        执行器
     */
    public static void register(String type, ProgramCategoryExecutorInvokerHandler invokeHandler) {
        if (StringUtils.isBlank(type) || Objects.isNull(invokeHandler)) {
            log.error("注册任务执行处理类失败");
            return;
        }
        HANDLER_MAP.put(type, invokeHandler);
    }

}
