/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.yzbdl.lanius.orchestrate.common.entity.system.RolePermissionEntity;

import java.util.List;

/**
 * 角色权限关联mapper
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-11 09:29
 */
public interface RolePermissionMapper extends BaseMapper<RolePermissionEntity> {

	/**
	 * 批量插入数据
	 * @param rolePermissionEntityList 数据集合
	 * @return 返回数据
	 */
	int insertBatch(List<RolePermissionEntity> rolePermissionEntityList);
}
