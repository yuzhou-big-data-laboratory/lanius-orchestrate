/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.yzbdl.lanius.orchestrate.common.entity.system.UserOrgEntity;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.UserOrgMapper;
import org.yzbdl.lanius.orchestrate.serv.service.system.UserOrgService;

/**
 * 用户组织映射
 *
 * @author hujian@yzbdl.ac.cn
 * @date 2022-09-13 14:04
 */
@Service
public class UserOrgServiceImpl extends ServiceImpl<UserOrgMapper, UserOrgEntity> implements UserOrgService {


}
