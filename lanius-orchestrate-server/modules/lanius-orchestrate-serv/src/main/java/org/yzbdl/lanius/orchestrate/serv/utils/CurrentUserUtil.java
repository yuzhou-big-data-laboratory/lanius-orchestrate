/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.utils;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.yzbdl.lanius.orchestrate.common.dto.system.PayLoadClaimDto;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.BusinessException;

import java.util.Optional;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2020/6/16
 */
public class CurrentUserUtil {

    private final static String NO_USER_MSG = "当前没有上下文用户！";

    /**
     * 设置当前用户对象
     * 用于该线程用户信息的上下文共享
     * @param payLoadClaimDto 用户声明承载对象
     * @param user 用户实体
     */
    static public void setCurrentUser(PayLoadClaimDto payLoadClaimDto, UserDetails user){
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(payLoadClaimDto, null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    /**
     * 获取当前管用户
     * @return 用户声明承载对象
     */
    static public PayLoadClaimDto getCurrentUser(){
        try{
            return (PayLoadClaimDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 获取当前管用户
     * 严格模式，如果没有则抛异常
     * @return 用户声明承载对象
     */
    static public PayLoadClaimDto getCurrentUserIfNullThrow(){
        try{
            return (PayLoadClaimDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }catch (Exception e){
            throw new BusinessException(NO_USER_MSG);
        }
    }

    /**
     * 获取当前用户id
     * @return 返回当前用户id
     */
    static public Long getCurrentUserId(){
        return Optional.ofNullable(getCurrentUser()).map(PayLoadClaimDto::getUserId).orElseThrow(()->new BusinessException(NO_USER_MSG));
    }

    /**
     * 获取当前用户信息(无异常版本)
     * @return 返回当前用户id
     */
    static public Long getCurrentUserIdNoException(){
        return Optional.ofNullable(getCurrentUser()).map(PayLoadClaimDto::getUserId).orElse(null);
    }


    /**
     * 获取当前用户组织id
     * @return 组织id
     */
    static public Long getCurrentUserOrgId(){
        PayLoadClaimDto payLoadClaimDto = getCurrentUser();
        if(payLoadClaimDto!=null){
            return payLoadClaimDto.getOrgId();
        }
        throw new BusinessException(NO_USER_MSG);
    }

    /**
     * 是否是管理员
     * @return 布尔值
     */
    static public boolean isManager(){
        PayLoadClaimDto payLoadClaimDto = getCurrentUser();
        return payLoadClaimDto != null && payLoadClaimDto.isManager();
    }

    /**
     * 是否是普通用户
     * @return 布尔值
     */
    static public boolean isOrgChief(){
        PayLoadClaimDto payLoadClaimDto = getCurrentUser();
        return payLoadClaimDto != null && payLoadClaimDto.isOrgChief();
    }

    /**
     * 是否是普通用户
     * @return 布尔值
     */
    static public boolean isNormalUser(){
        PayLoadClaimDto payLoadClaimDto = getCurrentUser();
        return payLoadClaimDto != null && !payLoadClaimDto.isManager();
    }

    static public boolean hasCurrentUser(){
        try{
            return SecurityContextHolder.getContext().getAuthentication().getPrincipal()!=null;
        }catch (Exception e){
            return false;
        }
    }


}
