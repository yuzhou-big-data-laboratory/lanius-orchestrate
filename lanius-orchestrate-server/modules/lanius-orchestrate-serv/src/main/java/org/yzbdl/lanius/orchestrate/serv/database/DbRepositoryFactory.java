/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.database;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 数据库资源库处理类工厂
 *
 * @author hujian@yzbdl.ac.cn
 * @date 2022-09-20 16:46
 */
@Slf4j
public class DbRepositoryFactory {

    /**
     * 处理类缓存Map
     */
    public static Map<Integer, AbstractDbRepositoryInvokeHandler> handlerMap = new HashMap();

    /**
     * 获取某个具体处理Handler
     *
     * @param dbType
     *            类型
     * @return
     */
    public static AbstractDbRepositoryInvokeHandler getHandler(Integer dbType) {
        return handlerMap.get(dbType);
    }

    /**
     * 向工厂缓存Map中注册具体的执行类
     * 
     * @param dbType
     *            类型
     * @param invokeHandler
     *            执行类
     */
    public static void register(Integer dbType,
                                AbstractDbRepositoryInvokeHandler invokeHandler) {
        if (Objects.isNull(dbType) || Objects.isNull(invokeHandler)) {
            log.error("注册数据库资源库处理类失败");
            return;
        }
        handlerMap.put(dbType, invokeHandler);
    }
}
