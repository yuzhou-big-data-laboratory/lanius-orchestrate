/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.task.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.yzbdl.lanius.orchestrate.serv.service.task.TaskInstanceService;
import org.yzbdl.lanius.orchestrate.serv.service.task.TaskPlanService;
import org.yzbdl.lanius.orchestrate.common.vo.task.ScheduleTaskInstanceVO;
import org.yzbdl.lanius.orchestrate.common.vo.task.ScheduleTaskPlanVO;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 调度总览
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-14 16:58
 */
@Api(value = "调度总览控制管理相关接口", tags = "调度总览控制管理相关接口")
@RestController
@RequestMapping("/scheduler/view")
public class SchedulerViewController {

    @Autowired
    private TaskInstanceService taskInstanceService;

    @Autowired
    private TaskPlanService taskPlanService;

    /**
     * 任务实例、任务计划、资源配置统计
     *
     * @return
     *        结果信息
     */
    @CheckPermission("resource::statistic::query")
    @GetMapping(value = "/statistics")
    @ApiOperation(value = "任务实例、任务计划、资源配置统计", notes = "任务实例、任务计划、资源配置统计")
    @ResponseBody
    public ResultObj<Map<String, Object>> getTaskInstanceSchedulerView() {

        List<ScheduleTaskInstanceVO> schedulerViewTaskInstances = taskInstanceService.getSchedulerViewStatistics();
        List<ScheduleTaskPlanVO> schedulerViewTaskPlans = taskPlanService.getSchedulerViewStatistics();

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("schedulerViewTaskInstance", schedulerViewTaskInstances);
        resultMap.put("schedulerViewTaskPlan", schedulerViewTaskPlans);

        return ResultObj.success(resultMap);
    }

}
