/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.vo.system.PermissionMenuUrlVo;
import org.yzbdl.lanius.orchestrate.serv.service.system.PermissionService;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import java.util.List;

/**
 * 权限接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-11 13:31
 */
@Api(tags = "权限接口")
@RestController
@RequestMapping("/permission")
public class PermissionController {


	@Autowired
	PermissionService permissionService;

	@CheckPermission("role::query")
	@ApiOperation(value="获取所有权限", notes="")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResultObj<List<PermissionMenuUrlVo>> getAll(){
		return ResultObj.success(permissionService.listPermissionsWithMenuUrl());
	}

}
