/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.dto.system.*;
import org.yzbdl.lanius.orchestrate.common.entity.system.ManagerEntity;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.InvalidCodeException;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.utils.MessageUtil;
import org.yzbdl.lanius.orchestrate.common.vo.system.ManagerTokenVo;
import org.yzbdl.lanius.orchestrate.serv.service.system.ManagerAuthService;
import org.yzbdl.lanius.orchestrate.serv.service.system.ManagerService;
import org.yzbdl.lanius.orchestrate.serv.service.system.OrgService;
import org.yzbdl.lanius.orchestrate.serv.service.system.UserService;
import org.yzbdl.lanius.orchestrate.serv.utils.CurrentUserUtil;
import org.yzbdl.lanius.orchestrate.serv.utils.VerifyCodeUtil;

/**
 * 管理员访问接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 12:48
 */
@Api(tags = "管理员接口")
@RestController
@RequestMapping("/manager")
@Validated
public class ManagerController {

    @Autowired
    ManagerService managerService;
    @Autowired
    ManagerAuthService managerAuthService;
    @Autowired
    OrgService orgService;
    @Autowired
    UserService userService;

    @Value("${verify-code.skip}")
    boolean verifyCodeSkip;

    @ApiOperation(value = "管理员登录", notes = "")
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResultObj<?> login(@RequestBody ManagerLoginParamDTO managerLoginParamDto, @RequestParam String key) {
        if (!verifyCodeSkip && !VerifyCodeUtil.verify(key, managerLoginParamDto.getCode())) {
            throw new InvalidCodeException(MessageUtil.get("system.user.login.valid_code_fail"));
        }
        return ResultObj.success(managerAuthService.login(managerLoginParamDto.getUserName(), managerLoginParamDto.getPassword()));
    }

    @ApiOperation(value = "刷新token", notes = "")
    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    public ResultObj<ManagerTokenVo> refresh(@RequestBody @Validated TokenRefreshDto tokenRefreshDto) {
        return ResultObj.success(managerAuthService.refresh(tokenRefreshDto.getRefreshToken()));
    }

    @ApiOperation(value = "管理员添加", notes = "")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResultObj<Boolean> insert(@RequestBody @Validated ManagerInsertParamDto managerInsertParamDto) {
        return ResultObj.success(managerService.insertManager(managerInsertParamDto));
    }

    @ApiOperation(value = "分页查看管理员", notes = "")
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ResultObj<Page<ManagerEntity>> listManagerPage(
            @RequestParam(defaultValue = "20") int size,
            @RequestParam(defaultValue = "1") int page,
            @RequestBody @Validated ManagerListDto managerListDto
    ) {
        return ResultObj.success(managerService.listManagerPage(managerListDto, new Page<>(page, size)));
    }

    @ApiOperation(value = "删除管理员", notes = "")
    @RequestMapping(value = "{managerId}", method = RequestMethod.DELETE)
    public ResultObj<?> deleteManagerById(@PathVariable("managerId") String managerId) {
        if (managerService.count() <= 1) {
            return ResultObj.error("不能删除唯一的管理员");
        }
        return ResultObj.success(managerService.removeById(managerId));
    }

    @ApiOperation(value = "修改密码", notes = "")
    @RequestMapping(value = "/password", method = RequestMethod.POST)
    public ResultObj<Boolean> updatePassword(@RequestBody @Validated UpdatePasswordDto updatePasswordDto) {
        return ResultObj.success(managerAuthService.updatePassword(
                CurrentUserUtil.getCurrentUserId(),
                updatePasswordDto.getNewPassword(),
                updatePasswordDto.getOldPassword()
        ));
    }

}
