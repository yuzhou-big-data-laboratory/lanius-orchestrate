package org.yzbdl.lanius.orchestrate.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.dto.system.UserInsertParamDTO;
import org.yzbdl.lanius.orchestrate.common.dto.system.UserListDTO;
import org.yzbdl.lanius.orchestrate.common.dto.system.UserOrgUpdateParamDTO;
import org.yzbdl.lanius.orchestrate.common.dto.system.UserStatusDTO;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.vo.system.AccountVO;
import org.yzbdl.lanius.orchestrate.serv.service.system.UserService;

import javax.validation.constraints.NotNull;

/**
 * 管理员账户访问接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-08-30 15:56
 */
@Api(tags = "管理员账号接口")
@RestController
@RequestMapping("/manager/account")
@Validated
public class ManagerAccountController {

    @Autowired
    UserService userService;

    @ApiOperation(value = "分页查询用户", notes = "")
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ResultObj<Page<AccountVO>> queryByPage(
            @RequestParam(defaultValue = "20") int size,
            @RequestParam(defaultValue = "1") int page,
            @RequestBody @Validated UserListDTO userListDto
    ) {
        return ResultObj.success(userService.listAccountPage(userListDto, new Page<>(page, size)));
    }

    @ApiOperation(value = "新增用户", notes = "")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResultObj<Boolean> insertUser(@RequestBody @Validated UserInsertParamDTO userInsertParamDto) {
        userService.insertAccount(userInsertParamDto);
        return ResultObj.success(true);
    }

    @ApiOperation(value = "更改用户", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResultObj<Boolean> updateUser(
            @RequestBody @Validated UserOrgUpdateParamDTO userOrgUpdateParamDTO,
            @PathVariable("id") Long id
    ) {
        return ResultObj.success(userService.updateUser(userOrgUpdateParamDTO, id));
    }

    @ApiOperation(value = "删除账户", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResultObj<Boolean> deleteUser(@PathVariable("id") @NotNull Long id) {
        return ResultObj.success(userService.deleteUserById(id));
    }

    @ApiOperation(value = "设置用户状态", notes = "")
    @RequestMapping(value = "/{userId}/status", method = RequestMethod.POST)
    public ResultObj<Boolean> changeStatus(@PathVariable("userId") @OprId(server = UserService.class) Long userId, @RequestBody @Validated UserStatusDTO userStatusDto) {
        return ResultObj.success(userService.setUserState(userStatusDto.getStatus(), userId));
    }

    @ApiOperation(value = "重置用户密码", notes = "")
    @RequestMapping(value = "/{userId}/password", method = RequestMethod.POST)
    public ResultObj<String> resetPassword(@PathVariable("userId") @OprId(server = UserService.class) Long userId) {
        return ResultObj.success(userService.resetPassword(userId));
    }


}
