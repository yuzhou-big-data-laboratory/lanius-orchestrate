package org.yzbdl.lanius.orchestrate.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.dto.system.RoleInsertParamDto;
import org.yzbdl.lanius.orchestrate.common.dto.system.RoleListDto;
import org.yzbdl.lanius.orchestrate.common.dto.system.RoleUpdateParamDto;
import org.yzbdl.lanius.orchestrate.serv.service.system.RoleService;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLog;
import org.yzbdl.lanius.orchestrate.common.chain.DeleteCheckChain;
import org.yzbdl.lanius.orchestrate.common.chain.DeleteExploreDto;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import java.util.List;

/**
 * 角色管理
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 16:50
 */
@Api(tags = "角色接口")
@RestController
@RequestMapping("/role")
@Validated
public class RoleController {

    @Autowired
    RoleService roleService;

    @OprLog(value = UserOprEventConstant.SYSTEM_ROLE_ADD,msg="新增了角色${roleName}")
    @CheckPermission("system::role::edit")
    @ApiOperation(value="新增角色", notes="")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResultObj insert(@RequestBody @Validated RoleInsertParamDto roleInsertParamDto){
        return ResultObj.success(roleService.saveRole(roleInsertParamDto));
    }

    @OprLog(value = UserOprEventConstant.SYSTEM_ROLE_EDIT,msg="修改为角色${roleName},id是${id}")
    @CheckPermission("system::role::edit")
    @ApiOperation(value="修改角色", notes="")
    @RequestMapping(value = "{roleId}", method = RequestMethod.POST)
    public ResultObj updateById(
            @RequestBody @Validated RoleUpdateParamDto roleUpdateParamDto,
            @PathVariable("roleId") @OprId(server = RoleService.class) Long roleId
    ){
        return ResultObj.success(roleService.updateRole(roleUpdateParamDto.coverToRoleEntity(roleId)));
    }

    @CheckPermission("system::role::query")
    @ApiOperation(value="根据id获取角色", notes="")
    @RequestMapping(value = "/{roleId}", method = RequestMethod.GET)
    public ResultObj getById(@PathVariable("roleId") String roleId){
        return ResultObj.success(roleService.getById(roleId));
    }

    @OprLog(value = UserOprEventConstant.SYSTEM_ROLE_EDIT,msg="保存角色${roleName}权限")
    @CheckPermission("system::role::edit")
    @ApiOperation(value="保存角色权限", notes="")
    @RequestMapping(value = "/{roleId}/permission", method = RequestMethod.POST)
    public ResultObj saveRolePermission(@RequestBody List<Long> permissionIds, @PathVariable @OprId(server = RoleService.class) Long roleId){
        return ResultObj.success(roleService.savePermissionIdsByRoleId(roleId,permissionIds));
    }

    @CheckPermission("system::role::query")
    @ApiOperation(value="分页查询角色", notes="")
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ResultObj queryByPage(@RequestParam(defaultValue = "20") int size, @RequestParam(defaultValue = "1") int page,@RequestBody @Validated RoleListDto roleListDto){
        return ResultObj.success(roleService.listRolePage(roleListDto,new Page<>(page,size)));
    }

    @CheckPermission("system::role::query")
    @ApiOperation(value="查询全部角色", notes="")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResultObj listALL(){
        return ResultObj.success(roleService.list());
    }


    @CheckPermission("system::role::delete")
    @ApiOperation(value="删除角色", notes="")
    @RequestMapping(value = "/{roleId}", method = RequestMethod.DELETE)
    @OprLog(value = UserOprEventConstant.SYSTEM_ROLE_DELETE,msg="删除角色${roleName}，id是${id}")
    public ResultObj deleteById(@PathVariable("roleId") @OprId(server = RoleService.class) Long roleId){
        DeleteExploreDto deleteExploreDto = new DeleteCheckChain()
                .addChain(() -> roleService.deleteRoleById(roleId))
                .process();
        return deleteExploreDto.isOk()?
                ResultObj.success("成功！"):ResultObj.error(deleteExploreDto.getReason());
    }


}
