/*
 *
 * Copyright(c) YuZhou Big Data Laboratory CO.,Ltd, 2022. All rights reserved
 */
package org.yzbdl.lanius.orchestrate.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.dto.system.ManagerOrgFrozenDTO;
import org.yzbdl.lanius.orchestrate.common.dto.system.OrgParamIdDTO;
import org.yzbdl.lanius.orchestrate.common.dto.system.OrgQueryDTO;
import org.yzbdl.lanius.orchestrate.common.entity.system.OrgEntity;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.utils.MessageUtil;
import org.yzbdl.lanius.orchestrate.common.vo.system.OrgParamDTO;
import org.yzbdl.lanius.orchestrate.serv.service.system.OrgService;

import java.util.List;

/**
 * 组织管理接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-08-08 14:00
 */
@Api(tags = "管理员组织接口")
@RestController
@RequestMapping("/manager/org")
@Validated
public class ManagerOrgController {

    @Autowired
    OrgService orgService;

    @ApiOperation(value = "分页查看组织", notes = "")
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ResultObj<Page<OrgEntity>> queryOrg(
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "1") int page,
            @RequestBody OrgQueryDTO orgQueryDto
    ) {
        return ResultObj.success(orgService.listOrgPage(orgQueryDto, new Page<>(page, size)));
    }

    @ApiOperation(value = "查看所有组织", notes = "")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResultObj<List<OrgEntity>> queryAllOrg(@RequestParam(required = false) String orgName) {
        return ResultObj.success(orgService.listOrgByOrgName(orgName));
    }

    @ApiOperation(value = "新增组织")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResultObj<?> insertOrg(@RequestBody @Validated OrgParamDTO orgParamDto) {
        if (orgService.existOrg(orgParamDto.getOrgName())) {
            return ResultObj.error(MessageUtil.get("system.org.same_org_name"));
        }
        orgService.save(orgParamDto.toOrgEntity());
        return ResultObj.success();
    }

    @ApiOperation(value = "修改组织", notes = "")
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResultObj<Boolean> updateOrg(@RequestBody @Validated OrgParamIdDTO orgParamIdDto) {
        OrgEntity orgEntity = OrgEntity.builder().orgName(orgParamIdDto.getOrgName()).build();
        orgEntity.setId(orgParamIdDto.getId());
        return ResultObj.success(orgService.updateOrg(orgEntity));
    }

    @ApiOperation(value = "删除组织", notes = "")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResultObj<?> deleteOrgById(@PathVariable("id") Long id) {
        return orgService.removeCascade(id) ? ResultObj.success("成功！") : ResultObj.error(MessageUtil.get("system.org.delete_org_error"));
    }

    @ApiOperation(value = "冻结组织", notes = "")
    @RequestMapping(value = "/{orgId}/frozen", method = RequestMethod.POST)
    public ResultObj<Boolean> deleteOrgById(
            @RequestBody @Validated ManagerOrgFrozenDTO managerOrgFrozenDto,
            @PathVariable("orgId") Long orgId
    ) {
        return ResultObj.success(orgService.changeOrgFrozen(managerOrgFrozenDto.getFrozen(), orgId));
    }

}
