/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.dto.system.UserLogQueryDto;
import org.yzbdl.lanius.orchestrate.serv.service.system.UserLogService;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

/**
 * 用户操作日志
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-13 17:12
 */
@Api(tags = "用户操作日志相关接口")
@RestController
@RequestMapping("/opr")
public class UserLogController {

	@Autowired
	UserLogService userLogService;

	@CheckPermission("system::userLog::query")
	@ApiOperation(value="获取所有日志事件", notes="")
	@RequestMapping(value = "/event", method = RequestMethod.GET)
	public ResultObj getAllEvent(){
		return ResultObj.success(userLogService.getAllEventName());
	}

	@CheckPermission("system::userLog::query")
	@ApiOperation(value="查询日志", notes="")
	@RequestMapping(value = "/query", method = RequestMethod.POST)
	public ResultObj query(@RequestParam int size, @RequestParam int page, @RequestBody UserLogQueryDto userLogQueryDto){
		return ResultObj.success(userLogService.listUserLogsPage(new Page<>(page, size),userLogQueryDto));
	}


}
