/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.resource.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.dto.resource.TaskResourceTreeDto;
import org.yzbdl.lanius.orchestrate.common.entity.resource.TaskResourceGroupEntity;
import org.yzbdl.lanius.orchestrate.serv.service.resource.TaskResourceGroupService;
import org.yzbdl.lanius.orchestrate.serv.service.resource.TaskResourceService;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLog;
import org.yzbdl.lanius.orchestrate.common.base.validated.Insert;
import org.yzbdl.lanius.orchestrate.common.chain.DeleteCheckChain;
import org.yzbdl.lanius.orchestrate.common.chain.DeleteExploreDto;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import java.util.List;

/**
 * 任务资源分组接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-11 11:33
 */
@Api(tags = "任务资源分组接口")
@RequestMapping("/resource/taskResourceGroup")
@RestController
@AllArgsConstructor
public class TaskResourceGroupController {

	private final TaskResourceGroupService taskResourceGroupService;

	private final TaskResourceService taskResourceService;

	/**
	 * 构建任务资源树结构
	 * @return TaskResourceTreeDto 构建任务资源树
	 */
	@CheckPermission("resource::taskResource::query")
	@ApiOperation(value = "构建任务资源树结构")
	@GetMapping("/tree")
	public ResultObj<List<TaskResourceTreeDto>> buildResourceTree(){
		return ResultObj.success(taskResourceGroupService.buildResourceTree());
	}

	/**
	 * 添加任务资源分组数据
	 * @param taskResourceGroupEntity 待添加任务资源分组数据
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_GROUP_ADD,msg="添加${groupName}任务资源分组数据")
	@CheckPermission("resource::taskResource::edit")
	@ApiOperation(value = "添加任务资源分组")
	@PostMapping()
	public ResultObj<Boolean> add(@RequestBody @Validated(Insert.class) TaskResourceGroupEntity taskResourceGroupEntity) {
		return ResultObj.success(taskResourceGroupService.addTaskResourceGroup(taskResourceGroupEntity));
	}

	/**
	 * 更新任务分组
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_GROUP_UPDATE,msg="更新${groupName}任务资源分组数据")
	@CheckPermission("resource::taskResource::edit")
	@ApiOperation(value = "更新任务分组")
	@PutMapping()
	public ResultObj<Boolean> updateTaskResourceGroup(@RequestBody @Validated TaskResourceGroupEntity taskResourceGroupEntity) {
		return ResultObj.success(taskResourceGroupService.updateTaskResourceGroup(taskResourceGroupEntity));
	}

	/**
	 * 逻辑删除任务资源分组数据
	 * @param id id
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_GROUP_DELETE,msg="删除id:${id}任务资源分组数据")
	@CheckPermission("resource::taskResource::edit")
	@ApiOperation(value = "逻辑删除任务资源分组")
	@DeleteMapping("/{id}")
	public ResultObj<Boolean> delete(@PathVariable @OprId(server = TaskResourceGroupService.class) Long id) {
		return ResultObj.success(taskResourceGroupService.deleteTaskResourceGroup(id));
	}

    /**
     * 逻辑删除任务资源分组数据
     * @param id id
     * @return true|false
     */
    @OprLog(value = UserOprEventConstant.TASK_RESOURCE_GROUP_DELETE,msg="删除id:${id}任务资源分组数据")
    @CheckPermission("resource::taskResource::edit")
    @ApiOperation(value = "逻辑删除任务资源分组")
    @DeleteMapping("/deleteByChain/{id}")
    public ResultObj<String> deleteByChain(@PathVariable @OprId(server = TaskResourceGroupService.class) Long id) {
        DeleteExploreDto deleteExploreDto =
            new DeleteCheckChain().addChain(!taskResourceService.isExistsTaskResource(id), "请清除当前目录下的内容！")
                .addChain(!taskResourceGroupService.isExistChildrenTaskResourceGroup(id),
                    () -> taskResourceGroupService.directlyDeleteTaskResourceGroup(id), "请清除当前目录下的子目录！")
                .process();
        return deleteExploreDto.isOk() ? ResultObj.success() : ResultObj.error(deleteExploreDto.getReason());
    }

	/**
	 * 根据id获取任务资源分组数据
	 * @param id id
	 * @return 根据id获取任务资源分组数据
	 */
	@CheckPermission("resource::taskResource::query")
	@ApiOperation(value = "获取任务")
	@GetMapping()
	public ResultObj<TaskResourceGroupEntity> getTaskResourceGroup(@RequestParam Long id) {
		return ResultObj.success(taskResourceGroupService.getTaskResourceGroup(id));
	}

	/**
	 * 通过名称模糊匹配分组
	 * @param groupName 分组名称
	 * @return 返回详细分组值
	 */
	@CheckPermission("resource::taskResource::query")
	@ApiOperation(value = "通过名称模糊匹配分组")
	@GetMapping("/getTaskResourceGroupByName")
	public ResultObj<List<TaskResourceGroupEntity>> getTaskResourceGroupByName(@RequestParam String groupName) {
		return ResultObj.success(taskResourceGroupService.getTaskResourceGroup(groupName));
	}

}
