/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.resource.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLog;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ServerProgramDto;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ServerProgramPageDto;
import org.yzbdl.lanius.orchestrate.common.entity.resource.ServerProgramEntity;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.utils.AesUtils;
import org.yzbdl.lanius.orchestrate.common.utils.ExceptionUtil;
import org.yzbdl.lanius.orchestrate.serv.service.resource.ServerProgramService;

import java.util.Objects;

/**
 * 服务节点模块接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-08 15:35
 */
@Api(tags = "服务节点模块接口")
@RequestMapping("/resource/serverProgram")
@RestController
@RequiredArgsConstructor
public class ServerProgramController {

	private final ServerProgramService serverProgramService;

	@Value("${encryption.user-key}")
	String encryptKey;

	/**
	 * 添加服务节点数据
	 * @param serverProgramDto 待添加服务节点数据
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.SERVER_PROGRAM_ADD,msg="新增${programName}服务节点数据")
	@CheckPermission("resource::serverProgram::edit")
	@ApiOperation(value = "添加服务节点")
	@PostMapping()
	public ResultObj<Boolean> add(@RequestBody @Validated ServerProgramDto serverProgramDto) {
		return ResultObj.success(serverProgramService.addServerProgram(serverProgramDto));
	}

	/**
	 * 逻辑删除服务节点数据
	 * @param id id
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.SERVER_PROGRAM_DELETE,msg="删除${programName}服务节点数据")
	@CheckPermission("resource::serverProgram::edit")
	@ApiOperation(value = "逻辑删除服务节点")
	@DeleteMapping("/{id}")
	public ResultObj<Boolean> delete(@PathVariable @OprId(server = ServerProgramService.class) Long id) {
		return ResultObj.success(serverProgramService.deleteServerProgram(id));
	}

	/**
	 * 根据id获取服务节点数据
	 * @param id id
	 * @return 根据id获取服务节点数据
	 */
	@CheckPermission("resource::serverProgram::query")
	@ApiOperation(value = "获取任务")
	@GetMapping()
	public ResultObj<ServerProgramEntity> getServerProgram(@RequestParam Long id) {
		return ResultObj.success(serverProgramService.getServerProgram(id));
	}

    /**
     * 分页查询服务节点数据
     * @param serverProgramPageDto serverProgramPageDto 分页参数
     * @return 分页数据
     */
    @CheckPermission("resource::serverProgram::query")
    @ApiOperation(value = "分页查看服务节点")
    @PostMapping("query")
    public ResultObj<IPage<ServerProgramDto>> pageServerProgram(@RequestParam(defaultValue = "1") Integer page,
        @RequestParam(defaultValue = "20") Integer size,
        @RequestBody ServerProgramPageDto serverProgramPageDto) {
        return ResultObj.success(serverProgramService.pageServerProgram(page, size, serverProgramPageDto));
    }

	/**
	 * 更新服务节点数据
	 * @param serverProgramDto 待更新服务节点参数
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.SERVER_PROGRAM_UPDATE,msg="更新${programName}服务节点数据")
	@CheckPermission("resource::serverProgram::edit")
	@ApiOperation(value = "更新服务节点数据")
	@PutMapping()
	public ResultObj<Boolean> updateServerProgram(@RequestBody @Validated ServerProgramDto serverProgramDto) {
		return ResultObj.success(serverProgramService.updateServerProgram(serverProgramDto));
	}

	/**
	 * 测试服务节点连通性
	 */
	@CheckPermission("resource::serverProgram::query")
	@ApiOperation(value = "测试服务节点连通性")
	@PostMapping("/testConnection")
	public ResultObj<Integer> testConnection(@RequestBody ServerProgramDto serverProgramDto) {
        ExceptionUtil.checkParam(
            StringUtils.hasLength(serverProgramDto.getServerIp()) && Objects.nonNull(serverProgramDto.getProgramPort())
                && StringUtils.hasLength(serverProgramDto.getUserName())
                && StringUtils.hasLength(serverProgramDto.getPassword()),
            "请确保节点ip、端口、账户、密码完全输入！");
        // 密码解密
		serverProgramDto.setPassword(AesUtils.decrypt(serverProgramDto.getPassword(), encryptKey));
        return ResultObj.success(serverProgramService.testConnectionExecuteByScheduled(serverProgramDto.getServerId(),
				serverProgramDto.getServerIp(), serverProgramDto.getProgramPort(), serverProgramDto.getUserName(),
				serverProgramDto.getPassword()));
	}

}

