/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.resource.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yzbdl.lanius.orchestrate.common.dto.resource.ResourceStatisticDto;
import org.yzbdl.lanius.orchestrate.serv.service.resource.ResourceStatisticService;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;

/**
 * 资源状态统计接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-18 11:23
 */
@Api(tags = "资源统计接口")
@RequestMapping("/resource/statistic")
@RestController
@AllArgsConstructor
public class ResourceStatisticController {

	private final ResourceStatisticService resourceStatisticService;

	/**
	 * 获取统计资源状态统计结果
	 * @return 统计量
	 */
	@CheckPermission("resource::statistic::query")
	@ApiOperation(value = "获取统计资源状态统计结果")
	@GetMapping("/getStatusStatistic")
	public ResultObj<ResourceStatisticDto> getStatusStatistic() {
		return ResultObj.success(resourceStatisticService.getStatusStatistic());
	}

}
