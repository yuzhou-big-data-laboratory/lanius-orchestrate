Lanius常见问题列表
-----------------
#### 2022-08-23
#### Q: 如何部署并开始使用？
- 请参阅《<a target='_blank' href ='/README.md'>README.md</a>》中的部署教程进行产品的部署，需要注意按部署文档中的说明步骤操作，提高一次性部署成功率，遇到问题可以在QQ群<a href="https://qm.qq.com/cgi-bin/qm/qr?k=2JFFIbTFGQwyFFmfiwS-Tqn0OysAHPIg&authKey=QGrhB288t7TDopZbz5x1z3y8UZhR1%2FQ7U1H94H5Q62F58htTXsEtP0bCqUstqOch&noverify=0&group_code=155800768">155800768</a> 进行交流。

#### Q: 调度器安装之后，用户名和密码是啥？
- 在我们的README.MD中有描述；默认超管账户密码是"admin/123QWEqwe!@#"。

#### Q: MySQL的版本分别是多少?
- MySQL的版本要求是8.0。

#### Q: 调度器现在只支持MySQL？
- 调度器自身数据库及调度器中的资源库目前暂时只支持MySQL，调度器正在对达梦进行适配，预计不久后上线。

#### Q: 资源库选项只有MySQL可选？
- 资源库的支持项用户可以在issue中提出需求，将会在后续优先进行适配。

#### Q: JDK版本是多少？
- 调度器的JDK版本分别是11，与执行器（设计器）是1.8。

#### Q: 调度器与设计器后续版本能否统一？
- 后续将对调度器与执行器（设计器）的JDK版本进行统一，调度器也会支持其他版本的MySQL数据库和其他数据库。

#### Q: 是否需要初始化数据库，以及如何初始化数据库？
- 数据库需要先初始化，使用安装教程中提到的数据库初始化SQL脚本即可初始化数据库。

#### Q: 调度器初始化后无法登录，lo_user这个表有没有初始化脚本？
- 调度器有两个登录地址。一个是管理员访问地址( http://ip:8083/admin/login )，管理员登录新建组织和用户（完成`lo_user`初始化）；然后组织用户即可访问用户访问地址( http://ip:8083/login )，使用管理员创建的组织用户进行登录。

#### Q: 能否提供打好包的docker镜像，或提供基于docker-compose all-in-one 的运行环境？？
- 我们已经提供了一键部署脚本，请参考《<a target='_blank' href ='autorun/AUTORUN.md'>自动部署手册</a>》中的使用说明下载对应的脚本文件使用Linux进行快速自动在线（离线初始化请从QQ群获取离线镜像包）初始化实现All In One。

#### Q: 有设计器、执行器了，怎么还需要另外一套调度器，是不是可以整合下？目前这样部署是否过于麻烦？
- 我们的产品分设计器、执行器、调度器，目前仅依靠设计器和执行器模块并不能实现跨平台的任务调度，配合调度器就可以实现这样的能力；在我们的规划中调度平台未来只可以调度我们自己的执行器，还会继续扩展调度其他任务平台，所以单独的调度器模块是有必要的；后续为了简化会将调度平台两个仓库进行合并，降低用户上手难度及使用成本。目前我们在不断完善部署方案，相信不久后能为广大用户提供更多可选的简便快捷部署方式。


#### Q: 安装好的设计器、执行器里面没有插件，如何获取和安装插件？
- 设计器点击`工具-插件管理-插件安装`，选择程序目录`addons`中的插件进行安装，第一次装建议点击`批量插件安装`选中上述目录进行快速安装。命令行可以使用`.\lanius-extend.bat(linux脚本后缀.sh) -d .\addons\` 可以快速批量安装，其他操作参见`.\lanius-extend.bat(linux脚本后缀.sh) -h`。

#### Q: 有没有Windows平台的详细安装教程？
- 工程师们正在编写基于Windows器的部署脚本和教程。

#### Q: 是否有详细功能介绍和产品的能力描述文档?
- 后续会提供产品白皮书，请关注本项目的更新和QQ群里的公告。

#### Q: 双击执行器 lanius-executor-web.bat 闪退，如何启动设计器和执行器程序？
- Windows用户，请使用后缀为`.bat`结尾的脚本，Linux用户请使用`.sh`后缀结尾的脚本；例如Windows用户启动执行器需要在程序目录启动命令行窗口使用`lanius-executor-web.bat`脚本配合配置信息进行启动，在程序`pwd`目录中有提供默认启动配置，具体命令参考：`.\lanius-executor-web.bat .\pwd\carte-config-master-8080.xml`，用户可根据需要修改`xml`中的端口认证等信息；Windows用户启动设计器请在程序目录双击或者命令行执行`lanius-design.bat`脚本启动。

#### Q: 执行器Web界面的初始账号密码是什么？
- 执行器的默认账号密码都为"cluster/cluster"。

#### Q: 平台可以用于生产吗?
- 平台允许在任何环境使用。

#### Q: 调度器开源，设计器收费?
- 调度器免费，设计器也免费，只是暂时先开源调度器。

#### Q: 用户操作手册和说明文档有吗？
- 用户操作手册，后续会发布出来的，请继续关注我们；产品说明请参考《<a target='_blank' href ='/README.md'>README.md</a>》文档。


#### Q: 调度平台新增资源库时，提示报错信息“远程资源库连接异常，请检查资源库！”，日志输出“Table 'lanius.r_job' doesn't exist”
- 需要先使用设计器初始化资源库，然后才能通过调度平台新增资源库。参考issues: https://gitee.com/yuzhou-big-data-laboratory/lanius-orchestrate/issues/I5NVP2

#### Q: 调度平台执行job或trans，是用Carte方式吗？源代码是哪个文件？
- 是采用的Carte方式，源代码文件可参考：https://gitee.com/yuzhou-big-data-laboratory/lanius-orchestrate/blob/master/lanius-orchestrate-server/modules/orchestrate-serv/src/main/java/org/yzbdl/lanius/orchestrate/serv/quartz/handler/category/impl/KettleExecutorHandler.java

#### Q: 项目构建报错：Could not find artifact org.apache.xerces:com.springsource.org.apache.xerces:pom:2.8.1 ，依赖包拉取失败
- common模块下的pom.xml使用下面配置进行替换：
```
<dependency>
    <groupId>xerces</groupId>
    <artifactId>xercesImpl</artifactId>
    <version>2.8.1</version>
</dependency>
```

#### Q: 支不支持在线的流程设计和大数据cdh的连接？
- 目前不支持

#### Q: 执行初始化脚本./lanius_init.sh时，会自动安装一个数据库？
- 会的，也可以指定MySql8数据库。具体操作如下：
- 1、在已有MySql8数据库中初始化/yzdbl/db_init/目录下的SQL脚本；
- 2、修改/yzdbl/config/application-prod.yml文件，跟换数据源；
- 3、注释lanius_init.sh文件中的42、43行；
- 5、执行初始化脚本./lanius_init.sh。

#### Q: kettle写的作业在Lanius调度器上面是否能执行?
- 能执行。

#### Q: 是否能离线部署Lanius？
- 我们只提供Lanius程序相关的包，需要用户具备Docker及Docker Compose环境。

#### Q: Lanius用于商业需要授权吗？
- Lanius的来源协议是MulanPSL 2.0，不需要授权，可以用于商业。

#### Q: Lanius需要哪个版本的JDK？
- Lanius v1.0支持JDK8，Lanius v1.0.5支持JDK11。

#### Q: 在什么情况下使用git资源库？
- 用于ktr、kjb的版本管理。Git资源库在跟程序集成之后，就不需要搭建数据库资源库或者文件资源库了，程序启动后就可以使用。

#### Q: 在离线环境下，Lanius的所有功能能不能正常运行？
- 可以正常运行。
