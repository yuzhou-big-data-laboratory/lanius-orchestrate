# Lanius 容器快速安装说明

----

本安装程序适合在红帽系操作系统运行，其他系统请自行验证。   
说明：<a target='_blank' href ='Lanius容器一键部署.v1.0.0.tar'>点击此处下载</a>《Lanius容器一键部署.v1.0.0.tar》压缩包拷贝到Linux服务器，解压并进入目录。

## 在线安装

> 1、执行目录中的`lanius_init.sh`脚本等待安装完成。    


## 离线安装

> 1、从官方QQ群：<a href="https://qm.qq.com/cgi-bin/qm/qr?k=2JFFIbTFGQwyFFmfiwS-Tqn0OysAHPIg&authKey=QGrhB288t7TDopZbz5x1z3y8UZhR1%2FQ7U1H94H5Q62F58htTXsEtP0bCqUstqOch&noverify=0&group_code=155800768">155800768</a> 下载群文件>程序>`Lanius离线镜像xxx.zip` 包解压并上传到部署脚本`offline_images`目录中。    
> 2、执行目录中的`lanius_init_offline.sh`脚本等待程序初始化完成。 


## 提示
> 1、首次使用请访问部署服务器IP:8083/admin/login进行组织初始化。   
> 2、程序将默认初始化数据库及前后端程序外加一个执行器（通过组织用户进入系统后可以再执行程序进行关联），若需要多个执行器，请单独拷贝`executor`目录使用`docker-compose`命令进行启动。   
> 3、对于离线环境，需要用户自己解决Docker及Docker Compose环境问题。   
> 4、访问系统出现无法登录请检查系统时间是否正确。   

## 更新说明
> 1.解压下载的《Lanius容器一键部署.vxx.tar》解压到覆盖到原来的位置。    
> 2.执行目录中的`lanius_update.sh`脚本进行程序更新。    


## 版本历史
> 2022-08-18 - <a target='_blank' href ='Lanius容器一键部署.v1.0.0.tar'>Lanius容器一键部署.v1.0.0.tar</a>    
>> 版本说明：    
>> 1.重置构建仓库引用，构建稳定版本。    
>> 2.补充一键更新脚本    
> 
> 2022-08-17 - <a target='_blank' href ='Lanius容器一键部署.v0.0.1.tar'>Lanius容器一键部署.v0.0.1.tar</a>  
>> 版本说明：    
>> 1.初始版本。
