部署文档

# 1 基础环境(必备)

​    JDK:11+、MYSQL:8+、Nginx

# 2 前后端程序编译、打包

​前后端程序编译、打包。请参考<a target='_blank' href ='Build.md'>本地编译文档</a>

# 3 初始化数据库

​ 请参考<a target='_blank' href ='Build.md#123-初始化数据库'>初始化数据库</a>

# 4 准备相关文件和目录

# 4.1 前后端程序打包后上传的指定目录

![procedure_dir.png](img/deploy/procedure_dir.png)

# 5 前端部署

## 5.1 将打包后的dist目录上传至lanius-fe目录下

## 5.2 Nginx配置文件：nginx.conf

使用vim命令编辑nginx.conf

在http标签下追加以下配置

```
server {
	listen       80;
	listen  [::]:80;
	server_name  localhost;
	location / {
		root   /home/lanius/lanius-fe/dist;
		index  index.html index.htm;
		try_files $uri $uri/ /index.html;
	}
	location /api/ {
		proxy_pass http://127.0.0.1:8081/;
	}
	error_page   500 502 503 504  /50x.html;
	location = /50x.html {
		root   /opt/nginx/nginx-1.9.9/html;
	}
}
```
## 5.3 启动前端程序

在nginx安装目录sbin下启动或者重启nginx
执行以下命令：

```
./nginx  或者 ./nginx -s reload
```

# 6 后端部署
## 6.1 准备好相关文件目录
### 6.1.1 后端程序上传后的目录结构

将打包后的dist目录下的所有文件上传至lanius-server目录下

![server_dir.png](img/deploy/server_dir.png)

## 6.2 脚本部署

### 6.2.1 进入lanius-server目录下

```
cd lanius-server/
```
### 6.2.2 启动后端程序

#### 6.2.2.1 执行startup.sh脚本

使用sh命令，命令如下：
```
sh ./startup.sh
```

#### 6.2.2.2 启动成功

![script-server-start.png](img/deploy/script-server-start.png)

使用下面命令查看java进程。
命令如下：

```
ps -ef|grep java
```

![server-course.png](img/deploy/server-course.png)

### 6.2.3 停止后端程序

执行shutdown.sh脚本。

命令如下：

```
sh ./shutdown.sh
```

![script-server-shutdown.png](img/deploy/script-server-shutdown.png)

## 6.3 Docker容器部署

## 6.3.1 准备好相关环境

注意：请提前安装docker和docker-compose并自行配置镜像加速。

## 6.3.2 docker-compose 配置后端程序容器

### 6.3.2.1 编写Dockerfile

进入jar包所在的目录，编写Dockerfile

```
FROM openjdk:11

ENV TZ=Asia/Shanghai

RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /home/lanius/lanius-server

EXPOSE 8081

ADD ./lanius-orchestrate-server.jar ./

CMD java -jar lanius-orchestrate-server.jar -Xmx512m  --spring.profiles.active=dev

```
### 6.3.2.2 编写docker-compose.yml文件

在lanius目录下编写docker-compose.yml

```
version: '3.3'

services:
  lanius-orchestrate.service:
    build:
      context: /home/lanius/lanius-server #Dockerfile的目录
      dockerfile: Dockerfile 
    network_mode: host
    restart: always
    privileged: true 
    container_name: lanius-orchestrate.service
    hostname: lanius-orchestrate.service
    image: lanius-orchestrate.service
    volumes: #如果上传文件保存在本地需要进行地址映射
      - /home/lanius/lanius-server/logs:/home/lanius/lanius-server/logs
      - /home/lanius/lanius-server/task-instance-log:/home/lanius/lanius-server/task-instance-log
      - /home/lanius/lanius-server/application-dev.yml:/home/lanius/lanius-server/application-dev.yml
      - /home/lanius/lanius-server/logback-spring.xml:/home/lanius/lanius-server/logback-spring.xml

    ports: 
      - 8081:8081
```
## 6.3.3 启动后端程序容器

在docker-compose.yml 即lanius目录下启动后端程序容器
命令如下：

```
docker-compose up -d
或者
docker-compose --compatibility up
```

![server-docker-compose-up.png](img/deploy/server-docker-compose-up.png)

## 6.3.4 停止后端程序容器

在docker-compose.yml 文件路径下 停止lanius-orchestrate.service服务
命令如下：
```

docker-compose down
```

![server-docker-compose-down.png](img/deploy/server-docker-compose-down.png)

## 6.3.5 查看在线的后端程序容器

在docker-compose.yml 文件路径下 查看启动的容器
命令如下：

```
docker-compose ps
```

![server-docker-compose-ps.png](img/deploy/server-docker-compose-ps.png)

# 7 浏览访问

## 7.1 超级管理平台登录

http://xxxx/admin/login

默认超管账号密码：admin/123QWEqwe!@#

![admin-login.png](img/deploy/admin-login.png)

## 7.2 普通用户登录

http://xxxx/login

![user-login.png](img/deploy/user-login.png)


