# 1 本地开发

##    1.1 基本环境

​    JDK:11+、Maven:3.5+、MYSQL:8+ | 达梦:8 | Oracle:11g、Node:16+、npm:8.16.0+、vue/cli:5+

##    1.2 开发环境

​    idea、vscode

##    1.3 后台系统工程(JAVA端)

###     1.3.1 下载代码

```
https://gitee.com/yzbdlab/lanius-orchestrate.git
```
###     1.3.2 idea加载lanius-orchestrate-server目录
![server-path.png](img/build/fast/server-path.png)
###     1.3.3 打开项目加载依赖目录如下：
![project-module.png](img/develop/project-module.png)
###     1.3.4 在lanius-orchestrate-common模块的pom.xml中添加数据库依赖
####     1.3.4.1 mysql
```
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```
####     1.3.4.2 达梦
首先准备达梦数据库的驱动Jar包，使用mvn install命令安装到本地仓库
```
mvn install:install-file -Dfile=D:\tool\dm\DmJdbcDriver18.jar -DgroupId=dm.jdbc -DartifactId=dm -Dversion=8 -Dpackaging=jar -DgeneratePom=true -DcreateChecksum=true
```
然后添加依赖
```
<dependency>
	<groupId>dm.jdbc</groupId>
	<artifactId>dm</artifactId>
	<version>8</version>
	<scope>runtime</scope>
</dependency>
```
####     1.3.4.3 Oracle
```
<dependency>
    <groupId>com.oracle.database.jdbc</groupId>
    <artifactId>ojdbc8</artifactId>
    <version>21.1.0.0</version>
</dependency>
<dependency>
    <groupId>com.oracle.database.nls</groupId>
    <artifactId>orai18n</artifactId>
    <version>21.1.0.0</version>
</dependency>
```

###     1.3.5 配置开发环境数据库信息
####     1.3.5.1 使用Mysql数据库，文件路径如下：
![dev-yml.png](img/develop/dev-yml.png)

![dev-db-yml.png](img/develop/dev-db-yml.png)

####     1.3.5.2 使用达梦数据库，文件路径如下：
![dm-yml.png](img/develop/dm-yml.png)

![dm-db-yml.png](img/develop/dm-db-yml.png)

####     1.3.5.3 使用Oracle数据库，文件路径如下：
![oracle-yml.png](img/develop/oracle-yml.png)

![oracle-db-yml.png](img/develop/oracle-db-yml.png)

###     1.3.6 在父级pom.xml输入命令mvn clean install 或者 idea操作 install
![parent-clean-install.png](img/develop/parent-clean-install.png)
###     1.3.7 启动程序
####     1.3.7.1 使用Mysql数据库
![application-dev-active.png](img/develop/application-dev-active.png)
####     1.3.7.2 使用达梦数据库
![application-dm-active.png](img/develop/application-dm-active.png)
####     1.3.7.3 使用Oracle数据库
![application-oracle-active.png](img/develop/application-oracle-active.png)
####     1.3.7.4 启动后端程序
![start-program.png](img/develop/start-program.png)
##    1.4 前端工程(VUE)

###     1.4.1 请确保本地已经安装node

###     1.4.2 下载源码

```
https://gitee.com/yzbdlab/lanius-orchestrate.git
```
###     1.4.3 加载lanius-orchestrate-client目录
![client-path.png](img/build/fast/client-path.png)

###     1.4.4 npm i,当前所有命令必须在当前工程目录下进行，目录结构如下：
![vue-project.png](img/develop/vue-project.png)
###     1.4.5 修改开发环境后端连接信息，文件路径如下：
![background-connect.png](img/develop/background-connect.png)
###     1.4.6 修改前端访问IP与端口，文件路径如下：
![web-access.png](img/develop/web-access.png)
###     1.4.7 在控制台输入命令：npm run dev,控制台打印出如下画面，则表示启动成功
![vue-start-success.png](img/develop/vue-start-success.png)
##    1.5 浏览访问

###      1.5.1 超级管理平台登录

​			http://localhost:3000/admin/login

​            默认超管账号密码：admin/123QWEqwe!@#
![admin-login.png](img/develop/admin-login.png)
###      1.5.2 普通用户登录

​            http://localhost:3000/login
![auser-login.png](img/develop/user-login.png)
