# 1 本地编译

##    1.1 基本环境(必备)

​    JDK:11+、Maven:3.5+、MYSQL:8+ | 达梦:8 | Oracle:11g、Node:16+


##    1.2 后台系统工程(JAVA端)

###     1.2.1 下载代码

```
https://gitee.com/yzbdlab/lanius-orchestrate.git
```
切换到JAVA端目录：

![server-path.png](img/build/fast/server-path.png)
###     1.2.2 打开项目，目录结构如下：
![server-detail.png](img/build/fast/server-detail.png)

### 1.2.3 初始化数据库

#### 1.2.3.1 Mysql

##### 1.2.3.1.1 新建数据库 lanius_orchestrate

##### 1.2.3.1.2 导入数据库，文件路径如下：

db/lanius_orchestrate_latest.sql

![db-mysql-script.png](img/build/fast/db-mysql-script.png)

#### 1.2.3.2 达梦
##### 1.2.3.2.1 创建新模式LANIUS
![db-dm-schema.png](img/build/fast/db-dm-schema.png)
##### 1.2.3.2.2 导入数据库，文件路径如下：

db/lanius_orchestrate_dm.sql

![db-dm-script.png](img/build/fast/db-dm-script.png)

#### 1.2.3.3 Oracle
##### 1.2.3.3.1 创建新用户LANIUS，使用LANIUS模式
![db-oracle-schema.png](img/build/fast/db-oracle-schema.png)
##### 1.2.3.3.2 导入数据库，文件路径如下：

db/lanius_orchestrate_oracle.sql

![db-oracle-script.png](img/build/fast/db-oracle-script.png)

###     1.2.4 在lanius-orchestrate-common模块的pom.xml中添加数据库依赖

####     1.2.4.1 Mysql
```
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```
####     1.2.4.2 达梦
首先准备达梦数据库的驱动Jar包，使用mvn install命令安装到本地仓库
```
mvn install:install-file -Dfile=D:\tool\dm\DmJdbcDriver18.jar -DgroupId=dm.jdbc -DartifactId=dm -Dversion=8 -Dpackaging=jar -DgeneratePom=true -DcreateChecksum=true
```
然后添加依赖
```
<dependency>
	<groupId>dm.jdbc</groupId>
	<artifactId>dm</artifactId>
	<version>8</version>
	<scope>runtime</scope>
</dependency>
```

####     1.2.4.3 Oracle
```
<dependency>
    <groupId>com.oracle.database.jdbc</groupId>
    <artifactId>ojdbc8</artifactId>
    <version>21.1.0.0</version>
</dependency>
<dependency>
    <groupId>com.oracle.database.nls</groupId>
    <artifactId>orai18n</artifactId>
    <version>21.1.0.0</version>
</dependency>
```

###     1.2.5 在lanius-orchestrate模块的resource下提供了默认的yml配置文件
![orchestrate-resource.png](img/build/fast/orchestrate-resource.png)

####     1.2.5.1 Mysql

![mysql-yml.png](img/build/fast/mysql-yml.png)

####     1.2.5.2 达梦

![dm-yml.png](img/build/fast/dm-yml.png)

####     1.2.5.3 Oracle

![oracle-yml.png](img/build/fast/oracle-yml.png)

###     1.2.6 执行打包编译脚本，路径如下

![server-complie-package.png](img/build/fast/server-complie-package.png)

```
双击脚本：  package.bat
```

### 1.2.7 查看打包编译日志信息，路径如下

![server-build-log.png](img/build/fast/server-build-log.png)

### 1.2.8 打包编译后输出目录
![server-package-output-dir.png](img/build/fast/server-package-output-dir.png)

### 1.2.9 输出目录结构

![server-package-output-dir-structure.png](img/build/fast/server-package-output-dir-structure.png)

### 1.2.10 配置开发环境数据库信息
#### 1.2.10.1 Mysql，文件路径如下：
![server-dev-yml.png](img/build/fast/server-dev-yml.png)

![dev-db-yml.png](img/build/fast/dev-db-yml.png)

```
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/lanius_orchestrate?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true
    username: root
    password: root
```
#### 1.2.10.2 达梦，文件路径如下：
![server-dm-yml.png](img/build/fast/server-dm-yml.png)

![dm-db-yml.png](img/build/fast/dm-db-yml.png)

```
spring:
  datasource:
    driver-class-name: dm.jdbc.driver.DmDriver
    url: jdbc:dm://localhost:5236?schema=LANIUS
    username: SYSDBA
    password: 123456789
```

#### 1.2.10.3 Oracle，文件路径如下：
![server-oracle-yml.png](img/build/fast/server-oracle-yml.png)

![oracle-db-yml.png](img/build/fast/oracle-db-yml.png)

```
spring:
  datasource:
    driver-class-name: oracle.jdbc.OracleDriver
    url: jdbc:oracle:thin:@127.0.0.1:1521:orcl
    username: LANIUS
    password: LANIUS
```

### 1.2.11 启动程序，启动程序路径如下：

进入目录/dist
#### 1.2.11.1 切换application.yml中active设置
![application-yml-path.png](img/build/fast/application-yml-path.png)

![application-active.png](img/build/fast/application-active.png)
dev配置的是Mysql数据库，dm配置的是达梦数据库，oracle配置的是Oracle数据库
#### 1.2.11.2 启动程序脚本


```
双击脚本 startup.cmd
```

启动成功
![server-start-success.png](img/build/fast/server-start-success.png)

#### 1.2.11.3 如果启动过程中窗口关闭

使用管理员身份运行脚本startup.cmd

![server-admin-run-startup.png](img/build/fast/server-admin-run-startup.png)

![server-admin-run-startup-cmd.png](img/build/fast/server-admin-run-startup-cmd.png)

这样可以查看到，后端程序启动时的错误日志信息

#### 1.2.11.4 查看后端程序启动的日志信息，路径如下

![sever-run-log.png](img/build/fast/sever-run-log.png)

#### 1.2.11.5 停止程序脚本


```
双击该脚本 shutdown.cmd
```


##    1.3 后台前端工程(VUE)

###     1.3.1 请确保本地已经安装node

###     1.3.2 下载源码

```
 https://gitee.com/yzbdlab/lanius-orchestrate.git
```
切换到前端目录：

![client-path.png](img/build/fast/client-path.png)
###     1.3.3 打开项目，目录结构如下：
![client-structure.png](img/build/fast/client-structure.png)
###     1.3.4 执行打包编译脚本：

```
双击脚本：  package.bat
```

在文件夹下

![client-structure-package.png](img/build/fast/client-structure-package.png)

### 1.3.5 打包编译后输出目录

![client-output-dir.png](img/build/fast/client-output-dir.png)

### 1.3.6 启动程序，启动程序路径如下：

进入目录/bin

目录结构如下：
![client-start-structure.png](img/build/fast/client-start-structure.png)

```
双击脚本：  startup.cmd
```

##    1.4 浏览访问

###      1.4.1 超级管理平台登录

​			http://localhost:3000/admin/login

​            默认超管账号密码：admin/123QWEqwe!@#
![admin-login.png](img/build/fast/admin-login.png)
###      1.4.2 普通用户登录

​            http://localhost:3000/login
![auser-login.png](img/build/fast/user-login.png)
